var app = angular.module('phoenix');
app.config(['$stateProvider','$ionicConfigProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
  function ($stateProvider,$ionicConfigProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires, $authProvider, $locationProvider) {
    $ionicConfigProvider.views.swipeBackEnabled(false);
    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob|cdvphotolibrary|ionic|app-file):|data:image\//);
    $compileProvider.aHrefSanitizationWhitelist(/^\s(https|ftp|file|cdvphotolibrary|mailto|chrome-extension|ionic|app-file|tel):/); 
    // LAZY MODULES
    $ocLazyLoadProvider.config({
      debug: false,
      events: true,
      modules: jsRequires.modules
    });

    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        resolve: loadSequence('SlideCtrl'),
        cache: false,
        //controller: 'userCtrl'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'templates/member-login.html',
        resolve: loadSequence('signupCtrl'),
        controller: 'signupCtrl',
        cache: false,
      })
      .state('forgot-password', {
        url: '/forgot-password',
        templateUrl: 'templates/forgot-password.html',
        resolve: loadSequence('signupCtrl'),
          controller: 'signupCtrl',
          cache: false,
      })
      .state('account-type', {
        url: '/account-type',
        templateUrl: 'templates/choose-account-type.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl',
          cache: false,
      })
      .state('create-account', {
        url: '/create-account',
        templateUrl: 'templates/member-createaccount.html',
        resolve: loadSequence('memberSignupCtrl'),
        controller: 'registerCtrl',
        cache: false,
      })

      .state('member-signup', {
        cache: false,
        url: '/member-signup',
        templateUrl: 'templates/member-signup.html',
        resolve: loadSequence('signupCtrl'),
        controller: 'signupCtrl'
      })
      .state('post-images', {
        cache: false,
        url: '/post-images/:post_id',
        templateUrl: 'templates/post-images.html',
        resolve: loadSequence('postCtrl'),
        controller: 'postCtrl'
      })

      .state('register', {
        cache: false,
        url: '/register',
        templateUrl: 'templates/register.html',
         resolve: loadSequence('memberSignupCtrl'),
           controller: 'registerCtrl'
      })
      .state('choose-account', {
        cache: false,
        url: '/choose-account',
        templateUrl: 'templates/choose-account-type.html',
        resolve: loadSequence('signupCtrl'),
        controller: 'signupCtrl'
      })
      .state('signup-form', {
        cache: false,
        url: '/signup-form/:user_type/:provider_type?',
        templateUrl: 'templates/signup-form.html',
        resolve: loadSequence('signupCtrl'),
        controller: 'signupCtrl'
      })
     .state('member-signup-form', {
      cache: false,
        url: '/member-signup-form',
        templateUrl: 'templates/member-signup-form.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
        controller: 'chooseAccountTypeCtrl'
      })
      .state('provider-signup-form', {
        cache: false,
        url: '/provider-signup-form/:user_type/:provider_type?',
        templateUrl: 'templates/provider-signup-form.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
        controller: 'chooseAccountTypeCtrl'
      })

      .state('privacy', {
        cache: false,
        url: '/privacy/:page',
        templateUrl: 'templates/privacy.html',
        resolve: loadSequence('privacyCtrl'),
        controller: 'privacyCtrl'
      })
       .state('curl-root', {
        cache: false,
         url: '/curl-root',
         templateUrl: 'templates/root-curl.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
        controller: 'chooseAccountTypeCtrl'
       })
       .state('advance-option', {
        cache: false,
        url: '/advance-option',
        templateUrl: 'templates/advance-option.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
        controller: 'chooseAccountTypeCtrl'
      })

      .state('create-post', {
        cache: false,
        url: '/create-post',
        templateUrl: 'templates/create-post.html',
        resolve: loadSequence('postCtrl','pr.longpress'),
        controller: 'postCtrl'
      })

      .state('explore', {
        cache: false,
        url: '/explore',
        templateUrl: 'templates/explore.html',
        resolve: loadSequence('exploreCtrl'),
        controller: 'exploreCtrl'
      })
      .state('filter', {
        cache: false,
        url: '/filter',
        templateUrl: 'templates/filter.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl'
      })
      .state('filterpost', {
        cache: false,
        url: '/filterpost/:post_type/:skeyword/:tags/:curl',
        templateUrl: 'templates/filterpost.html',
        resolve: loadSequence('filterpostCtrl','angularMoment'),
        controller: 'filterpostCtrl'
      })

      .state('find-service', {
        cache: false,
        url: '/find-service/:city_id?/:lat?/:lng?',
        templateUrl: 'templates/find-service.html',
        resolve: loadSequence('searchCtrl'),
        controller: 'searchCtrl'
      })
      .state('service-area', {
        cache: false,
        url: '/service-area',
        templateUrl: 'templates/service-area.html',
        resolve: loadSequence('searchCtrl'),
        controller: 'searchCtrl'
      })
      .state('message-camera', {
        cache: false,
        url: '/message-camera',
        templateUrl: 'templates/message-camera.html',
          resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl'
      })
      .state('message-chatbox', {
        cache: false,
        url: '/message-chatbox',
        templateUrl: 'templates/message-chatbox.html',
        resolve: loadSequence('chatCtrl'),
        controller: 'chatCtrl',
        params: {roomId: null}
      })
      .state('message-img', {
        cache: false,
        url: '/message-img',
        templateUrl: 'templates/message-img.html',
          resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl'
      })
      .state('message-loading', {
        cache: false,
        url: '/message-loading',
        templateUrl: 'templates/message-loading.html',
          resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl'
      })
      .state('message-imgupload', {
        cache: false,
        url: '/message-imgupload',
        templateUrl: 'templates/message-imgupload.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
        controller: 'chooseAccountTypeCtrl'
      })
      // .state('new-post-creation', {
      //   url: '/new-post-creation',
      //   templateUrl: 'templates/new-post-creation.html'
      // })
      .state('member-profile-preferences', {
        cache: false,
        url: '/member-profile-preferences',
        templateUrl: 'templates/member-profile-preferences.html',
         resolve: loadSequence('editprofileCtrl'),
           controller: 'editprofileCtrl'
      })

      .state('news-feed', {
        cache: false,
        url: '/news-feed',
        templateUrl: 'templates/news-feed.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl'
      })
      .state('news', {
        cache: false,
        url: '/news',
        templateUrl: 'templates/news.html',
        resolve: loadSequence('chooseAccountTypeCtrl'),
          controller: 'chooseAccountTypeCtrl'
      })
      .state('listings', {
        cache: false,
        url: '/listings/:show',
        templateUrl: 'templates/listings.html',
        //resolve: loadSequence('postCtrl','angularMoment','ion-autocomplete'),
        resolve: loadSequence('postCtrl','angularMoment'),
        controller: 'postCtrl'
      })
      .state('listings-details', {
        cache: false,
        url: '/listings-details/:post_id',
        templateUrl: 'templates/listings_details.html',
        resolve: loadSequence('postCtrl','angularMoment'),
        controller: 'postCtrl'
      })
      .state('notifications-list', {
        cache: false,
        url: '/notifications-list',
        templateUrl: 'templates/notifications-list.html',
        resolve: loadSequence('chatlistCtrl'),
          controller: 'chatlistCtrl'
      })
      .state('notifications', {
        cache: false,
        url: '/notifications',
        templateUrl: 'templates/notifications.html',
        resolve: loadSequence('notificationCtrl','angularMoment','ngDraggable'),
        controller: 'notificationCtrl'
      })
      .state('post-settings', {
        cache: false,
        url: '/post-settings/:post_id',
        templateUrl: 'templates/post-settings.html',
        resolve: loadSequence('postCtrl','ngTagsInput'),
        controller: 'postCtrl'
      })
    .state('member-profile-member-view', {
      cache: false,
        url: '/member-profile-member-view/:userId?',
        templateUrl: 'templates/member-profile-member-view.html',
        resolve: loadSequence('chooseAccountTypeCtrl','ion-autocomplete'),
        controller: 'chooseAccountTypeCtrl',
        params: {userId: null}
      })
      .state('pro-feature', {
        cache: false,
        url: '/pro-feature',
        templateUrl: 'templates/pro-feature.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })
      .state('provider-profile-view', {
        cache: false,
        url: '/provider-profile-view',
        templateUrl: 'templates/provider-profile-view.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })
      .state('provider-profile-preferences', {
        cache: false,
        url: '/provider-profile-preferences',
        templateUrl: 'templates/provider-profile-preferences.html',
         resolve: loadSequence('profilePreferenceCtrl'),
           controller: 'profilePreferenceCtrl'
      })
      .state('provider-setting', {
        cache: false,
        url: '/provider-setting',
        templateUrl: 'templates/provider-setting.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })
      .state('saves', {
        cache: false,
        url: '/saves',
        templateUrl: 'templates/saves.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })
      .state('settings', {
        cache: false,
        url: '/settings',
        templateUrl: 'templates/settings.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })
      .state('single-news', {
        cache: false,
        url: '/single-news',
        templateUrl: 'templates/single-news.html'
      })
      .state('social-share', {
        cache: false,
        url: '/social-share',
        templateUrl: 'templates/social-share.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })

      .state('location-result', {
        cache: false,
        url: '/location-result',
        templateUrl: 'templates/location-result.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })

      .state('location', {
        cache: false,
        url: '/location/:service_type?/:city_id?/:lat?/:lng?/:cityname?',
        templateUrl: 'templates/location.html',
        //resolve: loadSequence('locationCtrl','ion-autocomplete'),
        resolve: loadSequence('locationCtrl'),
        controller: 'locationCtrl'
      })
      .state('search-result', {
        cache: false,
        url: '/search-result',
        templateUrl: 'templates/search-result.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })
      .state('search-result-photos', {
        cache: false,
        url: '/search-result-photos/:user_id',
        templateUrl: 'templates/single-result-photos.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })

      .state('single-result', {
        cache: false,
        url: '/single-result/:user_id/:city/:distance',
        templateUrl: 'templates/single-result.html',
         resolve: loadSequence('locationCtrl'),
           controller: 'locationCtrl'
      })

      .state('single-result-review', {
        cache: false,
        url: '/single-result-review',
        templateUrl: 'templates/single-result-review.html',
         resolve: loadSequence('chooseAccountTypeCtrl'),
           controller: 'chooseAccountTypeCtrl'
      })





    $urlRouterProvider.otherwise('/member-signup');

    function loadSequence() {
      var _args = arguments;
      return {
        deps: ['$ocLazyLoad', '$q',
          function ($ocLL, $q) {
            var promise = $q.when(1);
            for (var i = 0, len = _args.length; i < len; i++) {
              promise = promiseThen(_args[i]);
            }
            return promise;

            function promiseThen(_arg) {
              if (typeof _arg == 'function')
                return promise.then(_arg);
              else
                return promise.then(function () {
                  var nowLoad = requiredData(_arg);
                  if (!nowLoad)
                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
                  return $ocLL.load(nowLoad);
                });
            }

            function requiredData(name) {
              if (jsRequires.modules)
                for (var m in jsRequires.modules)
                  if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
                    return jsRequires.modules[m];
              return jsRequires.scripts && jsRequires.scripts[name];
            }
          }
        ]
      };
    }

  }
]).directive('imageonload', function() {
  return {
      restrict: 'A',
      link: function(scope, element, attrs) {
          element.bind('load', function() {
              //call the function that was passed
              scope.$apply(attrs.imageonload);
          });
      }
  };
}).directive('bufferedScroll', function ($parse) {
  return function ($scope, element, attrs) {
    var handler = $parse(attrs.bufferedScroll);
    element.scroll(function (evt) {
      var scrollTop    = element[0].scrollTop,
          scrollHeight = element[0].scrollHeight,
          offsetHeight = element[0].offsetHeight;
      if (scrollTop === (scrollHeight - offsetHeight)) {
        $scope.$apply(function () {
          handler($scope);
        });
      }
    });
  };
});;
