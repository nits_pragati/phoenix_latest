app.service(
    "PostService",
    function ($q, $http, $window, $rootScope, $httpParamSerializer) {
        var savePostFiles = function (files,userid) {
            var allFiles = [];
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if(file.result)
                {
                  allFiles.push(file.result);
                }
                else
                {
                  allFiles.push(file);
                }
            }
            var request = $http({
              method: "POST",
              url: $rootScope.serviceurl + "posts/savePostFiles.json?user_id="+userid,
              data : $httpParamSerializer(allFiles),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });

            return (request.then(handleSuccess, handleError));
          }

        var savePostVideos = function (file,userid) {
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "posts/savePostFiles.json?user_id="+userid,
            data : $httpParamSerializer({video : file}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var replaceOldFile = function(file,name)
        {
          var data = {file : file,name : name}
           var request = $http({
              method: "POST",
              url: $rootScope.serviceurl + "posts/replaceOldFile.json",
              data : $httpParamSerializer(data),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });

            return (request.then(handleSuccess, handleError));
        }

        var replaceOldFiles = function(files)
        {
           var request = $http({
              method: "POST",
              url: $rootScope.serviceurl + "posts/replaceOldFiles.json",
              data : $httpParamSerializer(files),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });

            return (request.then(handleSuccess, handleError));
        }
        var getPostDetails = function(post_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "posts/getDetails.json?id=" + post_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
        }

        var getPostList = function(user_id, keyWord ,tags,show,pageno)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "posts/getPostList.json?user_id=" + user_id+'&key_word='+keyWord+'&tags='+ tags +'&show='+show+'&pageno='+pageno,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
        }

        var getPostListexplore = function(user_id,is_feature)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "posts/getPostListexplore.json?user_id=" + user_id+'&is_feature='+is_feature,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
        }


        var getPostListnew = function(user_id, keyWord ,spost_type,tags,curl)
        {
            //console.log('new');
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "posts/getPostListnew.json?user_id=" + user_id+'&key_word='+keyWord+'&tags='+ tags +'&post_type='+ spost_type +'&curl='+ curl,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
        }

        var getPostListImage = function(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "posts/getUserPostImageList.json?user_id=" + user_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
        }
        var savePostDetails = function(postDetails){
          if(postDetails.tags)
          {
            postDetails.tags = JSON.stringify(postDetails.tags);
          }

          var request = $http({
              method: "POST",
              url: $rootScope.serviceurl + "posts/savePostDetails.json",
              data : $httpParamSerializer(postDetails),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });

            return (request.then(handleSuccess, handleError));
        }

        var savePostComment = function(data){
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "post_comments/savecomment.json",
            data : $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });
          return (request.then(handleSuccess, handleError));
        }

        var savePostLike = function(data){
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "post_likes/savelike.json",
            data : $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var saveCommentLike = function(data){
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "comment_likes/savelike.json",
            data : $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var getMemberList = function(data){
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/memberList.json",
            data : $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }
        
        var saveFavPost = function(data){
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "posts/usersaveFavPost.json",
            data : $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var checkFavPost = function(data){
          var request = $http({
            method: "POST",
            url: $rootScope.serviceurl + "posts/userSaveFavPostCheck.json",
            data : $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var getTagSuggetion = function(key){
          var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "hashtags/search.json?keyword=" + key,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var getPostById = function(id,user_id){
          var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "posts/getpostbyid.json?id=" + id + "&user_id=" + user_id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var getCountryList = function(){
          var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "countries/list.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var getCityDetails = function(id){
          var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "countries/citydetails.json?id=" + id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        var getCityList = function(id){
          var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "countries/citylist.json?id=" + id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          });

          return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
              return ($q.reject("An unknown error occurred."));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
          }

          function handleSuccess(response) {
            return (response.data);
          }


          return {
            savePostFiles : savePostFiles,
            getPostDetails : getPostDetails,
            savePostDetails : savePostDetails,
            getPostList : getPostList,
            replaceOldFile : replaceOldFile,
            replaceOldFiles : replaceOldFiles,
            savePostComment : savePostComment,
            savePostLike : savePostLike,
            saveCommentLike : saveCommentLike,
            getTagSuggetion : getTagSuggetion,
            getMemberList : getMemberList,
            getPostById : getPostById,
            getPostListImage:getPostListImage,
            getCountryList : getCountryList,
            getCityDetails : getCityDetails,
            getCityList : getCityList,
            getPostListnew:getPostListnew,
            saveFavPost:saveFavPost,
            checkFavPost:checkFavPost,
            savePostVideos: savePostVideos,
            getPostListexplore
          }
})
