app.service(
    "NotificationService",
    function ($q, $http, $window, $rootScope, $httpParamSerializer) {
        
        var getNotificationsList = function (id) {
            var request = $http({
              method: "GET",
              url: $rootScope.serviceurl + "notifications/list.json?user_id="+id
            });
      
            return (request.then(handleSuccess, handleError));
        }

        var getSavePostList = function (id) {
            var request = $http({
              method: "GET",
              url: $rootScope.serviceurl + "notifications/save_postlist.json?user_id="+id
            });
      
            return (request.then(handleSuccess, handleError));
        }

        var readNotification1 = function (id) {
            var request = $http({
              method: "GET",
              url: $rootScope.serviceurl + "notifications/readnoti.json?noti_id="+id
            });
      
            return (request.then(handleSuccess, handleError));
        }

        var addNewFolder = function (data) {
            var request = $http({
                method: 'POST',
                url: $rootScope.serviceurl+"folders/addnew.json",
                data: $httpParamSerializer(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
               });
           return( request.then( handleSuccess, handleError ) );
        }

        var saveToFolder = function (data) {
            var request = $http({
                method: 'POST',
                url: $rootScope.serviceurl+"folders/addfile.json",
                data: $httpParamSerializer(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
               });
           return( request.then( handleSuccess, handleError ) );
        }

        var readNotification = function (id) {
            var request = $http({
              method: "GET",
              url: $rootScope.serviceurl + "folders/folderlist.json?user_id="+id
            });
      
            return (request.then(handleSuccess, handleError));
        }

        var getFolderFiles = function(id){
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "folders/getFolderFiles.json?folder_id="+id
              });
        
              return (request.then(handleSuccess, handleError));
        }
        
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
              return ($q.reject("An unknown error occurred."));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
          }
      
          function handleSuccess(response) {
            return (response.data);
          }
      
      
        return {
            getNotificationsList : getNotificationsList,
            getSavePostList:getSavePostList,
            readNotification : readNotification,
            addNewFolder:addNewFolder,
            readNotification1: readNotification1,
            saveToFolder:saveToFolder,
            getFolderFiles:getFolderFiles
        }
    })