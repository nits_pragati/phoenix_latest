app.service(
  "ProductService",
  function ($q, $http, $window, $rootScope, $httpParamSerializer) {




    var getHomeProducts = function (page, user_id, conditions) {

      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/list_service.json",
        data: $httpParamSerializer({
          user_id: user_id,
          conditions: conditions
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });

      return (request.then(handleSuccess, handleError));
    }



    var getTechnicians = function () {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "users/listtechnician.json",
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var getUsersTechnicians = function (user_id) {
      var request = $http({
        method: "POST",
        data: {
          user_id: user_id
        },
        url: $rootScope.serviceurl + "users/userstechnicianlist.json",
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var makeArchive = function (data) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/archive.json",
        data: $httpParamSerializer(data),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var saveCrown = function (data) {
      if (data.id) {
        var request = $http({
          method: "POST",
          url: $rootScope.serviceurl + "crownfabrications/update.json",
          data: $httpParamSerializer(data),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      } else {
        var request = $http({
          method: "POST",
          url: $rootScope.serviceurl + "crownfabrications/add.json",
          data: $httpParamSerializer(data),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });
      }
      return (request.then(handleSuccess, handleError));
    }

    var getCurrentProduct = function (id) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/details.json",
        data: $httpParamSerializer({
          id: id
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var getProductDetails = function (id) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/viewdetails.json",
        data: $httpParamSerializer({
          id: id
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var getTechniciansProducts = function (user_id, conditions) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/technicians_list.json",
        data: $httpParamSerializer({
          user_id: user_id,
          conditions: conditions
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });

      return (request.then(handleSuccess, handleError));
    }

    var markAsRead = function (id) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/archive.json",
        data: $httpParamSerializer({
          id: id,
          is_read: 1
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var fixWhiteBalance = function (data) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "crownfabrications/white_balance.json",
        data: $httpParamSerializer(data),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });

      return (request.then(handleSuccess, handleError));
    }

    var saveSubscription = function (data) {
      var request = $http({
        method: "POST",
        url: $rootScope.serviceurl + "paypal/payment.json",
        data: $httpParamSerializer(data),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      return (request.then(handleSuccess, handleError));
    }

    var getFilePath = function (data) {
      var deferred = $q.defer();
      window.FilePath.resolveNativePath(data, function (result) {
        deferred.resolve('file://' + result);
      }, function (error) {
        throw new Error("");
      });

      return deferred.promise;
    }

    //    var getFilePath = function(Uri contentURI) {
    //        Cursor cursor = this.cordova.getActivity().getContentResolver().query(contentURI, null, null, null, null); 
    //        cursor.moveToFirst(); 
    //        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
    //        console.log(cursor.getString(idx))
    //        return cursor.getString(idx); 
    //    }




    function handleError(response) {
      if (!angular.isObject(response.data) || !response.data.message) {
        return ($q.reject("An unknown error occurred."));
      }
      // Otherwise, use expected error message.
      return ($q.reject(response.data.message));
    }

    function handleSuccess(response) {
      return (response.data);


    }


    return {
      getHomeProducts: getHomeProducts,
      getTechnicians: getTechnicians,
      saveCrown: saveCrown,
      getCurrentProduct: getCurrentProduct,
      getTechniciansProducts: getTechniciansProducts,
      getProductDetails: getProductDetails,
      fixWhiteBalance: fixWhiteBalance,
      saveSubscription: saveSubscription,
      getUsersTechnicians: getUsersTechnicians,
      makeArchive: makeArchive,
      markAsRead: markAsRead,
      getFilePath: getFilePath
    };
  })
