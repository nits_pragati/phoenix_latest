app.service(
    "UserService", function($q, $http, $window,$rootScope,$httpParamSerializer,$httpParamSerializerJQLike) {
    var LOCAL_TOKEN_KEY = 'yourTokenKey';
    var username = '';
    var userInfo = '';
    var isAuthenticated = false;
    var role = '';
    var authToken;
    var now=new Date()
    var exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());
    var register = function(data) {
        return $q(function(resolve, reject) {
            $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/register.json",
             data: $httpParamSerializerJQLike(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

                //$cookies.put('CurrentUser',response.user_id,{expires:exp});
                resolve(response);
               //console.log(response);
            }).error(function () {

           reject('Registration Failed.');
          });
        });
    };

    var userProfileEdit = function(data) {
        return $q(function(resolve, reject) {
            $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/userProfileEdit.json",
            data: $httpParamSerializerJQLike(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                resolve(response);
            }).error(function () {
              reject('Data update Failed.');
          });
        });
    };
    var saveUserDetails = function(data){
        return $q(function(resolve, reject) {
            $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/userregister_service",
             data: $httpParamSerializer(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

                //$cookies.put('CurrentUser',response.user_id,{expires:exp});
                resolve(response);
               //console.log(response);
            }).error(function () {

           reject('Registration Failed.');
           //console.log('failed...');
          });
        });
    }

    var userlogin = function(data) {
        return $q(function(resolve, reject) {
            $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/token.json",
             data: $httpParamSerializer(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {

                //$cookies.put('CurrentUser',response.user_id,{expires:exp});
                resolve(response);
               //console.log(response);
            }).error(function () {

           reject('Registration Failed.');
           //console.log('failed...');
          });
        });
    };
    var forgotPassword = function(data) {
        var request = $http({
            method: "POST",
            data: $httpParamSerializer(data),
            url: $rootScope.serviceurl+"users/forgotpassword.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    };

    var setUserInfo = function(data)
    {
        var userInfo
        $window.localStorage.setItem(LOCAL_TOKEN_KEY, JSON.stringify(data));
        if ($window.localStorage[LOCAL_TOKEN_KEY]) {
          userInfo = JSON.parse($window.localStorage[LOCAL_TOKEN_KEY]);
        }
        return userInfo;
    }

    var getUserInfo = function()
    {
        var userInfo = 0;
        if ($window.localStorage[LOCAL_TOKEN_KEY]) {
            //console.log($window.localStorage[LOCAL_TOKEN_KEY]);
          userInfo = JSON.parse($window.localStorage[LOCAL_TOKEN_KEY]);
        }
        return userInfo;
    }

    var checkUsername = function(data)
    {
        var request = $http({
            method: "POST",
            data: $httpParamSerializer(data),
            url: $rootScope.serviceurl+"users/checkusername.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }

    var followUser = function(data) {
        var request = $http({
            method: "POST",
            data: $httpParamSerializer(data),
            url: $rootScope.serviceurl+"followings/save.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }

    var logout = function(){
        $window.localStorage.removeItem(LOCAL_TOKEN_KEY);
        //if(typeof facebookConnectPlugin != undefined)
        {
            facebookConnectPlugin.logout();
            window.plugins.googleplus.logout();
        }

    }

    var getUserDetailsById = function(userId,id){
        var request = $http({
            method: "POST",
            url: $rootScope.serviceurl+"users/details.json",
            data: $httpParamSerializer({user_id:userId,my_id : id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }
    var getUserDetails = function(userId){
        var request = $http({
            method: "POST",
            url: $rootScope.serviceurl+"users/getuserdetails.json",
            data: $httpParamSerializer({id:userId}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }
    var updateUserDetails = function(data){
        var request = $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/updateuser_service.json",
             data: $httpParamSerializer(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        return( request.then( handleSuccess, handleError ) );
    }

    var getNewMsgCnt = function(data){
        var request = $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/getNewNotificationCnt.json",
             data: $httpParamSerializer(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        return( request.then( handleSuccess, handleError ) );
    }
    var updateUserDetailsById = function(data){
        var request = $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/updateuser_service.json",
             data: $httpParamSerializer(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        return( request.then( handleSuccess, handleError ) );
    }

    var socialLogin = function(data){
        var request = $http({
             method: 'POST',
             url: $rootScope.serviceurl+"users/sociallogin.json",
             data: $httpParamSerializer(data),
             headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        return( request.then( handleSuccess, handleError ) );
    }

    var getUserList = function(id){
        var request = $http({
                method: "GET",
                url: $rootScope.serviceurl + "users/getUserList.json?service_type="+id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
    }

    var getUserSearch = function(data){
        console.log('data users',data);
        var request = $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getUsersSearch.json",
                data: $httpParamSerializer(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return (request.then(handleSuccess, handleError));
    }

    var getUserDetailsData = function (uid,my_id){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/getuserdetails.json",
            data: $httpParamSerializer({id:uid,my_id:my_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
       return( request.then( handleSuccess, handleError ) );
    }

    var getFollowUnfollow = function (uid,my_id){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/checkfollow.json",
            data: $httpParamSerializer({id:uid,my_id:my_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
       return( request.then( handleSuccess, handleError ) );
    }

    var updateUserFields = function (uid,data){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/updateUserFields.json?user_id=" + uid,
            data: $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
       return( request.then( handleSuccess, handleError ) );
    }
    

    var getBlockUnblock = function (uid,my_id){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"blocks/checkblocks.json",
            data: $httpParamSerializer({id:uid,my_id:my_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
       return( request.then( handleSuccess, handleError ) );
    }
    
    var savereview = function (data){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/savereview.json",
            data: $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
       return( request.then( handleSuccess, handleError ) );
    }

    var checkpostnoti = function (uid,p_id){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"blocks/checkpostnoti.json",
            data: $httpParamSerializer({id:uid,p_id:p_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
       return( request.then( handleSuccess, handleError ) );
    }

    var addPostNoti = function(data) {
        var request = $http({
            method: "POST",
            data: $httpParamSerializer(data),
            url: $rootScope.serviceurl+"blocks/savepostnoti.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }

    var blockUser = function(data) {
        var request = $http({
            method: "POST",
            data: $httpParamSerializer(data),
            url: $rootScope.serviceurl+"blocks/save.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }

    var getBlocksAccount = function(id){
        var request = $http({
            method: "POST",
            data: $httpParamSerializer({my_id:id}),
            url: $rootScope.serviceurl+"blocks/getblocks.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return( request.then( handleSuccess, handleError ) );
    }

    var deleteChatMsg = function (did){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/deleteChatMsg.json",
            data: $httpParamSerializer({id:did}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
       return( request.then( handleSuccess, handleError ) );
    }
    
    var getUserPostPhotos = function (uid){
        var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "users/getuserphotos.json?id="+uid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return (request.then(handleSuccess, handleError));
    }

    var getUserPostPhotosnew = function (uid){
        var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "users/getuserpostphotos.json?id="+uid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return (request.then(handleSuccess, handleError));
    }

    var getChatsdata = function(data){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/getchatlist.json",
            data: $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
       return( request.then( handleSuccess, handleError ) );
    }

    var getmyChatList = function(data){
        var request = $http({
            method: 'POST',
            url: $rootScope.serviceurl+"users/getuserchatlist.json",
            data: $httpParamSerializer(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
           });
        return( request.then( handleSuccess, handleError ) );
    }

    var getNewNotifications = function(id){
        var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "notifications/unread.json?user_id=" + id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return (request.then(handleSuccess, handleError));
    }
    
    var getHashTagList = function(){
        var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "hashtags/getHashTag.json",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return (request.then(handleSuccess, handleError));
    }

    var getFavPostFiles = function(id){
        var request = $http({
            method: "GET",
            url: $rootScope.serviceurl + "users/getFavPostFiles.json?user_id=" + id,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
        return (request.then(handleSuccess, handleError));
    }

    function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

    function handleSuccess( response ) {
        return( response.data );


    }


  return {
    register:register,
    userlogin:userlogin,
    setUserInfo:setUserInfo,
    forgotPassword:forgotPassword,
    getUserInfo:getUserInfo,
    logout:logout,
    getUserDetailsById:getUserDetailsById,
    saveUserDetails:saveUserDetails,
    updateUserDetails:updateUserDetails,
    getUserList : getUserList,
    socialLogin : socialLogin,
    getUserDetails:getUserDetails,
    updateUserDetailsById:updateUserDetailsById,
    getUserDetailsData:getUserDetailsData,
    getChatsdata:getChatsdata,
    getmyChatList:getmyChatList,
    getUserPostPhotos:getUserPostPhotos,
    getUserPostPhotosnew:getUserPostPhotosnew,
    checkUsername:checkUsername,
    followUser:followUser,
    getNewMsgCnt:getNewMsgCnt,
    userProfileEdit:userProfileEdit,
    deleteChatMsg:deleteChatMsg,
    getHashTagList:getHashTagList,
    getNewNotifications:getNewNotifications,
    getUserSearch:getUserSearch,
    getFollowUnfollow,
    getBlockUnblock,
    blockUser,
    checkpostnoti,
    addPostNoti,
    savereview,
    getFavPostFiles,
    getBlocksAccount,
    updateUserFields
  };
})

