app.service(
    "CommonService",
    function ($q, $http, $window, $rootScope, $httpParamSerializer) {
        var getCurlCategories = function () {

            var request = $http({
              method: "GET",
              url: $rootScope.serviceurl + "curl_patterns/categories.json"
            });
      
            return (request.then(handleSuccess, handleError));
          }

          var getCurlDetailsById = function (id) {

            var request = $http({
              method: "GET",
              url: $rootScope.serviceurl + "curl_patterns/getcurl.json?id="+id
            });
      
            return (request.then(handleSuccess, handleError));
          }
      
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
              return ($q.reject("An unknown error occurred."));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
          }
      
          function handleSuccess(response) {
            return (response.data);
          }
      
      
          return {
            getCurlCategories : getCurlCategories,
            getCurlDetailsById:getCurlDetailsById
          }
})