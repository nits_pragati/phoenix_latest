app.directive('imageGallery', ['$timeout', function($timeout) {
    return {
        restrict: "E",
        scope : {
           cancelmethod : '&',
           savemethod : '&',
           visible: '='
        },
        templateUrl : 'js/directives/gallery.html',
        link: function($scope, elem, attrs) {
            //console.log('inital');
            $scope.selectedImages = {};
            $scope.selectedAlbum = '';
            $scope.toDataUrl = function (path,id, callback) {
                // console.log(' ppppppp ',path)
                window.resolveLocalFileSystemURL(path, gotFile, fail);
        
                function fail(e) {
                    console.log('Cannot found requested file',e);
                }
        
                function gotFile(fileEntry) {
                    // console.log(' file entry ',fileEntry.toURL());
                    // console.log(' internal ',fileEntry.toInternalURL())
                    fileEntry.file(function (file) {
                        var reader = new FileReader();
                        reader.onloadend = function (e) {
                            var content = this.result;
                            callback(content,new Date().valueOf());
                        };
                        // The most important point, use the readAsDatURL Method from the file plugin
                        reader.readAsDataURL(file);
                    });
                }
            }
            $scope.library = [];
            $scope.albumImages = [];
            $scope.getAllImages = function(albumid){
                //console.log('started')
                $scope.library = [];
                if(typeof(cordova)!= 'undefined')
                {
                    //alert(typeof(cordova))
                    if(!albumid)
                    {
                        cordova.plugins.photoLibrary.getLibrary(
                            function (result) {
                                // console.log('result ',result)
                            
                                
                            result.library.forEach(function(libraryItem) {                       
                                if(libraryItem.filePath)
                                {
                                    if(!albumid || (albumid && libraryItem.albumIds.indexOf(albumid)>=0))
                                    {
                                        $scope.toDataUrl("file://" + libraryItem.filePath,libraryItem.id, function (base64Image,id) {
                                            $scope.library.push({image : base64Image,id : id});
                                            $scope.albumImages.push({image : base64Image,id : id,albumids: libraryItem.albumIds});
                                            $scope.$apply();
                                        });
                                    }
                                    
                                }
                                else
                                {
                                }
                            
                            })
                        
                        
                            },
                            function (err) {
                                // console.log('errr',err);
                                cordova.plugins.photoLibrary.requestAuthorization(
                                    function () {
                                        $scope.getAllImages($scope.selectedAlbum);
                                        $scope.getAlbums();
                                    },
                                    function (err) {
                                        console.log('err',err)
                                    }, // if options not provided, defaults to {read: true}.
                                    {
                                    read: true,
                                    write: true
                                    }
                                );
                    
                            },
                            { // optional options
                                useOriginalFileNames:true,
                                includeAlbumData: true, // default
                                itemsInChunk: 50, 
                                chunkTimeSec: 0.5, 
            
                            }
                        );
                    }
                    else
                    {
                        $scope.albumImages.forEach(function(image){
                            if((albumid && image.indexOf(albumid)>=0))
                            {
                                $scope.library.push({image : image.image,id : image.id});
                            }
                        })
                    }
                }
                
            }
            
            $scope.croppie = '';
            $scope.startCropper = function(img){
                console.log('click')
                // console.log('hii111',Object.keys($scope.selectedImages).length)
                        if($scope.activeCrop)
                        {
                            var old_one = $scope.activeCrop;
                        }
                
                       if($scope.croppie)
                        {                            
                            
                            $scope.croppie.croppie('result').then(function(html,size) {
                                //console.log('cropping',html)
                                if(html && $scope.selectedImages && 
                                    
                                    $scope.selectedImages[old_one.id])
                                {
                                    //console.log(' before undefined',$scope.selectedImages[old_one.id])
                                    $scope.selectedImages[old_one.id].base = html
                                    //$scope.activeCrop.base = html
                                    $scope.selectedImages[old_one.id].croppie = $scope.croppie;
                                    console.log('croppie',size)
                                }
                                
                            });
                            console.log('destroi',$scope.selectedImages)
                            $('.cropbox').croppie('destroy');
                            $scope.croppie = null;
                        }
                        //console.log('img ',img)    
                        //$scope.selectedImages[img.id] = img
                            
                            $scope.croppie = $('.cropbox').croppie({
                                url: img.image,
                                showZoomer: false,
                                enableOrientation: true,                                
                                viewport: {
                                    width: '100%',
                                    height: '100%'
                                }
                            }); 
                            console.log(' length .... ', $scope.selectedImages.length,Object.keys($scope.selectedImages).length)
                            if($scope.multiselectEnabled && !$scope.selectedImages[img.id] && Object.keys($scope.selectedImages).length<4)
                            {
                                console.log(1111111)
                                $scope.selectedImages[img.id] = img
                            }
                            else if($scope.multiselectEnabled && $scope.selectedImages && $scope.selectedImages[img.id])
                            {
                                console.log(222222)
                                if($scope.activeCrop.id == img.id)
                                {
                                    delete $scope.selectedImages[img.id];
                                }
                                else
                                {
                                    //$scope.croppie.bind($scope.selectedImages[img.id].croppie)
                                }
                                
                            }
                            else if(!$scope.multiselectEnabled)
                            {
                                console.log(3333)
                                delete $scope.selectedImages;
                                $scope.selectedImages = [];
                                $scope.selectedImages[img.id] = img;
                            }
                            $scope.activeCrop = img;
//                         if($scope.multiselectEnabled && !$scope.selectedImages[img.id])
//                         {
//                             //console.log('11')
//                             if(Object.keys($scope.selectedImages).length<4)
//                             {
// //console.log('22')
//                                 $scope.selectedImages[img.id] = img
//                                 $scope.activeCrop = img;
//                                 $scope.croppie = $('.cropbox').croppie({
//                                     url: img.image,
//                                     showZoomer: false,
//                                     enableOrientation: true,
//                                     viewport: {
//                                         width: '100%',
//                                         height: '100%'
//                                     }
//                                 });
//                             }
//                         }
//                         else if($scope.multiselectEnabled && $scope.selectedImages[img.id] && $scope.activeCrop && $scope.activeCrop.id==img.id)
//                         {
//                             //console.log('21');
//                             //$scope.selectedImages.splice(img.id, 1);
//                             delete $scope.selectedImages[img.id];
                        
//                             console.log(' ppppppp ',$scope.selectedImages);
//                             //console.log(' zzzzzz ',$scope.selectedImages.slice(-1))
//                             //img = $scope.selectedImages.slice(-1)[0];
//                             img = $scope.selectedImages[Object.keys($scope.selectedImages)[Object.keys($scope.selectedImages).length - 1]]
//                             $scope.activeCrop = img;
//                             $scope.croppie = $('.cropbox').croppie({
//                                 url: img.image,
//                                 showZoomer: false,
//                                 enableOrientation: true,                                
//                                 viewport: {
//                                     width: '100%',
//                                     height: '100%'
//                                 }
//                             });
//                         }
//                         else
//                         {
//                             console.log('23');
//                             $scope.selectedImages[img.id] = img
//                             $scope.activeCrop = img;
//                             $scope.croppie = $('.cropbox').croppie({
//                                 url: img.image,
//                                 showZoomer: false,
//                                 enableOrientation: true,                                
//                                 viewport: {
//                                     width: '100%',
//                                     height: '100%'
//                                 }
//                             });
//                         }
                        
                        
                        

                        // $scope.croppie.croppie('result').then(function(html) {
                        //     console.log('cropping',html)
                        //     //$scope.selectedImages[img.id].base = html
                        // });
                    
                       
                
                // $scope.$apply();
            }
            $scope.multiselectEnabled = false;
            $scope.onLongPress = function(img){
                console.log('multi call',img.id)
                if(!$scope.multiselectEnabled){
                    delete $scope.selectedImages;
                    $scope.selectedImages = [];
                    $scope.multiselectEnabled = true;
                    $scope.selectedImages[img.id]  = img;
                }
                console.log($scope.selectedImages)
                // if(!$scope.croppie)
                // {
                //     //console.log('calling croppie',img)
                //     $scope.startCropper(img);
                // }
                // console.log($scope.selectedImages)
                // $scope.$apply();
            }

            $scope.preparesave = function(){
                var images = [] 
                //console.log(' active ',$scope.activeCrop.id)
                $scope.croppie.croppie('result').then(function(html) {
                    //console.log('cropping',html)
                    if(html)
                    {
                        //console.log(' before undefined',$scope.selectedImages[$scope.activeCrop.id])
                        $scope.selectedImages[$scope.activeCrop.id].base = html
                        $scope.activeCrop.base = html

                        if($scope.multiselectEnabled)
                        {
                            console.log('yes multi enable',$scope.selectedImages)
                            for(var ind in $scope.selectedImages)
                            {
                                console.log('++++++++',ind)
                                var value = $scope.selectedImages[ind];
                                if($scope.selectedImages[ind] && $scope.selectedImages[ind].base)
                                {
                                    images.push($scope.selectedImages[ind].base);
                                } 
                            }                            
                        }
                        else 
                        {
                            images.push($scope.activeCrop.base)
                        }
                        console.log(' -- ',images )
                        $('.cropbox').croppie('destroy');
                        $scope.croppie = null;
                        $scope.selectedImages = [];
                        $scope.multiselectEnabled = false;
                        $scope.activeCrop=null;
                        $scope.savemethod({img:images});
                    }
                    
                });
               
            }

            $scope.hitcancelmethod = function(){
                $('.cropbox').croppie('destroy');
                $scope.croppie = null;
                $scope.selectedImages = [];
                $scope.multiselectEnabled = false;
                $scope.activeCrop=null;
                $scope.cancelmethod();
            }

            $scope.getAlbums = function(){
                //console.log('getting albums');
                cordova.plugins.photoLibrary.getAlbums(
                    function (albums) {
                        $scope.allAlbums = albums;
                    }, 
                    function (err) { }
                  );
            }
            $scope.getAlbums();
            $scope.getAllImages($scope.selectedAlbum);
            $scope.changeAlbum = function(){
                $scope.getAllImages($scope.selectedAlbum);
            }
            // $scope.library = [{image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAvCAYAAACc5fiSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4RUEyRkFFREI2OEYxMUU4OEZBRUM3MTRBMzhGNDExNSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4RUEyRkFFRUI2OEYxMUU4OEZBRUM3MTRBMzhGNDExNSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjhFQTJGQUVCQjY4RjExRTg4RkFFQzcxNEEzOEY0MTE1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjhFQTJGQUVDQjY4RjExRTg4RkFFQzcxNEEzOEY0MTE1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+JcrZhwAAGCNJREFUeNqUWgmYVNWV/u97r/alq/eVptmhG5odBGQTFDRRiUrGLREzuIeYhDHquGTUmEWNxlFJTCAx7nEjxi3jhiwisi9KN003dEPvWy1de7337vyvqme+cRLzGb7vdr16VXXfuef85z//uRfxyv35+MI/CaR1gdFjTV7bsOWQi7cE4ikFk6uDiHQl8NzrEsUFQCpjw8xaHRetcKNuYh7eerULexsy6I8rcNokWk8JnH+Bcu/8GdjW062/39MDuFCIijFF+LwzAr+nE6apQkod+d5yhPURcDlOIRoOwaYJGiPwZf80/BP/0mmgKF9B3TgBr0fC5hDV48YY/6Lr6oPSFIjEDcyb4kAg4EZHj4KrL5HnTV4cvKutWTlaEPfXtbcmkcmoGKEqKCtMfsfutG2WUg3aNQUiY8dgSn5lW/4pwyNDJubP1OD25aM3aMe4kYn6ybXRB3p6k+NjCd81PpcNDadSaNpu4qypzsrxY+JvIWjAnZa1gTLn1J1btUOax6gcNbb92dLE0BK3p+Illws4fUqit8cJE8ZXtkX5sg8Eo2SaFhwEMrqCZEZBPKlCFQaaToSxdU+cq46s9eYBbhldm0h27yjwqWN2NQrsO5kpXXV1dE+sMYM3VgNNvzXRdWrwsUnTUz84Z9lAi80ML4FdQh9Kjxno0nHiJJ8lBZ9hRzKtIp2RfOYXRzpj2UPQCp3WGV/ucctoTQXyPDo8LhWlBGOh1/iGCfFe54AehjpUNn1O9MLevwCH/gBMWJdYMOkM0biiT1tdUqZfZxtllnc/AexvA2qiQP0N6YUTJ6cXdj4O7N0BFK+m4WXRpQ2teYfcLjvy7WZ+ecHghQWB6NRBDXcqCmIYRrnMDi6Ai9J1BxRFfrnhiRQwcYSC2aN1vP5JCB63Js+dl3zZZ9f7p9c57vZ5M2eobomu7UBzI1DwPjBivtQuPz+z2ennBO1AZzPhxcvaywR8iyXMD4HG3xIavCl9gO+q9Hw7YsG6seHrfIXp+gma7oknFHjU0nsMosYy0GTumNKBPG8IJ07Z0BUshMOhQ7M8+wWIyJy3desHxMvIIg3FDjt/IEwjrmzPnDIXThhIbJj6NX45BDhpQD7d0vUeEL6YC6hjIOkrlePUUXqbpDX7Bk6a4PuXuRgabT0yUAGMrtRXjx8TWZ3pBJK7OddMvkbUPjPtTSicU89otEMi39MPh5qEbtizcLGGoqgm/u8Q2VdDqIoxOZWBP5YWuPSsUiyeVoD+gUy041ng/V/SSHoU5TTAm0uUZsLhxGtcOJNNsfMGDQ/309vWAsdy7Ke33+E9XhbaeGsy4KkSEH0CTfcBe24DTnJu1SYLTvXmFza2VaGz1wevOgivMw7DYmdp5Z7MDm38BNcXaZxJwk9EQd7gbkWPpzWvbX+gINUyJiVaNBfGe2YIdH4k8flHwPzz6OHxpElOSLvRx3uZFsDGeyY5G4xG6dQcUPuJ6xMDOW9XjiSfT2NkuIr9d0u0H+Qc1vr4PXeRodpOtlRIzd5Z4uuDy64jYzBs9PgX6DCd9A0bPLwaui+j20xnUWyb35dccfxlfWn/UGJp7Tf5ZSc/O19gFfG/l9CYSSg4lgAV9GgH8RwapPEH+H4WEB+i5wOAv4qTcxHNNJwBQBFH6SL+OQPofUGi4WBuMdUlwJhVRJSuGDUV8f0wBqBYrMZkdDv+thApvd0hZEdPCP1dUYS7Y2hpiqC9R2y2041tfyRrPElD7+ID+uipYomFNwOzzgcaNnMGenfywlzmH+MY3MM/ZKwhejNJGDlYYdHKeRpziZpHpqpYzgt+dozf7eKlm2PurYzURCKsV3szmVCKySqrdQOXS0OOzeYhkaAbpGbSsjUUqcTIilG4vQlIexKNrToK3SpCPe7/Mn0KRp+RtQPbdwJ/uZ7GWIaRU+dfxAg5uBhycPmF9CL5icyInfv4hxTIWoXT/Fz18D3hs6Mb6LbWOZfemk9PfgLsfk3AYTn/ciY4B4LEvJ6ZFdBiXfk+vFQyFs/5StPHpRk+qNnSm/PyjFWB/AzyC9LMQ5Zfa7gcXIVDYnvTENqDcYSHnK39Mee+if8OTJpAIzjvIT75rzdw/rf5hji1MT3eeprXK4ApS4HPmaaPtiswCCEnC1NjiLwbFThCbz9FoFuOm7o6l9R/fkBBAzF/xhhi+wc5hop2S/g8ZmX5dAg/YRans0DH5FcYUzOGXMVg9ZQUxFBC4xXJOEjTgPVqEXxfSENPMKONrIpcocR0pyQnn/kocP5ZIhvqfcyR926hp18lY9CI50lxrz/CUK/L4fcojd/aRMyWAZ8NCnTQ29s7rOQVqK0H7FcA7z6sYP2fBUoJsDm35RaSIgxVRk3lAlpYpD66GtjyPTLO/bDgjt6Q/bpURPnE70qhsXEI6pVf99ATGoSm+koKUw/VVupXL5meenjMqOhVvoBRIlQrm4BxTJya8QKnjwlsD9IRfyVceG8ceXvJzQouWUlvqRJbGjQ4/AIKleNH+1Usq9OxaZeCEx0qXvqpCQo/LL5WQb80sYGLqFxDJ3C+6BHg+CtM8s8Ei59AYJZAOXPBWU1cu9Ww6i1ePWnCaPgLDBw8EoZW5PfCsICvq0ZN5cCF086MVx//MfABK2JeDVBGvh1BFrDTwElrJSYtE9h4D3DNCwI7zwZe3SfxkyslVt6s4t4rBZbWCHR3uLC20YvZFXG8slXB0SaJ1bPTKCK8Zn1HRYRJds9kiXoLIr3AgY30MnNj/NcZFULOX8NUJ4OBTsswyXsapdNmRvak0+0fR5OR0zZNeU7s/UM1HCKVFVWegN9dM+XUYNtDKcenG3K4Zh6C8hmjSHuT6GHPubxBpnhwrcCPNqm4tF7HLdfZcdMvHJZ8R1lARZBVMs1q29KXgZ7WsLyC/OmTsPMZL27LYNG4JD58DFnj7lsv0NoA3PW0RI1VrLpy9JlgBILMjU6OJNmt7BtkqElWVXf1nGyzzVKvvrgaYbOOSVmK0y3NmZI8zTnyG6lF1cSjRl4+dYoLiHMw4dpe54SfMwJMvAXreY8M88wODZ3dHtjLAtjZrCNEJdfca2D9OUMocKTRR1m7t1uFYbNRCquYGjCwcKEfhQUC3/lBBk81KHidhWvEDM7NvGl7kZy/ibBhFW3bxkgcZ20gS5UxWt5phUfTmbGjE6neiOaym+iMVqElTEFlf+fnnUeUVZUlCnyrDZxJiExgoTn8JjHNyY+zjP/pDdLjGwLfuxPYdL+JAtWGN3cbKNbiWLMwhRJPgjJAoopJvXwlH0h1GZVJMpeKB19xwTbFhd3Nfmx6KYk+djnBtykzWLyeuwkYOJiLsCUhPMP8Xs7CVHUBmWUZopG+gvmW6BK6DrHjmTMxECnyldj27Dhjeke91V5J6pOyeqtayJyupMczh4EjfyZXU2/c0yGyPcg3F7DjmZaH/3zHicGBMJ5eF0c1n5ZgzojsAwRiuolJ9NbL9F57grAIO7Ftr4o5VQbWrU5i1wETPyHDlJBhVgzLmtHFhCarb/kc8gI5Xy9X+1ubvHV6BL3UUewRVIiPnxl9x8SyzlvcSOadpHfbmSTtTJgklzxqscCMpRIl1TnezrqA5bF3F3AvGeDXb9sxvcqDqtEaLp41CE2YKHcpkGEzqzIFRYyDOI5SR3hYKSuKTPziHQWvHA7ggokS7+wbQmeYHRKNvnkE8PV5zKc5w/pmXM7tZjfrwDalPRxxrApUePbBTCOackJzKB2jPEWpPJBHT1ArN5Cu+nJyGu/tkqh8QKCYbnBQc5TOlagjy1SVC/zyuxYMVPzxUxuM9gQcswxUeOjhHhNu8rHJRHV6tWy7JzImxSI/y1NQziimUjq6GNkq5sqNZ0msWUxanGexwHAzGckVJJP8H+Fr5RijqlSL7+3qUO5WTOd9krJWC0ZL1rY0xR4bVzy4fepN8Hk/oBhitVs+TaCyLidRB1gA9nws8eLvBHo5cV9Kw8h8B0aWqmiLmbh2WgrFjEY3I5XH5DS9As58SZo12O0r0EOCBc5SkTZU+HTMG5nCnk4v5tW4sHppHBmqqXefIiJ7+VsC3GQu2QazcgYFAQkvaVkSPs7i6L1KsVEylMpbLz7+vQsenQ8u0kbZK/ybHKn+pR4mjTLCEkLDfGoNMhqYPCmyylHS07aDLvxpXxF0kcH1K/tQXcBW9xTLOjHtLqaxBT70kRarlDDMfoE0i4q0aUhpOn61hdU57EEhtf/WY4lsD2nJtGkshVMxbLDVbAyj0zkciLor6X1KjlCfmtYGjCXoUMbj057JJ8eEP3x+Wf2LS1Ems4UhQSE0SB4NnhAIn1bxeb+CNxmNPSGDDzZw2dQ4IpqGfc0axs5OoZ0h9jnt2X0YkTJQQgWVCdGFaZNcTB4vEsQ62zjeP2XYUFYUx8wJJhKDJspIJ3VcXCXzQuM67MMGu7gKF7Gex1XkU4m67ArSbs/D2kDQBa1wPtxKsHZa9V83mAxXF7m042PWARaGIBVbiARlaY39NIlCmV4wsfYsHaNLE3hmt5/dCcv0KHqKk0eH6PU+HV42rdJeDoetDNFkB5IigRFFBiNiMHFV9KbtyJCK9z4UzUoKDOWwHeXbJCWAksk1IGpebigMAX+CUJftakPkPaXZ5ABGuPb766s3fuhoCtp2sdy3n87lh6XmLP+V0djASAMXsGsZx5LsoDTt4HfufkxHLJJGyGND4YgkkoRToikNmZEY7BVwuWKIE79J0mvVKIHGHh1PESZ7+vOxpj6J1v4k6m+0YW29gfPPM1FDCe2l4vRqw+42c1ScJuajCfFZOCg3pKKZp5y+NDugNGrG5P92d6A4WKxzpX4SfhFXX8AC4qLCKyItlUxnmNgsYE5Oa3+fZfo3rHLnzlRx8SIPGoISt7+g44pFCUwl/584rkKNmETIEHQ5BBvLvQXSE6Srj074sZxirbkjifmjozi7XkG3oeIDirYz2yXyJkjY3LmmPUXDh1LK8658eZ3Lr0ZtNnb3HkKO/YKWTgxE3I7gms7mPFdK988oXNdzy+TKtA0US5a+gLXVwGQ1Tkl8cAewmRI3wXunX7I2GZO4+tEoIhEXmiP5ONqlIpmJodZvsPtR4PeYGKITigmF5ynaXtnrw4oZNuSlI3h6XxxvHVbx5DwT960iLkiHSYoqnaLKYAMaG361Keby1ialPqJrO6kHoVJUqfY4xKYfz8DC6U3sEZ2Uk84VkRM9b3Yczmj+YhXFo2kIebivheEekuxXJeZTCI3/PqHyjMCsm2zoTmXw7dlOiMoSNDQZiKdTmFIexbyJaew5Sc9Yziant0Z9aGpzYqwnjgdvDKKDDeil95LbmT//RkysGssaQcYIsHwaxHmITQuVAmUsncTr5lZtATuxnex5spuk6iVLFfaXmgh4w4+U+4OP25OmcpBdzeFPJTpYjrsaJYrIySvZWi1gX1hI/dF5p8AlP3Kg0TBx6xQDjz6kY9unZAeKq8oiBz5sddJoD8rcLkbBh09aPMgPuFBblMIb+yI4dFLigYeAa1nOtQ8kgjQwRt6OvMskZNEpnMI8YnMxQKWY4MirJHQMOdmpiY15hIrPRa8vpSoLRVURcIlan1N+rmhSHTtXVs6hiJ84m8ZexnEtq29xLlla7hD4469spHUdN5XruP0tYpFV8JqfUeD3pLDxhhjsTMYKLjbUk8S82jQW1qQZDR0Pfi8CZziDV/YRE53Aef8BLGOiu9metTN3rA2IGCVFaAuNn5DLqyPU/UHqHG8dqoQj/wmXxxXXbE6yjkkdLTSZzLgfJlTWRzPq3LDqmNsRtf2czLinoBw5DUDItz5MjP+GHE2Wucxp4EY2AOCC7rlVRU+SIZ9kYGqNAdUfw7qvDeCthiGEiefbbgxjT1sIDUfSeORn7JzogSfY8vX+ib9fQOMpNS5fb+2d5x7VQa/vYCvY8xr1zSJWYRs7Ih9rgtBvj5h5zAUPFIXpaw1VMaCxkhlpHZEwdvekArfLfM+PXbZc6xb6EHjz11aNM6m3DCz/Ke+fx6z/vcBnR9m5MALraESamFx/lQlnKrcR3MTP7GSaKxaZ+PmjXP1EzrOeuUKavY1Stn+LtalCDce8+RY1eO2c3K7CIN2//y5G4QQbjHVquGikmsz3x384EA6N6gzGctvM/7ulnNThcttRPboQ06cVon6Ua46gR+OHWOLvym7/gbUA89kn+i7P7Ze8/zQ5mgu5xylRfU4OjyUzySafm6iwNjzjzE568JE7JUYVEs/s3PNvZ8dDiESoge5jw33kpVxtd/D3S54Hzib+ywgVP+cqHiPRn9QOf7TNl5808ufH0pF0exeV6JBF8FyiSc4uLHDB6/ew21ZgZJJw5KdKUpSJ+36IbJNrbUrNuo6a+abcblIPH9h0JLsnhNqzcmLa4HwaqSxNJriIt2Zbe340vJiGTuSqX38SWEMDZz1AVqHR71JabKIjVl0vcMa3czq+5CJlaN5cMRRuMaW9RnE6I5mFoYy6yBx0vuug3vE4yef1Y3QaLjGRla20TEOMpTpDoWSYGmyqXkTxDnchPcAH1P2ImLvOanBVpA6bOPgyIcaQj+SwdqcsZSfMXPnWh7K7DlnBlKFj7MzHSVzhe+ygrtrP7xE+57FKd96Uq9K7fiOJY4niBQJ9e5U9gZHasmSehtZGzdEW1hfrhpEKtQ4OHzrYoRUXurNQ6aJHT/bKYdhosM50Zjlkfcl0gemPKAi3mvDPVHcMHhBOM2nOkqS0SJNlGBsN0ldgbq40a0rO8KzneWk1Eop1QZyNY7P7GivkTjYhC9jhu5kjKy4F3nkx1661WMk4jzK2LDMm1EVNo7mYcyLVMai+q8cp0NhEWJsvlo1KVJZhiDhJKWUQzvIsgQon0cnruHTUhE+L7pOD4r54CRY0H/Au7Dqt7s2rYKEhBYYjOSYoX5LToXZri9mVawKsImLltZ2Ga47csQK1FVs0NlGkt/7dIruyCcyds1fl5gm1UBa8wEcXKl6nz+30MVxW9dVUQbFG1Uh20VjRVJXX3T2pvz3/yZ5IKDClZ26VL3481Z9JuN0CnzQTh/WizEaW6T2U00D5LP+Vy5CVBltfVDCF7MEGPlv1LMMVJ3IalV/uOp2T9QW812Hx9ZASHTvP9NY9TkdSWba+zcixRUyEkHJ51Qx7EPh8JrzMkc6gkj3akcMbt0oqyebz/40k2cVimHRaHFZUJVFJQz85pmHj22nsbTG3WGGX9KrFGtO+RW8tV/H24zZ89x4+vEXL0ptVmi1aU/3DLfsgaZGNhtUYpC38V0i0nZC7Pv3Qt7j3uH9zzQ1KeuUzTOBvWxJBOm02m2mjtnd4HThnlo0SWsLpFMjz5Yamqn/vEFQgtxkq4WehOd2usIB4UVmh4GibfCnWbjwy9RZdSbBaeGoV7H9CYOPPmOnEw7GtCmasVFDAxjiPHnYHcpv2GhdbN0fiJJuTBKsm+wzkl8hgZ7fcFosVbdM0s9RZaFzsHlLW5HkSs7v6g9VSKm0WxtyEWh21jHUupSpf4ZzT+o6Nxh86TQSqDkwamUF/yNbdFnF8VrtQr4+eUH9yuk1WBQ/pa6xOr9JaMinECCsYQbXXfYDJy7Akpe2z5oPaY7XnJZ6cRolwsjnH4WqR+mxZgOLBlkAmLXr2fapsONJZvuHcMxOzU6nOqK4bWSusQweyoFUt8T9HuOqFS4r+rtHWJnpRQQKGkUF3vx0jig0U+U0EGGun05jrzaAhdFJ8V+imDBTjsgLibzJ1TzXZ5XCT2DxqrjK6fJrU7JMJcb9yx87t9t/1xM2+MxeZX/Oz+TUqRMjjE//qd+uEpuTzFGRMA31RNwoDWid7ruwBFqRinexkj3iyJ3DDQ/lHHneyterpJ5ee1NDXJdDVoSLYxyTda7/mRJtyuXV4OxBWG5x09dybydMUZMEMHj52xLyoNS6eK6A4kyWOpoF278ayMoGP3ndu2Jm23zl2LckrgFdTCSnjnMPrjrOopGHoahYKNjad1lFhlvaULxn/yHDrPMiKUywmEWYvORTNUSDn1K3jxH3NCnY3a8fDabXH2vB59g08tnkL1hdWl+JAQ/nvYsdYqJJ5a2zuMpjseA/1JnHrr3H/869rW30ecdjQrC03kd0sddBw+dWP8v8xxnMHWvySlhvWtXVwqmpmFkqlpQ7kZ1T0RRLX9xyIzzvdjlsLAhpcRWMRHHIe/qxpaG1R/uAnihaBjxy5aLE9+78wYoZYuf94LJC0erMsDFh0nLljQEWIr2T4fwswALW/cZ/5LJLwAAAAAElFTkSuQmCC',
            //                 id:'1a'},{image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAYAAAAehFoBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRTgxQjk4NUI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRTgxQjk4NkI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVFODFCOTgzQjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVFODFCOTg0QjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+S8qrQgAAEWNJREFUeNqcWQuMbeVVXv/e/36e55yZuXOfQCkvK9Ai2vbWS2xIUKok1NZAlJKmJqVK1NpgSaioSCNiDI21aHutViFtQgOKpsZoMRCrUkKx1XrBIr3c98ydO2fOa7/ffuvfZ88MlBTsyezsM/vs/f/fv9a3vrXWv0USD6n5CCFIUEWbo5CiuCJdF+p6VVUURREtLAxw9IjKGa6WuF+nsixp5k3fYlnLL9hOlzxv823Hj598qNfrfef888+/uSgK9bxhGBhPx3MFjTZP4X+TXMdVz+d5TriNTNMgoTsYb0bDs2cwRUVC02jn55X/vc4H68HkgpKUKMsxdSnI85MbNza85yfT8WHDKPmujy0tLV0ppbwpCIKPW5ZFtm3PwdZTBkH03rIs9rJBGI8QbxyD9sbBYnA1cgWgBg4T4G2Kk+InpOlSmsa3rZ098ZE4SS6yLFtZdDab3Q/PtJMkAciAUpzPbax+aualj3te/FcxvKiOJMdYpZrjBwJcscPhKnYnH/w9TdMLszxfkdJQborjANYdUV5Ei4YhyZA2hUH+OSztoK5rsJzGVNDG4/GHARwDZjSerL1vNNr8WKfdpzSrDs6C0EwyHl/84BZmsKIqAEASgzNNkwFfAgBH4zg+Nh6P7tGEht9sXFccS/l//ki43TRMobwBwEyDNM1uZDi6FHoYh19y7I6iAO5pZ/lk4LbAZdvB/eINWVh+D+CKByuo3Xap21tWA0VRWI5GI4CUjud5vwMX/6RpWjfYdnuEKB2nKQILVtJ1A/foKsj4YABFkb/TBI/zIr8FYWVLaVKWJYinfAqvnC1ysW88Gv6aYejHbcv6rJgvlJ8tQRMF6PUoIYSEy2Lyg3OIYFjbsL4Lix3n3xzHYRAHfd/7zywryXU7jwrBk9QeUUojhJoUC+T5LHjmHXGS/ZJp2HMfKsucSZL8rrWzqy9FSXnnzAv/dDoLlNfCIHh3lmUX1FanVxzaawWXmky3KIiG4N0JWIRBm48xl/k3liQs4IDv+0+UBZ2wbWudB2caqEHnVuJDU3wPP1MU5dulbszdXjHsS5OsvM80Wo5jW6RreuKF8b1n1s78O1TkqZK0f9AwjzQtksb2IYp8zDDnklVRkqSLm8PJ+4VmzzQjfTovJifb7jI59r4Dnjc9qWmV8hKDD8MQz2lPuK61gP9/VNMksYw1C2d9nU5nuDendqeF2NCZGnhuws/BW10qcE+Wx6Tpeolxedm4v4DB5DcXBoOrSwS9igm9Zq/0Zl69YtzdbrcpiePr/TA9bFngYloWmt5+bDLZ+DNz2Xqy2118wA/8OyACSrZ4IHD6Ot/PqdVya96pQBRbwj0PsJp96h9iSpFlOsr6WZWTCRnEYrUKz+Z5Bo/m1Om2PwmD4n8YB/enSaj4LI6//G2MU+FCTo67QCt79tL6ubWvIpCuYxeCS3gImYKChwYLyw9Y1uIjWN5beJE8IWjBeotnbVjMpZrP+jzgCvJY0nCv4+I3gE7SiPzZkDrtReXuCorEnuF7GWwcJ0g08pn+wD3Ic0tkviQOabJxWq1XVjA/WyQrskPJ1LtdM8anYe3T0NfMNCzDQZbiRJEk8oPr6+du6PfL9V5/RU3EXuEsxokhS3MEWab43aTzan5u9BKKC0C+Ashga+vLuaqUyrJsT9vR3s/BnsQlSRfSKpm/pjKCbHUGSj+zYnIDUu7PJ3GkVqtrdolgUZHPmcu2XdbkRT+YLpqWA3XognsZcr9OTqtFs/FY1Rts3aJUSUPxnA+Fly0OT+VprLgLzkIKt7MbWzOB23s99xDGXg38mFjVOAmV1Gg0zo0FQPTWqdPHnwNdLrNMpZuK/DUdBNkA3e70YaGIonBGvf4SGQgwlQkxhg/X82ItWNzC4mrABSQqVFrugONhwBmvBD8HyuRMEcYRRiGF/szrdlvXLy7tehq0OOQHs3flRXaR1M1enmZ5Egz/wjT1JyWKFjVgu+UGS0uDd6+tnf1WUck90F7lJg6INIlRC3iwQESd3oAcpNYAk7dFD3RCxQbQEkEYw8JhUlIFbmu6TTm0PC8SZKdULaaAFVvt7lZSYdKEYQCwno+A/3Knu3Cl54dfgFEu5SJLCBMGqygIY1hWpJZpPymOHn1ZpV8uG9kiRZG6aZb9Y5aW13Ay0OblHUe278/A1QQW6pMOTvF324I1EWgptDpODAo3XyQRnyAJ61fWPsqDdTIXr1KBbWghrN/CYmrFiANfBZTrtqNOtwcKZ12mEDKBwsJHijmQBf971/Luaw1pDsXJkycVYBdRXJO+UhZPs/Q3YbmPIlstq7q34ioNQRMFFHhTTOwgcGyVZjmSpbWHRme+QrMjf0At7Shp0NqstZ9jiJwDN1Li3kyt/nkENVO1SoRxmJXtThcaX1OIA5OpyDTMspQB+4ZZPbi8vHKX6wxovDkkiSqMo/y3wMWfhpt24ayxSkCeXiiKbIREsgTQgq2tI9XCGpgRcgbQGp61uvsp2/xfOvGtT5ARPUYW6MNO0TJI0PQ4rA/Njh8k48LnKG4/REUMT6arZCBO2FMmxwvEtil7MCcMUxtOGvIpw9T/MgxSQxNBliQeiWPHjglkrLLT6ShLcxA1H04ChVpxTjsrMEbEUZ2mFQ2PfYNGL36a+v7T5OInFvsyF0gMAAzg7P08QUGxRBQvX01T917q7X0rrIo0P1cIpduvKg9U0FYlDJ0bWECia+LZMk9/X+7evbsCyKun0+kjAHZxk1rrukDMq6ZKaS2yoDqrwUATndPsy5+n4NjTdPGFRFEqaBIJMlmfATTjtSGjajb4OqzITP+DWm/6O8qSKxCIkC0YiMvYujUjlXSabMkBjz9jXo9Ynje7Jk+jTPiQnZbrIIAKOZ6MP4EH7oD2desiSNsOOuYwrJ+lGfq9CCnVpdF3/olWn/kQ7e9kAI+JGCR4yGCLAveDFi0XkogireIUizH0/Ss0W/lzau26CtcrBbhpn8Scx0yJpnmoZRNqZRj/bEjtI1JUCaRF8Sa3LHEvAN6LlV6LZ38OEXst7HPJ3En1wCaA8XexCB6iHAQ1oGbkwJItW9Gb8C9tBIbibxnlXNGQC8chixMlY1AvgqfQ0eQ+AlbjSk3VGGJHZkRSKTHW19H3fc22zCd6vcWnEkwkuViPUnTJyRDcq7gQbyMBnsT5CKTthzRpXsI1gqbpymUF8zoH5GoEtRjjmkGoOLbqbKaAqmkTyB7ub5sIFBT33AVBiMirelSEPrntRGk8L19JGMZkxCxrtU5rFdZ+3LLNw/1e/0SlFMrnGhscSjNkE+NDeVb9AoD+WNMxMEiVrSDumrKCUC6rJQ76ufddJCFLebQJl9XNAQPWwWXTgoKAJi32jaYSHFk8hI4U7+yifn9AtoPWX9XRYqtLYcAqXpJYh4LdEoXFLWPynzSkuL2q8hflbDa9eTKNHkRML7FrhCG2Kq46cdT8KlUxw0ALpRxVKak3OI+GnV0029wEF8FH0IW5KiE0ywaKIdxvSygGtLdAho9Z6nQYwj2ALw4kGrWHZqjU38QKG5DVijsbllx0K1yjXDsMJs+6lt6XG8PxIwhC3GDVUdq09PNuoVaeqr7e8IutX6KQQZSJ7nU0zv+HuhUXMPMmFtbswdKVLoiLNy69U9AkYZ46h4C+RXE4osqoy1AG2ajSzn6QQavqD9/RmD/q2malLS8OPmhZ8mtJ5kPyfKgBNHe+wQGECizXCkWeKMsyRWrX4Tt+33XBe6i7jM5BsJ7yPgNHOe8LKbkmF2gTSEfIertyOenLv07dQR9jwnqoTdAAqD0LpkGzpdAcrPUsb1zidrqLb13cdeDtIo03kQ4NGo039gDeRaalV3mWvwcN4i8D9kKtxbpqUbiOrfcsUMVhMPRpSr5o9hKtff1OioZHaLFDtZxZtbX9ADKHa3L3j5B92R9RZ987yBJTVTQV9X6HapOaxMSdTN3M0lYSY3oyeNWSBd5ZWLKNh1FZlR5KQHS4UXk/gu8QUrHkqo0PpoticVXvhTXRzLRIM4NmJ5+j9PTDlA0fJZsbUS6IqAX9cGjl4uupddl9oG0L/EQXoQkFysDBGlsHWaLGbegAkDm4jG5dfzPmMjDvGfz+uIggMYYBvRERra6deBil6a1SbaKY8w08WYPlzDOnR51HNXgGCgJxZfBOdw/Fo1Xyzz1DFm1SOPPJ7L2ZuksIsNZFZKMktRCIXI5zILM1a97WccPNAJSh7l4ytnqR2Y79UK/XudM0nTa8vArAhUhRUCDkjXMbo8fDMPkZG0TnQdhCXAbWe16ktqfqDlZXAcnuDMA/1Sah42D/x3FKujVQ1E/9U1CcejOmQFecpz51u72tfq+pGXaWAex25jNbmgfkBUD67t6397zfYwOxYogoWEUr7t80HIVfdt2OylTc49WNL6dV51/w7AIGu7Lmlqb01oNlKwRhq9tVrb0P8LzZ57baip9s/QxBheeJ9x04uFiuuMja6vmqakvOdm4NeP4Mho4sLuJT9G2WqX0AY3yJjSS5CXRb7jd6ufZFZLHlokq5rDmHAHu53e79tWnYNrrihzngmuIkCGqwbYDlfi5D4HAQsgxZtqWyBFdpZaGrFt0w2good9e8uKbAovmm4yu2orBY1MeBIfW7sJ7ONE/3oSV7viostUCpS51s3TqmCXlrCfZF7JIyozY4xxweTzbHVan12brsJnYZu64FoAyQS8RgXsExnQxuLjVV/Sj3l0UdpE0i4G2BOkbklpX5+ra1WRnEwHE6Bzud9k2AR+NRqlow5YWUa4iSW2pUSJWpmkxu3QFQjsabx1Er7ONorlQ0x6oracHtvOmiAMFCk+lUBU6315vvIVMd+QieCoB5PF4gg0IZq85s8YbDTTrm680WF3fg/X7/o1joH6dpRM0OqVbv+tSdL1uxhy6gg0bR8yeHs6zYBxeoNohbft4r4HafAewMFG4u2br6nI81F1O1cafNi/1ibnEGygDZ0vWrgqLJdoBQeWxtPpR3x+NPB0HY5kxccp+HQxomXA/QlqOp5o8/aRq/LQzTX+TtJHwHP3POt981TfeAaRoWg+CJ+MyWYRmRW68EaGshzEc2Qr17tJ0cut3uKQBGkx1tNQw8Hqz5J1jUf0ENbsX9l+O5Nd/3eLt3q+wU62snt3cUq1JZaTSZHgbG25h3sP6q41q/iubnQJ6VfwjA/Gl22Dn7nMN5F7t8250hTcZDWJ0Tha0WDIYpz8y7mBHGeCdAfxwL/jCrx3wxzy8sLFzOwPAbt24VN8fVjj1iORuvf8+LF0zwGU2667ZlvySN6lG05jEawVNClMZ2269cfAcmugrXPrC9IcNdQ/EwSsELUdkdUi6fl49sZQ42nAcAecVgMLgNcncGwO6pt2XjH8YQ1wHkExW/b2Dtf5WKSO5eXwGYD107IqV9pN1uqd2dNMnfm6be/sZCLOCY7H58/xQmfH5nxDNg9ogf6L+BhR/SBbIbSsx8ztWmIgPIm+CVv8Hxu6DFF6E+t+Pam0CDE0yzsnztlzTyNV/KzPcHSgg3JI2V48fVnhmnYhQgcBsyD929tramwbUXNANzsCD5/CtsMCur9EXWYU7nTAldimeFsM6DdXfPLbfSVGdY8FGozh3sAaYAU4SNUmc8eiOvDGAJJIogjBCpI5pMJuucqTD4GUjNz4LbdzM4TMQBmDWu42soBW+v1Paq/HsWGN7QU5sjWfYF12n9VL11oALW5YBjcA3FGol7NQ1e9y1SXdIZqjnk9gWDPoBFXLOysnIpZOlvefU82Z49eyJc50PpJhbySRQSR9iyi8v7p622e0/KL2CYJk5rE/9/G8/fxZIGt7+gunAoyPcD+P94sbgdmaBDBa79Gywa1EV1vU/BwGHVwxsbGx5c+XlQ5bdVkKn9U40GvYX7pKy+iTQ+7rTsrxZ5BJc79+PeK7DIX6k3Y1IFemcx9P0+/yfAALYMc2fGWQOJAAAAAElFTkSuQmCC',id:'2a'},
            //             {image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAYAAAAehFoBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRTgxQjk4NUI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRTgxQjk4NkI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVFODFCOTgzQjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVFODFCOTg0QjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+S8qrQgAAEWNJREFUeNqcWQuMbeVVXv/e/36e55yZuXOfQCkvK9Ai2vbWS2xIUKok1NZAlJKmJqVK1NpgSaioSCNiDI21aHutViFtQgOKpsZoMRCrUkKx1XrBIr3c98ydO2fOa7/ffuvfZ88MlBTsyezsM/vs/f/fv9a3vrXWv0USD6n5CCFIUEWbo5CiuCJdF+p6VVUURREtLAxw9IjKGa6WuF+nsixp5k3fYlnLL9hOlzxv823Hj598qNfrfef888+/uSgK9bxhGBhPx3MFjTZP4X+TXMdVz+d5TriNTNMgoTsYb0bDs2cwRUVC02jn55X/vc4H68HkgpKUKMsxdSnI85MbNza85yfT8WHDKPmujy0tLV0ppbwpCIKPW5ZFtm3PwdZTBkH03rIs9rJBGI8QbxyD9sbBYnA1cgWgBg4T4G2Kk+InpOlSmsa3rZ098ZE4SS6yLFtZdDab3Q/PtJMkAciAUpzPbax+aualj3te/FcxvKiOJMdYpZrjBwJcscPhKnYnH/w9TdMLszxfkdJQborjANYdUV5Ei4YhyZA2hUH+OSztoK5rsJzGVNDG4/GHARwDZjSerL1vNNr8WKfdpzSrDs6C0EwyHl/84BZmsKIqAEASgzNNkwFfAgBH4zg+Nh6P7tGEht9sXFccS/l//ki43TRMobwBwEyDNM1uZDi6FHoYh19y7I6iAO5pZ/lk4LbAZdvB/eINWVh+D+CKByuo3Xap21tWA0VRWI5GI4CUjud5vwMX/6RpWjfYdnuEKB2nKQILVtJ1A/foKsj4YABFkb/TBI/zIr8FYWVLaVKWJYinfAqvnC1ysW88Gv6aYejHbcv6rJgvlJ8tQRMF6PUoIYSEy2Lyg3OIYFjbsL4Lix3n3xzHYRAHfd/7zywryXU7jwrBk9QeUUojhJoUC+T5LHjmHXGS/ZJp2HMfKsucSZL8rrWzqy9FSXnnzAv/dDoLlNfCIHh3lmUX1FanVxzaawWXmky3KIiG4N0JWIRBm48xl/k3liQs4IDv+0+UBZ2wbWudB2caqEHnVuJDU3wPP1MU5dulbszdXjHsS5OsvM80Wo5jW6RreuKF8b1n1s78O1TkqZK0f9AwjzQtksb2IYp8zDDnklVRkqSLm8PJ+4VmzzQjfTovJifb7jI59r4Dnjc9qWmV8hKDD8MQz2lPuK61gP9/VNMksYw1C2d9nU5nuDendqeF2NCZGnhuws/BW10qcE+Wx6Tpeolxedm4v4DB5DcXBoOrSwS9igm9Zq/0Zl69YtzdbrcpiePr/TA9bFngYloWmt5+bDLZ+DNz2Xqy2118wA/8OyACSrZ4IHD6Ot/PqdVya96pQBRbwj0PsJp96h9iSpFlOsr6WZWTCRnEYrUKz+Z5Bo/m1Om2PwmD4n8YB/enSaj4LI6//G2MU+FCTo67QCt79tL6ubWvIpCuYxeCS3gImYKChwYLyw9Y1uIjWN5beJE8IWjBeotnbVjMpZrP+jzgCvJY0nCv4+I3gE7SiPzZkDrtReXuCorEnuF7GWwcJ0g08pn+wD3Ic0tkviQOabJxWq1XVjA/WyQrskPJ1LtdM8anYe3T0NfMNCzDQZbiRJEk8oPr6+du6PfL9V5/RU3EXuEsxokhS3MEWab43aTzan5u9BKKC0C+Ashga+vLuaqUyrJsT9vR3s/BnsQlSRfSKpm/pjKCbHUGSj+zYnIDUu7PJ3GkVqtrdolgUZHPmcu2XdbkRT+YLpqWA3XognsZcr9OTqtFs/FY1Rts3aJUSUPxnA+Fly0OT+VprLgLzkIKt7MbWzOB23s99xDGXg38mFjVOAmV1Gg0zo0FQPTWqdPHnwNdLrNMpZuK/DUdBNkA3e70YaGIonBGvf4SGQgwlQkxhg/X82ItWNzC4mrABSQqVFrugONhwBmvBD8HyuRMEcYRRiGF/szrdlvXLy7tehq0OOQHs3flRXaR1M1enmZ5Egz/wjT1JyWKFjVgu+UGS0uDd6+tnf1WUck90F7lJg6INIlRC3iwQESd3oAcpNYAk7dFD3RCxQbQEkEYw8JhUlIFbmu6TTm0PC8SZKdULaaAFVvt7lZSYdKEYQCwno+A/3Knu3Cl54dfgFEu5SJLCBMGqygIY1hWpJZpPymOHn1ZpV8uG9kiRZG6aZb9Y5aW13Ay0OblHUe278/A1QQW6pMOTvF324I1EWgptDpODAo3XyQRnyAJ61fWPsqDdTIXr1KBbWghrN/CYmrFiANfBZTrtqNOtwcKZ12mEDKBwsJHijmQBf971/Luaw1pDsXJkycVYBdRXJO+UhZPs/Q3YbmPIlstq7q34ioNQRMFFHhTTOwgcGyVZjmSpbWHRme+QrMjf0At7Shp0NqstZ9jiJwDN1Li3kyt/nkENVO1SoRxmJXtThcaX1OIA5OpyDTMspQB+4ZZPbi8vHKX6wxovDkkiSqMo/y3wMWfhpt24ayxSkCeXiiKbIREsgTQgq2tI9XCGpgRcgbQGp61uvsp2/xfOvGtT5ARPUYW6MNO0TJI0PQ4rA/Njh8k48LnKG4/REUMT6arZCBO2FMmxwvEtil7MCcMUxtOGvIpw9T/MgxSQxNBliQeiWPHjglkrLLT6ShLcxA1H04ChVpxTjsrMEbEUZ2mFQ2PfYNGL36a+v7T5OInFvsyF0gMAAzg7P08QUGxRBQvX01T917q7X0rrIo0P1cIpduvKg9U0FYlDJ0bWECia+LZMk9/X+7evbsCyKun0+kjAHZxk1rrukDMq6ZKaS2yoDqrwUATndPsy5+n4NjTdPGFRFEqaBIJMlmfATTjtSGjajb4OqzITP+DWm/6O8qSKxCIkC0YiMvYujUjlXSabMkBjz9jXo9Ynje7Jk+jTPiQnZbrIIAKOZ6MP4EH7oD2desiSNsOOuYwrJ+lGfq9CCnVpdF3/olWn/kQ7e9kAI+JGCR4yGCLAveDFi0XkogireIUizH0/Ss0W/lzau26CtcrBbhpn8Scx0yJpnmoZRNqZRj/bEjtI1JUCaRF8Sa3LHEvAN6LlV6LZ38OEXst7HPJ3En1wCaA8XexCB6iHAQ1oGbkwJItW9Gb8C9tBIbibxnlXNGQC8chixMlY1AvgqfQ0eQ+AlbjSk3VGGJHZkRSKTHW19H3fc22zCd6vcWnEkwkuViPUnTJyRDcq7gQbyMBnsT5CKTthzRpXsI1gqbpymUF8zoH5GoEtRjjmkGoOLbqbKaAqmkTyB7ub5sIFBT33AVBiMirelSEPrntRGk8L19JGMZkxCxrtU5rFdZ+3LLNw/1e/0SlFMrnGhscSjNkE+NDeVb9AoD+WNMxMEiVrSDumrKCUC6rJQ76ufddJCFLebQJl9XNAQPWwWXTgoKAJi32jaYSHFk8hI4U7+yifn9AtoPWX9XRYqtLYcAqXpJYh4LdEoXFLWPynzSkuL2q8hflbDa9eTKNHkRML7FrhCG2Kq46cdT8KlUxw0ALpRxVKak3OI+GnV0029wEF8FH0IW5KiE0ywaKIdxvSygGtLdAho9Z6nQYwj2ALw4kGrWHZqjU38QKG5DVijsbllx0K1yjXDsMJs+6lt6XG8PxIwhC3GDVUdq09PNuoVaeqr7e8IutX6KQQZSJ7nU0zv+HuhUXMPMmFtbswdKVLoiLNy69U9AkYZ46h4C+RXE4osqoy1AG2ajSzn6QQavqD9/RmD/q2malLS8OPmhZ8mtJ5kPyfKgBNHe+wQGECizXCkWeKMsyRWrX4Tt+33XBe6i7jM5BsJ7yPgNHOe8LKbkmF2gTSEfIertyOenLv07dQR9jwnqoTdAAqD0LpkGzpdAcrPUsb1zidrqLb13cdeDtIo03kQ4NGo039gDeRaalV3mWvwcN4i8D9kKtxbpqUbiOrfcsUMVhMPRpSr5o9hKtff1OioZHaLFDtZxZtbX9ADKHa3L3j5B92R9RZ987yBJTVTQV9X6HapOaxMSdTN3M0lYSY3oyeNWSBd5ZWLKNh1FZlR5KQHS4UXk/gu8QUrHkqo0PpoticVXvhTXRzLRIM4NmJ5+j9PTDlA0fJZsbUS6IqAX9cGjl4uupddl9oG0L/EQXoQkFysDBGlsHWaLGbegAkDm4jG5dfzPmMjDvGfz+uIggMYYBvRERra6deBil6a1SbaKY8w08WYPlzDOnR51HNXgGCgJxZfBOdw/Fo1Xyzz1DFm1SOPPJ7L2ZuksIsNZFZKMktRCIXI5zILM1a97WccPNAJSh7l4ytnqR2Y79UK/XudM0nTa8vArAhUhRUCDkjXMbo8fDMPkZG0TnQdhCXAbWe16ktqfqDlZXAcnuDMA/1Sah42D/x3FKujVQ1E/9U1CcejOmQFecpz51u72tfq+pGXaWAex25jNbmgfkBUD67t6397zfYwOxYogoWEUr7t80HIVfdt2OylTc49WNL6dV51/w7AIGu7Lmlqb01oNlKwRhq9tVrb0P8LzZ57baip9s/QxBheeJ9x04uFiuuMja6vmqakvOdm4NeP4Mho4sLuJT9G2WqX0AY3yJjSS5CXRb7jd6ufZFZLHlokq5rDmHAHu53e79tWnYNrrihzngmuIkCGqwbYDlfi5D4HAQsgxZtqWyBFdpZaGrFt0w2good9e8uKbAovmm4yu2orBY1MeBIfW7sJ7ONE/3oSV7viostUCpS51s3TqmCXlrCfZF7JIyozY4xxweTzbHVan12brsJnYZu64FoAyQS8RgXsExnQxuLjVV/Sj3l0UdpE0i4G2BOkbklpX5+ra1WRnEwHE6Bzud9k2AR+NRqlow5YWUa4iSW2pUSJWpmkxu3QFQjsabx1Er7ONorlQ0x6oracHtvOmiAMFCk+lUBU6315vvIVMd+QieCoB5PF4gg0IZq85s8YbDTTrm680WF3fg/X7/o1joH6dpRM0OqVbv+tSdL1uxhy6gg0bR8yeHs6zYBxeoNohbft4r4HafAewMFG4u2br6nI81F1O1cafNi/1ibnEGygDZ0vWrgqLJdoBQeWxtPpR3x+NPB0HY5kxccp+HQxomXA/QlqOp5o8/aRq/LQzTX+TtJHwHP3POt981TfeAaRoWg+CJ+MyWYRmRW68EaGshzEc2Qr17tJ0cut3uKQBGkx1tNQw8Hqz5J1jUf0ENbsX9l+O5Nd/3eLt3q+wU62snt3cUq1JZaTSZHgbG25h3sP6q41q/iubnQJ6VfwjA/Gl22Dn7nMN5F7t8250hTcZDWJ0Tha0WDIYpz8y7mBHGeCdAfxwL/jCrx3wxzy8sLFzOwPAbt24VN8fVjj1iORuvf8+LF0zwGU2667ZlvySN6lG05jEawVNClMZ2269cfAcmugrXPrC9IcNdQ/EwSsELUdkdUi6fl49sZQ42nAcAecVgMLgNcncGwO6pt2XjH8YQ1wHkExW/b2Dtf5WKSO5eXwGYD107IqV9pN1uqd2dNMnfm6be/sZCLOCY7H58/xQmfH5nxDNg9ogf6L+BhR/SBbIbSsx8ztWmIgPIm+CVv8Hxu6DFF6E+t+Pam0CDE0yzsnztlzTyNV/KzPcHSgg3JI2V48fVnhmnYhQgcBsyD929tramwbUXNANzsCD5/CtsMCur9EXWYU7nTAldimeFsM6DdXfPLbfSVGdY8FGozh3sAaYAU4SNUmc8eiOvDGAJJIogjBCpI5pMJuucqTD4GUjNz4LbdzM4TMQBmDWu42soBW+v1Paq/HsWGN7QU5sjWfYF12n9VL11oALW5YBjcA3FGol7NQ1e9y1SXdIZqjnk9gWDPoBFXLOysnIpZOlvefU82Z49eyJc50PpJhbySRQSR9iyi8v7p622e0/KL2CYJk5rE/9/G8/fxZIGt7+gunAoyPcD+P94sbgdmaBDBa79Gywa1EV1vU/BwGHVwxsbGx5c+XlQ5bdVkKn9U40GvYX7pKy+iTQ+7rTsrxZ5BJc79+PeK7DIX6k3Y1IFemcx9P0+/yfAALYMc2fGWQOJAAAAAElFTkSuQmCC',id:'3a'},
            //             {image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAYAAAAehFoBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRTgxQjk4NUI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRTgxQjk4NkI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVFODFCOTgzQjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVFODFCOTg0QjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+S8qrQgAAEWNJREFUeNqcWQuMbeVVXv/e/36e55yZuXOfQCkvK9Ai2vbWS2xIUKok1NZAlJKmJqVK1NpgSaioSCNiDI21aHutViFtQgOKpsZoMRCrUkKx1XrBIr3c98ydO2fOa7/ffuvfZ88MlBTsyezsM/vs/f/fv9a3vrXWv0USD6n5CCFIUEWbo5CiuCJdF+p6VVUURREtLAxw9IjKGa6WuF+nsixp5k3fYlnLL9hOlzxv823Hj598qNfrfef888+/uSgK9bxhGBhPx3MFjTZP4X+TXMdVz+d5TriNTNMgoTsYb0bDs2cwRUVC02jn55X/vc4H68HkgpKUKMsxdSnI85MbNza85yfT8WHDKPmujy0tLV0ppbwpCIKPW5ZFtm3PwdZTBkH03rIs9rJBGI8QbxyD9sbBYnA1cgWgBg4T4G2Kk+InpOlSmsa3rZ098ZE4SS6yLFtZdDab3Q/PtJMkAciAUpzPbax+aualj3te/FcxvKiOJMdYpZrjBwJcscPhKnYnH/w9TdMLszxfkdJQborjANYdUV5Ei4YhyZA2hUH+OSztoK5rsJzGVNDG4/GHARwDZjSerL1vNNr8WKfdpzSrDs6C0EwyHl/84BZmsKIqAEASgzNNkwFfAgBH4zg+Nh6P7tGEht9sXFccS/l//ki43TRMobwBwEyDNM1uZDi6FHoYh19y7I6iAO5pZ/lk4LbAZdvB/eINWVh+D+CKByuo3Xap21tWA0VRWI5GI4CUjud5vwMX/6RpWjfYdnuEKB2nKQILVtJ1A/foKsj4YABFkb/TBI/zIr8FYWVLaVKWJYinfAqvnC1ysW88Gv6aYejHbcv6rJgvlJ8tQRMF6PUoIYSEy2Lyg3OIYFjbsL4Lix3n3xzHYRAHfd/7zywryXU7jwrBk9QeUUojhJoUC+T5LHjmHXGS/ZJp2HMfKsucSZL8rrWzqy9FSXnnzAv/dDoLlNfCIHh3lmUX1FanVxzaawWXmky3KIiG4N0JWIRBm48xl/k3liQs4IDv+0+UBZ2wbWudB2caqEHnVuJDU3wPP1MU5dulbszdXjHsS5OsvM80Wo5jW6RreuKF8b1n1s78O1TkqZK0f9AwjzQtksb2IYp8zDDnklVRkqSLm8PJ+4VmzzQjfTovJifb7jI59r4Dnjc9qWmV8hKDD8MQz2lPuK61gP9/VNMksYw1C2d9nU5nuDendqeF2NCZGnhuws/BW10qcE+Wx6Tpeolxedm4v4DB5DcXBoOrSwS9igm9Zq/0Zl69YtzdbrcpiePr/TA9bFngYloWmt5+bDLZ+DNz2Xqy2118wA/8OyACSrZ4IHD6Ot/PqdVya96pQBRbwj0PsJp96h9iSpFlOsr6WZWTCRnEYrUKz+Z5Bo/m1Om2PwmD4n8YB/enSaj4LI6//G2MU+FCTo67QCt79tL6ubWvIpCuYxeCS3gImYKChwYLyw9Y1uIjWN5beJE8IWjBeotnbVjMpZrP+jzgCvJY0nCv4+I3gE7SiPzZkDrtReXuCorEnuF7GWwcJ0g08pn+wD3Ic0tkviQOabJxWq1XVjA/WyQrskPJ1LtdM8anYe3T0NfMNCzDQZbiRJEk8oPr6+du6PfL9V5/RU3EXuEsxokhS3MEWab43aTzan5u9BKKC0C+Ashga+vLuaqUyrJsT9vR3s/BnsQlSRfSKpm/pjKCbHUGSj+zYnIDUu7PJ3GkVqtrdolgUZHPmcu2XdbkRT+YLpqWA3XognsZcr9OTqtFs/FY1Rts3aJUSUPxnA+Fly0OT+VprLgLzkIKt7MbWzOB23s99xDGXg38mFjVOAmV1Gg0zo0FQPTWqdPHnwNdLrNMpZuK/DUdBNkA3e70YaGIonBGvf4SGQgwlQkxhg/X82ItWNzC4mrABSQqVFrugONhwBmvBD8HyuRMEcYRRiGF/szrdlvXLy7tehq0OOQHs3flRXaR1M1enmZ5Egz/wjT1JyWKFjVgu+UGS0uDd6+tnf1WUck90F7lJg6INIlRC3iwQESd3oAcpNYAk7dFD3RCxQbQEkEYw8JhUlIFbmu6TTm0PC8SZKdULaaAFVvt7lZSYdKEYQCwno+A/3Knu3Cl54dfgFEu5SJLCBMGqygIY1hWpJZpPymOHn1ZpV8uG9kiRZG6aZb9Y5aW13Ay0OblHUe278/A1QQW6pMOTvF324I1EWgptDpODAo3XyQRnyAJ61fWPsqDdTIXr1KBbWghrN/CYmrFiANfBZTrtqNOtwcKZ12mEDKBwsJHijmQBf971/Luaw1pDsXJkycVYBdRXJO+UhZPs/Q3YbmPIlstq7q34ioNQRMFFHhTTOwgcGyVZjmSpbWHRme+QrMjf0At7Shp0NqstZ9jiJwDN1Li3kyt/nkENVO1SoRxmJXtThcaX1OIA5OpyDTMspQB+4ZZPbi8vHKX6wxovDkkiSqMo/y3wMWfhpt24ayxSkCeXiiKbIREsgTQgq2tI9XCGpgRcgbQGp61uvsp2/xfOvGtT5ARPUYW6MNO0TJI0PQ4rA/Njh8k48LnKG4/REUMT6arZCBO2FMmxwvEtil7MCcMUxtOGvIpw9T/MgxSQxNBliQeiWPHjglkrLLT6ShLcxA1H04ChVpxTjsrMEbEUZ2mFQ2PfYNGL36a+v7T5OInFvsyF0gMAAzg7P08QUGxRBQvX01T917q7X0rrIo0P1cIpduvKg9U0FYlDJ0bWECia+LZMk9/X+7evbsCyKun0+kjAHZxk1rrukDMq6ZKaS2yoDqrwUATndPsy5+n4NjTdPGFRFEqaBIJMlmfATTjtSGjajb4OqzITP+DWm/6O8qSKxCIkC0YiMvYujUjlXSabMkBjz9jXo9Ynje7Jk+jTPiQnZbrIIAKOZ6MP4EH7oD2desiSNsOOuYwrJ+lGfq9CCnVpdF3/olWn/kQ7e9kAI+JGCR4yGCLAveDFi0XkogireIUizH0/Ss0W/lzau26CtcrBbhpn8Scx0yJpnmoZRNqZRj/bEjtI1JUCaRF8Sa3LHEvAN6LlV6LZ38OEXst7HPJ3En1wCaA8XexCB6iHAQ1oGbkwJItW9Gb8C9tBIbibxnlXNGQC8chixMlY1AvgqfQ0eQ+AlbjSk3VGGJHZkRSKTHW19H3fc22zCd6vcWnEkwkuViPUnTJyRDcq7gQbyMBnsT5CKTthzRpXsI1gqbpymUF8zoH5GoEtRjjmkGoOLbqbKaAqmkTyB7ub5sIFBT33AVBiMirelSEPrntRGk8L19JGMZkxCxrtU5rFdZ+3LLNw/1e/0SlFMrnGhscSjNkE+NDeVb9AoD+WNMxMEiVrSDumrKCUC6rJQ76ufddJCFLebQJl9XNAQPWwWXTgoKAJi32jaYSHFk8hI4U7+yifn9AtoPWX9XRYqtLYcAqXpJYh4LdEoXFLWPynzSkuL2q8hflbDa9eTKNHkRML7FrhCG2Kq46cdT8KlUxw0ALpRxVKak3OI+GnV0029wEF8FH0IW5KiE0ywaKIdxvSygGtLdAho9Z6nQYwj2ALw4kGrWHZqjU38QKG5DVijsbllx0K1yjXDsMJs+6lt6XG8PxIwhC3GDVUdq09PNuoVaeqr7e8IutX6KQQZSJ7nU0zv+HuhUXMPMmFtbswdKVLoiLNy69U9AkYZ46h4C+RXE4osqoy1AG2ajSzn6QQavqD9/RmD/q2malLS8OPmhZ8mtJ5kPyfKgBNHe+wQGECizXCkWeKMsyRWrX4Tt+33XBe6i7jM5BsJ7yPgNHOe8LKbkmF2gTSEfIertyOenLv07dQR9jwnqoTdAAqD0LpkGzpdAcrPUsb1zidrqLb13cdeDtIo03kQ4NGo039gDeRaalV3mWvwcN4i8D9kKtxbpqUbiOrfcsUMVhMPRpSr5o9hKtff1OioZHaLFDtZxZtbX9ADKHa3L3j5B92R9RZ987yBJTVTQV9X6HapOaxMSdTN3M0lYSY3oyeNWSBd5ZWLKNh1FZlR5KQHS4UXk/gu8QUrHkqo0PpoticVXvhTXRzLRIM4NmJ5+j9PTDlA0fJZsbUS6IqAX9cGjl4uupddl9oG0L/EQXoQkFysDBGlsHWaLGbegAkDm4jG5dfzPmMjDvGfz+uIggMYYBvRERra6deBil6a1SbaKY8w08WYPlzDOnR51HNXgGCgJxZfBOdw/Fo1Xyzz1DFm1SOPPJ7L2ZuksIsNZFZKMktRCIXI5zILM1a97WccPNAJSh7l4ytnqR2Y79UK/XudM0nTa8vArAhUhRUCDkjXMbo8fDMPkZG0TnQdhCXAbWe16ktqfqDlZXAcnuDMA/1Sah42D/x3FKujVQ1E/9U1CcejOmQFecpz51u72tfq+pGXaWAex25jNbmgfkBUD67t6397zfYwOxYogoWEUr7t80HIVfdt2OylTc49WNL6dV51/w7AIGu7Lmlqb01oNlKwRhq9tVrb0P8LzZ57baip9s/QxBheeJ9x04uFiuuMja6vmqakvOdm4NeP4Mho4sLuJT9G2WqX0AY3yJjSS5CXRb7jd6ufZFZLHlokq5rDmHAHu53e79tWnYNrrihzngmuIkCGqwbYDlfi5D4HAQsgxZtqWyBFdpZaGrFt0w2good9e8uKbAovmm4yu2orBY1MeBIfW7sJ7ONE/3oSV7viostUCpS51s3TqmCXlrCfZF7JIyozY4xxweTzbHVan12brsJnYZu64FoAyQS8RgXsExnQxuLjVV/Sj3l0UdpE0i4G2BOkbklpX5+ra1WRnEwHE6Bzud9k2AR+NRqlow5YWUa4iSW2pUSJWpmkxu3QFQjsabx1Er7ONorlQ0x6oracHtvOmiAMFCk+lUBU6315vvIVMd+QieCoB5PF4gg0IZq85s8YbDTTrm680WF3fg/X7/o1joH6dpRM0OqVbv+tSdL1uxhy6gg0bR8yeHs6zYBxeoNohbft4r4HafAewMFG4u2br6nI81F1O1cafNi/1ibnEGygDZ0vWrgqLJdoBQeWxtPpR3x+NPB0HY5kxccp+HQxomXA/QlqOp5o8/aRq/LQzTX+TtJHwHP3POt981TfeAaRoWg+CJ+MyWYRmRW68EaGshzEc2Qr17tJ0cut3uKQBGkx1tNQw8Hqz5J1jUf0ENbsX9l+O5Nd/3eLt3q+wU62snt3cUq1JZaTSZHgbG25h3sP6q41q/iubnQJ6VfwjA/Gl22Dn7nMN5F7t8250hTcZDWJ0Tha0WDIYpz8y7mBHGeCdAfxwL/jCrx3wxzy8sLFzOwPAbt24VN8fVjj1iORuvf8+LF0zwGU2667ZlvySN6lG05jEawVNClMZ2269cfAcmugrXPrC9IcNdQ/EwSsELUdkdUi6fl49sZQ42nAcAecVgMLgNcncGwO6pt2XjH8YQ1wHkExW/b2Dtf5WKSO5eXwGYD107IqV9pN1uqd2dNMnfm6be/sZCLOCY7H58/xQmfH5nxDNg9ogf6L+BhR/SBbIbSsx8ztWmIgPIm+CVv8Hxu6DFF6E+t+Pam0CDE0yzsnztlzTyNV/KzPcHSgg3JI2V48fVnhmnYhQgcBsyD929tramwbUXNANzsCD5/CtsMCur9EXWYU7nTAldimeFsM6DdXfPLbfSVGdY8FGozh3sAaYAU4SNUmc8eiOvDGAJJIogjBCpI5pMJuucqTD4GUjNz4LbdzM4TMQBmDWu42soBW+v1Paq/HsWGN7QU5sjWfYF12n9VL11oALW5YBjcA3FGol7NQ1e9y1SXdIZqjnk9gWDPoBFXLOysnIpZOlvefU82Z49eyJc50PpJhbySRQSR9iyi8v7p622e0/KL2CYJk5rE/9/G8/fxZIGt7+gunAoyPcD+P94sbgdmaBDBa79Gywa1EV1vU/BwGHVwxsbGx5c+XlQ5bdVkKn9U40GvYX7pKy+iTQ+7rTsrxZ5BJc79+PeK7DIX6k3Y1IFemcx9P0+/yfAALYMc2fGWQOJAAAAAElFTkSuQmCC',id:'4a'},
            //             {image:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAYAAAAehFoBAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpFRTgxQjk4NUI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpFRTgxQjk4NkI2OEUxMUU4OTEwOEIxRjhCMEUyMERFMCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkVFODFCOTgzQjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkVFODFCOTg0QjY4RTExRTg5MTA4QjFGOEIwRTIwREUwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+S8qrQgAAEWNJREFUeNqcWQuMbeVVXv/e/36e55yZuXOfQCkvK9Ai2vbWS2xIUKok1NZAlJKmJqVK1NpgSaioSCNiDI21aHutViFtQgOKpsZoMRCrUkKx1XrBIr3c98ydO2fOa7/ffuvfZ88MlBTsyezsM/vs/f/fv9a3vrXWv0USD6n5CCFIUEWbo5CiuCJdF+p6VVUURREtLAxw9IjKGa6WuF+nsixp5k3fYlnLL9hOlzxv823Hj598qNfrfef888+/uSgK9bxhGBhPx3MFjTZP4X+TXMdVz+d5TriNTNMgoTsYb0bDs2cwRUVC02jn55X/vc4H68HkgpKUKMsxdSnI85MbNza85yfT8WHDKPmujy0tLV0ppbwpCIKPW5ZFtm3PwdZTBkH03rIs9rJBGI8QbxyD9sbBYnA1cgWgBg4T4G2Kk+InpOlSmsa3rZ098ZE4SS6yLFtZdDab3Q/PtJMkAciAUpzPbax+aualj3te/FcxvKiOJMdYpZrjBwJcscPhKnYnH/w9TdMLszxfkdJQborjANYdUV5Ei4YhyZA2hUH+OSztoK5rsJzGVNDG4/GHARwDZjSerL1vNNr8WKfdpzSrDs6C0EwyHl/84BZmsKIqAEASgzNNkwFfAgBH4zg+Nh6P7tGEht9sXFccS/l//ki43TRMobwBwEyDNM1uZDi6FHoYh19y7I6iAO5pZ/lk4LbAZdvB/eINWVh+D+CKByuo3Xap21tWA0VRWI5GI4CUjud5vwMX/6RpWjfYdnuEKB2nKQILVtJ1A/foKsj4YABFkb/TBI/zIr8FYWVLaVKWJYinfAqvnC1ysW88Gv6aYejHbcv6rJgvlJ8tQRMF6PUoIYSEy2Lyg3OIYFjbsL4Lix3n3xzHYRAHfd/7zywryXU7jwrBk9QeUUojhJoUC+T5LHjmHXGS/ZJp2HMfKsucSZL8rrWzqy9FSXnnzAv/dDoLlNfCIHh3lmUX1FanVxzaawWXmky3KIiG4N0JWIRBm48xl/k3liQs4IDv+0+UBZ2wbWudB2caqEHnVuJDU3wPP1MU5dulbszdXjHsS5OsvM80Wo5jW6RreuKF8b1n1s78O1TkqZK0f9AwjzQtksb2IYp8zDDnklVRkqSLm8PJ+4VmzzQjfTovJifb7jI59r4Dnjc9qWmV8hKDD8MQz2lPuK61gP9/VNMksYw1C2d9nU5nuDendqeF2NCZGnhuws/BW10qcE+Wx6Tpeolxedm4v4DB5DcXBoOrSwS9igm9Zq/0Zl69YtzdbrcpiePr/TA9bFngYloWmt5+bDLZ+DNz2Xqy2118wA/8OyACSrZ4IHD6Ot/PqdVya96pQBRbwj0PsJp96h9iSpFlOsr6WZWTCRnEYrUKz+Z5Bo/m1Om2PwmD4n8YB/enSaj4LI6//G2MU+FCTo67QCt79tL6ubWvIpCuYxeCS3gImYKChwYLyw9Y1uIjWN5beJE8IWjBeotnbVjMpZrP+jzgCvJY0nCv4+I3gE7SiPzZkDrtReXuCorEnuF7GWwcJ0g08pn+wD3Ic0tkviQOabJxWq1XVjA/WyQrskPJ1LtdM8anYe3T0NfMNCzDQZbiRJEk8oPr6+du6PfL9V5/RU3EXuEsxokhS3MEWab43aTzan5u9BKKC0C+Ashga+vLuaqUyrJsT9vR3s/BnsQlSRfSKpm/pjKCbHUGSj+zYnIDUu7PJ3GkVqtrdolgUZHPmcu2XdbkRT+YLpqWA3XognsZcr9OTqtFs/FY1Rts3aJUSUPxnA+Fly0OT+VprLgLzkIKt7MbWzOB23s99xDGXg38mFjVOAmV1Gg0zo0FQPTWqdPHnwNdLrNMpZuK/DUdBNkA3e70YaGIonBGvf4SGQgwlQkxhg/X82ItWNzC4mrABSQqVFrugONhwBmvBD8HyuRMEcYRRiGF/szrdlvXLy7tehq0OOQHs3flRXaR1M1enmZ5Egz/wjT1JyWKFjVgu+UGS0uDd6+tnf1WUck90F7lJg6INIlRC3iwQESd3oAcpNYAk7dFD3RCxQbQEkEYw8JhUlIFbmu6TTm0PC8SZKdULaaAFVvt7lZSYdKEYQCwno+A/3Knu3Cl54dfgFEu5SJLCBMGqygIY1hWpJZpPymOHn1ZpV8uG9kiRZG6aZb9Y5aW13Ay0OblHUe278/A1QQW6pMOTvF324I1EWgptDpODAo3XyQRnyAJ61fWPsqDdTIXr1KBbWghrN/CYmrFiANfBZTrtqNOtwcKZ12mEDKBwsJHijmQBf971/Luaw1pDsXJkycVYBdRXJO+UhZPs/Q3YbmPIlstq7q34ioNQRMFFHhTTOwgcGyVZjmSpbWHRme+QrMjf0At7Shp0NqstZ9jiJwDN1Li3kyt/nkENVO1SoRxmJXtThcaX1OIA5OpyDTMspQB+4ZZPbi8vHKX6wxovDkkiSqMo/y3wMWfhpt24ayxSkCeXiiKbIREsgTQgq2tI9XCGpgRcgbQGp61uvsp2/xfOvGtT5ARPUYW6MNO0TJI0PQ4rA/Njh8k48LnKG4/REUMT6arZCBO2FMmxwvEtil7MCcMUxtOGvIpw9T/MgxSQxNBliQeiWPHjglkrLLT6ShLcxA1H04ChVpxTjsrMEbEUZ2mFQ2PfYNGL36a+v7T5OInFvsyF0gMAAzg7P08QUGxRBQvX01T917q7X0rrIo0P1cIpduvKg9U0FYlDJ0bWECia+LZMk9/X+7evbsCyKun0+kjAHZxk1rrukDMq6ZKaS2yoDqrwUATndPsy5+n4NjTdPGFRFEqaBIJMlmfATTjtSGjajb4OqzITP+DWm/6O8qSKxCIkC0YiMvYujUjlXSabMkBjz9jXo9Ynje7Jk+jTPiQnZbrIIAKOZ6MP4EH7oD2desiSNsOOuYwrJ+lGfq9CCnVpdF3/olWn/kQ7e9kAI+JGCR4yGCLAveDFi0XkogireIUizH0/Ss0W/lzau26CtcrBbhpn8Scx0yJpnmoZRNqZRj/bEjtI1JUCaRF8Sa3LHEvAN6LlV6LZ38OEXst7HPJ3En1wCaA8XexCB6iHAQ1oGbkwJItW9Gb8C9tBIbibxnlXNGQC8chixMlY1AvgqfQ0eQ+AlbjSk3VGGJHZkRSKTHW19H3fc22zCd6vcWnEkwkuViPUnTJyRDcq7gQbyMBnsT5CKTthzRpXsI1gqbpymUF8zoH5GoEtRjjmkGoOLbqbKaAqmkTyB7ub5sIFBT33AVBiMirelSEPrntRGk8L19JGMZkxCxrtU5rFdZ+3LLNw/1e/0SlFMrnGhscSjNkE+NDeVb9AoD+WNMxMEiVrSDumrKCUC6rJQ76ufddJCFLebQJl9XNAQPWwWXTgoKAJi32jaYSHFk8hI4U7+yifn9AtoPWX9XRYqtLYcAqXpJYh4LdEoXFLWPynzSkuL2q8hflbDa9eTKNHkRML7FrhCG2Kq46cdT8KlUxw0ALpRxVKak3OI+GnV0029wEF8FH0IW5KiE0ywaKIdxvSygGtLdAho9Z6nQYwj2ALw4kGrWHZqjU38QKG5DVijsbllx0K1yjXDsMJs+6lt6XG8PxIwhC3GDVUdq09PNuoVaeqr7e8IutX6KQQZSJ7nU0zv+HuhUXMPMmFtbswdKVLoiLNy69U9AkYZ46h4C+RXE4osqoy1AG2ajSzn6QQavqD9/RmD/q2malLS8OPmhZ8mtJ5kPyfKgBNHe+wQGECizXCkWeKMsyRWrX4Tt+33XBe6i7jM5BsJ7yPgNHOe8LKbkmF2gTSEfIertyOenLv07dQR9jwnqoTdAAqD0LpkGzpdAcrPUsb1zidrqLb13cdeDtIo03kQ4NGo039gDeRaalV3mWvwcN4i8D9kKtxbpqUbiOrfcsUMVhMPRpSr5o9hKtff1OioZHaLFDtZxZtbX9ADKHa3L3j5B92R9RZ987yBJTVTQV9X6HapOaxMSdTN3M0lYSY3oyeNWSBd5ZWLKNh1FZlR5KQHS4UXk/gu8QUrHkqo0PpoticVXvhTXRzLRIM4NmJ5+j9PTDlA0fJZsbUS6IqAX9cGjl4uupddl9oG0L/EQXoQkFysDBGlsHWaLGbegAkDm4jG5dfzPmMjDvGfz+uIggMYYBvRERra6deBil6a1SbaKY8w08WYPlzDOnR51HNXgGCgJxZfBOdw/Fo1Xyzz1DFm1SOPPJ7L2ZuksIsNZFZKMktRCIXI5zILM1a97WccPNAJSh7l4ytnqR2Y79UK/XudM0nTa8vArAhUhRUCDkjXMbo8fDMPkZG0TnQdhCXAbWe16ktqfqDlZXAcnuDMA/1Sah42D/x3FKujVQ1E/9U1CcejOmQFecpz51u72tfq+pGXaWAex25jNbmgfkBUD67t6397zfYwOxYogoWEUr7t80HIVfdt2OylTc49WNL6dV51/w7AIGu7Lmlqb01oNlKwRhq9tVrb0P8LzZ57baip9s/QxBheeJ9x04uFiuuMja6vmqakvOdm4NeP4Mho4sLuJT9G2WqX0AY3yJjSS5CXRb7jd6ufZFZLHlokq5rDmHAHu53e79tWnYNrrihzngmuIkCGqwbYDlfi5D4HAQsgxZtqWyBFdpZaGrFt0w2good9e8uKbAovmm4yu2orBY1MeBIfW7sJ7ONE/3oSV7viostUCpS51s3TqmCXlrCfZF7JIyozY4xxweTzbHVan12brsJnYZu64FoAyQS8RgXsExnQxuLjVV/Sj3l0UdpE0i4G2BOkbklpX5+ra1WRnEwHE6Bzud9k2AR+NRqlow5YWUa4iSW2pUSJWpmkxu3QFQjsabx1Er7ONorlQ0x6oracHtvOmiAMFCk+lUBU6315vvIVMd+QieCoB5PF4gg0IZq85s8YbDTTrm680WF3fg/X7/o1joH6dpRM0OqVbv+tSdL1uxhy6gg0bR8yeHs6zYBxeoNohbft4r4HafAewMFG4u2br6nI81F1O1cafNi/1ibnEGygDZ0vWrgqLJdoBQeWxtPpR3x+NPB0HY5kxccp+HQxomXA/QlqOp5o8/aRq/LQzTX+TtJHwHP3POt981TfeAaRoWg+CJ+MyWYRmRW68EaGshzEc2Qr17tJ0cut3uKQBGkx1tNQw8Hqz5J1jUf0ENbsX9l+O5Nd/3eLt3q+wU62snt3cUq1JZaTSZHgbG25h3sP6q41q/iubnQJ6VfwjA/Gl22Dn7nMN5F7t8250hTcZDWJ0Tha0WDIYpz8y7mBHGeCdAfxwL/jCrx3wxzy8sLFzOwPAbt24VN8fVjj1iORuvf8+LF0zwGU2667ZlvySN6lG05jEawVNClMZ2269cfAcmugrXPrC9IcNdQ/EwSsELUdkdUi6fl49sZQ42nAcAecVgMLgNcncGwO6pt2XjH8YQ1wHkExW/b2Dtf5WKSO5eXwGYD107IqV9pN1uqd2dNMnfm6be/sZCLOCY7H58/xQmfH5nxDNg9ogf6L+BhR/SBbIbSsx8ztWmIgPIm+CVv8Hxu6DFF6E+t+Pam0CDE0yzsnztlzTyNV/KzPcHSgg3JI2V48fVnhmnYhQgcBsyD929tramwbUXNANzsCD5/CtsMCur9EXWYU7nTAldimeFsM6DdXfPLbfSVGdY8FGozh3sAaYAU4SNUmc8eiOvDGAJJIogjBCpI5pMJuucqTD4GUjNz4LbdzM4TMQBmDWu42soBW+v1Paq/HsWGN7QU5sjWfYF12n9VL11oALW5YBjcA3FGol7NQ1e9y1SXdIZqjnk9gWDPoBFXLOysnIpZOlvefU82Z49eyJc50PpJhbySRQSR9iyi8v7p622e0/KL2CYJk5rE/9/G8/fxZIGt7+gunAoyPcD+P94sbgdmaBDBa79Gywa1EV1vU/BwGHVwxsbGx5c+XlQ5bdVkKn9U40GvYX7pKy+iTQ+7rTsrxZ5BJc79+PeK7DIX6k3Y1IFemcx9P0+/yfAALYMc2fGWQOJAAAAAElFTkSuQmCC',id:'5a'}]
            $scope.$watch('visible',function(newvalue){
                console.log('hi')
                if(newvalue && $scope.library)
                {
                    console.log('heyyy11',$scope.library)
                    $scope.startCropper($scope.library['0']);
                }
            },true)

        }
    }
}])