app.directive('imageEditor', function($interval,$timeout){
    return {
      restrict: 'EA',
      templateUrl: 'js/directives/imageEditor.html',
      scope: {
        list: '=',
        title: '@',
        src:'=src',
				brightness:'=brightness',
				cancelmethod: '&',
				savemethod:'&',
				imgname:'='
      },
      link: function ($scope, element, attrs) {  
				console.log($scope.src);
				
      var bt=3    
      var v = angular.element( document.querySelector('#canvas-id') );
      $scope.canvas = new fabric.Canvas('canvas-id',{
      	draggable:false
      });
      fabric.Image.fromURL($scope.src, function(img) {
	 	img.set({
	 		hasControls:false,
	 		hasBorders:false,
	 		lockMovementX:true,
	 		lockMovementY:true,
	 		
	 	})
	 	img.scaleToHeight(250);
	    $scope.canvas.add(img).renderAll();
	    $scope.canvas.centerObject(img);
	    $scope.canvas.setActiveObject(img);
	}, { crossOrigin: 'Anonymous' });
      //console.log(v);	
      	/*Caman(v['0'],$scope.src,function(){
      		$scope.caman = this;
      		$timeout(function() {
      			$scope.caman.brightness(20).render()
      		}, 2000);
      		
      		/*$interval(function(){
      			console.log(bt)
      			$scope.caman.brightness(20).render();
      		},2000)*/
  			/*$scope.$watch('brightness', function() {
      		if($scope.caman)
      		{
      			console.log($scope.brightness)
       			$scope.caman.brightness(60).render();
      		}
      		
     	});*/
      	//});
      	//var v = angular.element( document.querySelector('#canvas-id') );
      
      	
      },
      controller:function($scope){
				$scope.selectedTab = 'brightness';
				$scope.IsShowFirstTab=true;
        $scope.IsShowSecondTab=false;
      	$scope.applyFilter = function(index, filter) {
		  var obj = $scope.canvas.getActiveObject();
		  obj.filters[index] = filter;
		  obj.applyFilters($scope.canvas.renderAll.bind($scope.canvas));
		}

		var f = fabric.Image.filters;
		$scope.changeBrightness = function(){
			$scope.SelectObject();
			$scope.activeOb = $scope.canvas.getActiveObject();
			var filter = new fabric.Image.filters.Brightness({
			  brightness: $scope.brightness/100
			});
			$scope.activeOb.filters['0'] = filter;
			$scope.activeOb.applyFilters();
			$scope.canvas.renderAll()
		}
		$scope.showFirstTab=function()
		{
		 $scope.IsShowFirstTab=!$scope.IsShowFirstTab;
		 $scope.IsShowSecondTab=false;
		}
		$scope.showSecondTab=function()
		{
			$scope.IsShowSecondTab=!$scope.IsShowSecondTab;
			$scope.IsShowFirstTab=false;
		}

		$scope.changeContrast = function(){
			$scope.SelectObject();
			$scope.activeOb = $scope.canvas.getActiveObject();
			var filter = new fabric.Image.filters.Contrast({
			  contrast: $scope.contrast/100
			});
			$scope.activeOb.filters['1'] = filter;
			$scope.activeOb.applyFilters();
			$scope.canvas.renderAll()
		}

		$scope.changeSmooth = function(){
			$scope.SelectObject();
			$scope.activeOb = $scope.canvas.getActiveObject();
			var filter = new fabric.Image.filters.Saturation({
			  saturation: $scope.smooth/100
			});
			$scope.activeOb.filters['2'] = filter;
			$scope.activeOb.applyFilters();
			$scope.canvas.renderAll()
		}

		$scope.changeVibrance = function(){
			$scope.SelectObject();
			$scope.activeOb = $scope.canvas.getActiveObject();
			var filter = new fabric.Image.filters.HueRotation({
			  rotation: $scope.vibrance/100
			});
			$scope.activeOb.filters['3'] = filter;
			$scope.activeOb.applyFilters();
			$scope.canvas.renderAll()
		}

		$scope.changeFade = function(){
			$scope.SelectObject();
			$scope.activeOb = $scope.canvas.getActiveObject();
			var filter = new fabric.Image.filters.Blur({
			  blur: $scope.fade/100
			});
			$scope.activeOb.filters['3'] = filter;
			$scope.activeOb.applyFilters();
			$scope.canvas.renderAll()
		}

		$scope.SelectObject = function (ObjectName) {
		    $scope.canvas.getObjects().forEach(function(o) {
		        if(o.id === ObjectName) {
		            $scope.canvas.setActiveObject(o);
		        }
		    })
		}

		$scope.savechanges = function(){
			$scope.savemethod({img:$scope.canvas.toDataURL(),name:$scope.imgname})
		}
		
      
      }
     
    };
  });