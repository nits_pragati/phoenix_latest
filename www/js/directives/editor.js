app.directive('photoEditor', ['$timeout','$ionicSlideBoxDelegate', function($timeout,$ionicSlideBoxDelegate) {
    return {
        restrict: "E",
        scope : {
            image : '=',
            cancelmethod: '&',
            savemethod:'&',
            imgname:'=',
            onfilechange:'&'
        },
        templateUrl : 'js/directives/editor.html',
        link: function(scope, elem, attrs) {
            var imgCanvas, $save;
            scope.randdiv = 'dv' + Math.floor(Math.random()*100000);

            scope.currentTab = 'adjust';
            scope.selectedTab = 'brightness';
            console.log('heyyyy',scope.image)
            //init photo when click on real photo
            // scope.$on("fillCanvas", function(event, data) {
            //     console.log("fill canvas called");
            //     console.log(data.photo);
            //     scope.fillCanvas(data);
            // });
            // console.log('scope ',scope.image)
            scope.setImgFilter = function(filter){
               
                // console.log(filter)
                Caman('#' + filter, function () {
                    // console.log(this)
                    this[filter]();
                    this.render();
                  });
            }
            scope.fillCanvas = function(data) {
                var hzMIW = 0,
                    hzMIH = 0,
                    hzImgH = 0,
                    hzImgW = 0,
                    path = data,
                    c, c1, ctx, ctx1;
                console.log($("."+scope.randdiv))
                console.log($(".custom-canvas-area"))

                $("."+scope.randdiv).find(".hz-modal-image").children("canvas").remove();
                var canvas_ = $("<canvas data-caman-hidpi-disabled=\"true\"></canvas>").attr({id: 'photoEditorCanvas'+scope.randdiv});
                $("."+scope.randdiv).find(".hz-modal-image").append(canvas_);
                var imgSz = window.innerWidth -50;
                c = document.getElementById('photoEditorCanvas'+scope.randdiv);
                c.width = imgSz;
                c.height = imgSz;
    
                ctx = c.getContext('2d');
                
                imgCanvas = new Image();
                imgCanvas.onload = function() {
                    $("."+scope.randdiv).find('.filter-div').each(function(){
                        var id = $(this).attr('id') ;
                        // console.log('heyyyy')
                        var canvas1 = $("<canvas height=\"50\" width=\"50\" data-caman-hidpi-disabled=\"true\"></canvas>").attr({id: 'can-' + id + scope.randdiv});

                        $("."+scope.randdiv).find("#" + id).append(canvas1);
                        canvas1 = document.getElementById('can-' + id + scope.randdiv);
                        var cntxt = canvas1.getContext('2d');
                        cntxt.drawImage(imgCanvas, 0, 0, 50,50);
                        Caman('#can-' + id + scope.randdiv, imgCanvas, function() {
                            if(id != 'normal')
                            {
                                this[id]();
                            }                            
                            this.render();
                        })
                    })
                };
                imgCanvas.crossOrigin = "anonymous"
                imgCanvas.src = path;


                // console.log("imgCanvas.src");
                // console.log(imgCanvas.src);
                if (imgCanvas !== imgCanvas) {
                    console.log("canvas is load.....");
                }
    
                angular.element('input[type=range]').val(0);

                
               
                $('.fill-images').on('load',function(){   
                               
                    //scope.setImgFilter($(this).attr('id'));
                })
                imgCanvas.addEventListener('load', function() {
                    /*ctx.clearRect(0, 0, 0, 0);*/
                    console.log('hiii');
                    ctx.drawImage(imgCanvas, 0, 0, imgSz, imgSz);
                    scope.hideSpinner = true;
                    Caman('#photoEditorCanvas' + scope.randdiv, imgCanvas, function() {
                        this.revert(false);                                              
                        this.render(function(){
                            scope.base64image = this.toBase64();
                        });
                        
                        //this.contrast(contrast).render();
        
                    });
                }, false);
    
            };
            $timeout(function(){
                scope.fillCanvas(scope.image);
            })
            
            
            //give effect by slider like hue, contrast, sepia
            scope.applyImageParameters = function(filter) {
                var hue, cntrst, vibr, sep, brightness,sharpen;
                hue = parseInt($("."+scope.randdiv).find('#saturation').val());
                cntrst = parseInt($("."+scope.randdiv).find('#contrast').val());
                vibr = parseInt($("."+scope.randdiv).find('#vibrance').val());
                sep = parseInt($("."+scope.randdiv).find('#sepia').val());
                brightness = parseInt($("."+scope.randdiv).find('#brightness').val());
                sharpen = parseInt($("."+scope.randdiv).find('#sharpen').val());
                console.log('fil ',sharpen)
                if(filter)
                {
                    scope.filter = filter;
                }
                Caman('#photoEditorCanvas' + scope.randdiv, imgCanvas, function() {
                    this.revert(false);
                    // this.reloadCanvasData();
                    if(scope.filter && scope.filter!='normal')
                    {
                        this[scope.filter]();
                    }   
                    if(sharpen>0)
                    {
                        this.sharpen(sharpen);
                    }                
                    this.brightness(brightness).saturation(hue).contrast(cntrst).vibrance(vibr).exposure(sep).render(function(){
                        scope.base64image = this.toBase64();
                        scope.onfilechange({img:this.toBase64(),name:scope.imgname});
                    });

                    //this.contrast(contrast).render();
    
                });
            };

            
    
            angular.element("input[type=range]").on("input", function(e) {
                scope.applyImageParameters();
                // console.log("event type: " + e.type);
                var min = e.target.min,
                    max = e.target.max,
                    val = e.target.value;
                angular.element(e.target).css({
                    'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
                });
                //scope.reloadCanvasData();
    
            });

            
            

            scope.savechanges = function(){
                // console.log(scope.base64image)
                scope.savemethod({img:scope.base64image,name:scope.imgname})
            }

            scope.mouseoverWideDiv = function() {
                $ionicSlideBoxDelegate.enableSlide(false);
            };
            
            scope.mouseleaveWideDiv = function() {
                $ionicSlideBoxDelegate.enableSlide(true);
            };
            
        }
    };
    }])