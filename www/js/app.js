// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('phoenix', ['ionic', 'ksSwiper', 'ngCordova', 'oc.lazyLoad', 'ionic-toast','ngMap'])
.run(function($ionicPlatform,$rootScope,$state) {
//.run(function($ionicPlatform,$rootScope, $cordovaPushV5) {
  
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && Keyboard) {
      
      Keyboard.hideFormAccessoryBar(false);
        
    }

    var push = PushNotification.init({
      android: {
      },
       
      ios: {
        alert: "true",
        badge: "true",
        sound: "true"
      },
      windows: {}
    });
    
    push.on('registration', function(data) {
      console.log(" registartion ============ ", data.registrationId);
      $rootScope.uuid = data.registrationId;
      $rootScope.deviceType = deviceInformation.platform.toLowerCase();

    });

    push.on('notification', (data) => {
      // data.message,
      // data.title,
      // data.count,
      // data.sound,
      // data.image,
      var pushAdditionalData =data.additionalData;
      if(pushAdditionalData){
        if(pushAdditionalData.type == 'chat'){
          $state.go('notifications-list');
        }else if(pushAdditionalData.type == 'notification'){
          $state.go('notifications');
        }
        else if(pushAdditionalData.type == 'post'){
          $state.go('listings-details',{post_id : pushAdditionalData.id})
        }
        else if(pushAdditionalData.type == 'users')
        {
          $state.go('member-profile-member-view',{userId : pushAdditionalData.id})
        }
      }
      //console.log(data);
    });

    // initialize
    // $cordovaPushV5.initialize(options).then(function() {
    //   // start listening for new notifications
    //   $cordovaPushV5.onNotification();
    //   // start listening for errors
    //   $cordovaPushV5.onError();

    //   // register to get registrationId
    //   $cordovaPushV5.register().then(function(registrationId) {
    //     // save `registrationId` somewhere;
    //     //console.log(registrationId);
    //      $rootScope.uuid = registrationId;
    //      var deviceInformation = ionic.Platform.device();
    //       console.log('deviceInformation',deviceInformation);
    //       console.log('uuid',registrationId);
    //       $rootScope.deviceType = deviceInformation.platform.toLowerCase();
    //   })
    // });

    // $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data){
    //   // data.message,
    //   // data.title,
    //   // data.count,
    //   // data.sound,
    //   // data.image,
    //   // data.additionalData
    //   //$state.go();
    //   console.log(data);

    //     // if(data.additionalData.encoded.type =='chat'){
    //     //     $state.go('user.chat',{id:data.additionalData.encoded.id})
    //     // }else if(data.additionalData.encoded.type == 'groupchat'){
    //     //     $state.go('user.groupchat',{id:data.additionalData.encoded.id})
    //     // }else{
    //     //     $state.go('user.alert');
    //     // }
    // });

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
      //console.log('hide ------============\\\\\\\////')
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $rootScope.serviceurl = "http://111.93.169.90/bikash/roots-folio/api/";
  // $rootScope.serviceurl = "http://192.168.1.118/bikash/roots-folio/api/";
})

// .config(function($stateProvider, $urlRouterProvider) {

//   // Ionic uses AngularUI Router which uses the concept of states
//   // Learn more here: https://github.com/angular-ui/ui-router
//   // Set up the various states which the app can be in.
//   // Each state's controller can be found in controllers.js
//   $stateProvider

//   // setup an abstract state for the tabs directive
//     .state('tab', {
//     url: '/tab',
//     abstract: true,
//     templateUrl: 'templates/tabs.html'
//   })

//   // Each tab has its own nav history stack:

//   /*.state('tab.dash', {
//     url: '/dash',
//     views: {
//       'tab-dash': {
//         templateUrl: 'templates/splash.html',
//         controller: 'DashCtrl'
//       }
//     }
//   })*/
//   .state('home', {
//         url: '/home',
//         templateUrl: 'templates/home.html'
//   })
//   .state('login', {
//     url: '/login',
//     templateUrl:'templates/member-login.html'
//   })
//   .state('account-type', {
//     url: '/account-type',
//     templateUrl:'templates/choose-account-type.html'
//   })
//   .state('create-account', {
//     url: '/create-account',
//     templateUrl: 'templates/member-createaccount.html'
// //    controller: 'SlideCtrl'
//   })

//   .state('member-signup', {
//     url: '/member-signup',
//     templateUrl: 'templates/member-signup.html'
//   })

//    .state('register', {
//     url: '/register',
//     templateUrl: 'templates/register.html'
//   })

//   .state('privacy', {
//     url: '/privacy',
//     templateUrl: 'templates/privacy.html'
//   })

//   .state('menu-activities', {
//     url: '/menu-activities',
//     templateUrl: 'templates/menu-activities.html',
//     controller: 'SlideCtrl'
//   })

//   .state('menu-map', {
//     url: '/menu_map',
//     templateUrl: 'templates/menu-map.html',
//     controller: 'SlideCtrl'
//   })
//   .state('welcome', {
//     url: '/welcome',
//     templateUrl: 'templates/welcome.html',
//     controller: 'SlideCtrl'
//   })
//   .state('instant-post', {
//     url: '/instant-post',
//     templateUrl: 'templates/instant-post.html',
//     controller: 'SlideCtrl'
//   })
//   .state('post', {
//     url: '/post',
//     templateUrl: 'templates/post.html',
//     controller: 'SlideCtrl'
//   })

//   .state('group', {
//     url: '/group',
//     templateUrl: 'templates/group.html',
//     controller: 'SlideCtrl'
//   })


//   .state('profile', {
//     url: '/profile',
//     templateUrl: 'templates/profile.html',
//   })
//   .state('profile-page', {
//     url: '/profile-page',
//     templateUrl: 'templates/profile-page.html',
//   })

//   .state('deals', {
//     url: '/deals',
//     templateUrl: 'templates/deals.html',
//   })

//   .state('profile_next', {
//     url: '/profile_next',
//     templateUrl: 'templates/profile-next.html',
//   })
//     .state('tab.chat-detail', {
//       url: '/chats/:chatId',
//       views: {
//         'tab-chats': {
//           templateUrl: 'templates/chat-detail.html',
//           controller: 'ChatDetailCtrl'
//         }
//       }
//     })

//   .state('tab.account', {
//     url: '/account',
//     views: {
//       'tab-account': {
//         templateUrl: 'templates/tab-account.html',
//         controller: 'AccountCtrl'
//       }
//     }
//   });

//   // if none of the above states are matched, use this as the fallback
//   //$urlRouterProvider.otherwise('/tab/dash');

// });
