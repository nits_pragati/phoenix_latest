// Ionic Starter App
var app = angular.module('phoenix');

app.constant('JS_REQUIRES', {
  //*** Scripts
  scripts: {
    'SlideCtrl': ['js/controllers/SlideCtrl.js'],
    'memberSignupCtrl': ['js/controllers/memberSignupCtrl.js', 'js/controllers/registerCtrl.js'],
    'chooseAccountTypeCtrl':['js/controllers/chooseAccountTypeCtrl.js','js/services/CommonService.js','js/services/PostService.js'],
    'exploreCtrl':['js/controllers/exploreCtrl.js','js/services/CommonService.js','js/services/PostService.js'],
    'editprofileCtrl':['js/controllers/editprofileCtrl.js','js/services/CommonService.js'],
    'chatCtrl':['js/controllers/chatCtrl.js'],
    'chatlistCtrl':['js/controllers/chatlistCtrl.js'],
    'privacyCtrl':['js/controllers/privacyCtrl.js'],
    'signupCtrl' : ['js/controllers/signupCtrl.js','js/services/userdetails_services.js','js/services/CommonService.js'],
    'postCtrl' : ['js/controllers/postCtrl.js','js/services/PostService.js','js/directives/editor.js','js/services/CommonService.js','js/directives/gallery.js','lib/croppie/croppie.min.js','lib/croppie/croppie.css'],
    'locationCtrl' : ['js/controllers/locationCtrl.js','js/services/PostService.js'],
    'filterpostCtrl' : ['js/controllers/filterpostCtrl.js','js/services/PostService.js','js/services/CommonService.js','js/directives/gallery.js'],
    'profilePreferenceCtrl' : ['js/controllers/profilePreferenceCtrl.js','js/services/userdetails_services.js','js/services/CommonService.js'],
    'notificationCtrl' : ['js/controllers/notificationCtrl.js','js/services/NotificationService.js'],
    'searchCtrl' : ['js/controllers/searchCtrl.js','js/services/PostService.js']
    //*** Services
  },
  modules: [
    {
        name: 'ngTagsInput',
        files: ['lib/ngtags/ng-tags-input.min.css','lib/ngtags/ng-tags-input.min.js']
    },
    {
      name : 'angularMoment',
      files: ['lib/moment/angular-moment.min.js']
    },
    {
      name : 'ngtimeago',
      files: ['lib/ngtimeago/ngtimeago.js']
    },
    {
      name : 'pr.longpress',
      files : ['lib/longpress/angular-long-press.js']
    },
    {
      name : 'ion-autocomplete',
      files : ['lib/ion-autocomplete/ion-autocomplete.js','lib/ion-autocomplete/ion-autocomplete.css']
    },
    {
      name : 'ngDraggable',
      files : ['lib/ngDraggable.js']
    }
  ]
});
