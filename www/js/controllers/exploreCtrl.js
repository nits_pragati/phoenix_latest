app.controller('exploreCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, $ionicLoading, $ionicScrollDelegate,$location, UserService,CommonService,PostService,$timeout) {
    if (!UserService.getUserInfo()) {
        $state.go('login');
    }
    $scope.userDetails = UserService.getUserInfo();
    
    $scope.getPostListingsexplore = function () {
        PostService.getPostListexplore($scope.userDetails.id,false).then(function (data) {
            //console.log('is featur off',data);
            if (data.ack == 1) {
                //$scope.userImageUrl = data.user_img;
                $scope.postImageUrlexplore = data.file_url;
                $scope.postListingsexplore = data.list;
            }
        })
    }

    $scope.getPostListingsfeature = function () {
        PostService.getPostListexplore($scope.userDetails.id,true).then(function (data) {
            //console.log(data);
            if (data.ack == 1) {
                //$scope.userImageUrl = data.user_img;
                $scope.postImageUrlfeature = data.file_url;
                $scope.postListingsexplorefeature = data.list;
            }
        })
    }
});
