 app.controller('postCtrl', function ($scope, $state, $ionicSlideBoxDelegate, $ionicPopup, ionicToast, $rootScope, $ionicLoading, $stateParams, UserService, $http, $timeout, PostService, CommonService, $anchorScroll, $location, $sce, $ionicScrollDelegate) {
    if (!UserService.getUserInfo()) {
        $state.go('login');
    }

    $scope.postSetForm = {
        address1:'',
        address2:'',
        phone:'',
        gender:'',
        dob:'',
        texture:''
    }
    $scope.saveFavPostId = null;
    $scope.isSaveFavPost = false;
    $scope.userDetails = UserService.getUserInfo();
    if ($scope.userDetails && $scope.userDetails.profile_image) {
        $scope.userDetails.profile_image = $scope.userDetails.image_url + $scope.userDetails.profile_image;

    }
    $scope.imageArray = [];
    //$scope.userDetails = UserService.getUserInfo();
    //console.log('userDetails',$scope.userDetails);

    $scope.IsShowText=false;

    $scope.toDataUrl = function (path, callback) {
        window.resolveLocalFileSystemURL(path, gotFile, fail);

        function fail(e) {
            alert('Cannot found requested file');
        }
        function gotFile(fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    var content = this.result;
                    callback(content);
                };
                // The most important point, use the readAsDatURL Method from the file plugin
                reader.readAsDataURL(file);
            });
        }
    }

    $scope.SelectTogglerootcurl = function(){
        $scope.IsActive = !$scope.IsActive;
    }

    /*$scope.SelectToggle = function (postsocial) {
        postsocial.showsocialpopup = !postsocial.showsocialpopup;
    }

    $scope.SelectToggle2 = function (post) {
        $scope.saveFavPostId = post.id;
        post.showpopup = !post.showpopup;
    }

    
    $scope.goToSavesPostPage = function (){
        if($scope.saveFavPostId != null){
            $ionicLoading.show({
                template: 'Please wait...'
            })
            PostService.saveFavPost({user_id:$scope.userDetails.id, post_id:$scope.saveFavPostId}).then(function (result) {
                $ionicLoading.hide()
                if (result.ack == 1) {
                    if(result.type == 1){
                        ionicToast.show('Post Save Successfully.');
                    }else{
                        ionicToast.show('Post Remove Successfully.');
                    }
                    $scope.SelectToggle2(null);
                    //$state.go('post-images', { post_id: result.post_id });
                }
            })
        }
    }*/

    $scope.imageArray = [];
    //$scope.userDetails = UserService.getUserInfo();
    $scope.getCurlCategories = function () {
        CommonService.getCurlCategories().then(function (data) {
            if (data) {
                $scope.curlCategories = data.categories;
            }
            //console.log(' categories ', data)
        })
    }
    $scope.getCurlCategories();
    $scope.goToMyPostPage = function (post) {
        $state.go('member-profile-member-view', { userId: post.user.id })
    }
    $scope.cameraChooser = function () {
        if ($scope.imageArray.length < 4) {
            $ionicLoading.show({
                template: 'Loading...'
            })
            navigator.camera.getPicture($scope.uploadSelectedFile, function (err) {
                console.log('error', err);
                $ionicLoading.hide();
            }, {quality:80, correctOrientation: true, saveToPhotoAlbum: true,destinationType: Camera.DestinationType.FILE_URI});
        }
        else {
            ionicToast.show('Maximum 4 images allowed.')
        }

    }

    $scope.recordVideo = function () {        
        navigator.device.capture.captureVideo(function (data) {
            if(data)
            {
                console.log('video data ', data);
                $ionicLoading.show({
                    template: 'Loading...'
                })
                $scope.imageArray = [];
                
                var videoFile = data['0']
                var options = new FileUploadOptions();
                options.fileKey="videoFile";
                options.fileName=videoFile.name;
                options.contentType = "multipart/form-data";
                options.chunkedMode = false;
                options.mimeType=videoFile.type;
                options.httpMethod="POST";
                options.headers =   {
                    Connection: "close"
                };    


                var ft = new FileTransfer();

                //$scope.videoFile = videoFile.fullPath
                ft.upload(videoFile.fullPath, $rootScope.serviceurl + 'posts/uploadvideo.json', function(success){
                    if(success.responseCode == 200)
                    {
                        console.log(success,success.response);
                        var response = JSON.parse(success.response);
                        console.log('success ',response);
                        if(response.ack == 1)
                        {
                            $scope.videoFile = response;
                            $scope.videoFileUrl = response.file_url + response.file_name;
                            $scope.imageIsLoaded();
                        }
                    }
                    $ionicLoading.hide()
                }, function(err){
                    console.log(' err ',err);
                    $ionicLoading.hide()
                }, options);
            }
            
            console.log(' success ', data)
        }, function (err) {
            console.log('err ', err)
        },{duration:60});
    }

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
      }

    $scope.fileChooser = function () {
        if ($scope.imageArray.length < 4) {
            navigator.camera.getPicture($scope.uploadSelectedFile, function (err) {
                console.log('error', err);
            }, { sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                 destinationType: Camera.DestinationType.FILE_URI, 
                 quality:100});
        }
        else {
            ionicToast.show('Maximum 4 images allowed.')
        }

    }

    $scope.uploadSelectedFile = function (result) {
        //$scope.imageArray = [];
        console.log(' urllll ',result);
        if(result)
        {        
            $scope.videoFile = null;
            $scope.imageArray.unshift(window.Ionic.WebView.convertFileSrc(result));
            // $scope.toDataUrl(result, function (base64Image) {
                // $scope.imageArray = $scope.imageArray.filter(function (item) {
                //     return item !== undefined;
                // });
                // $scope.imageArray.push(base64Image);
                // $scope.imageArray.unshift(base64Image);
                // $timeout(function () {
                //     $ionicSlideBoxDelegate.update();
                //     console.log(' length ', $scope.imageArray.length)
                //     $ionicSlideBoxDelegate.slide($scope.imageArray.length);
                //     $('.cropperbox-' + $ionicSlideBoxDelegate.currentIndex()).croppie({
                //         showZoomer: false,
                //         enableOrientation: true,                                
                //         viewport: {
                //             width: '100%',
                //             height: '100%'
                //         }
                //     });
                //     //$ionicLoading.hide();
                // })

            // });
        }
        else
        {
            console.log('No Image');
            $ionicLoading.hide();
        }
    }

    $scope.deleteImageFromSlider = function () {
        delete $scope.imageArray[$ionicSlideBoxDelegate.currentIndex()];
        $scope.imageArray = $scope.imageArray.filter(function (item) {
            return item !== undefined;
        });
        $ionicSlideBoxDelegate.update();
    }
    $scope.goToPreference = function () {

        //$state.go('provider-profile-preferences')
        $state.go('member-profile-member-view', {userId:$scope.userDetails.id})
    }
    $scope.editSlideImage = function () {
        console.log($scope.postDetails.file_url + $scope.postDetails.post_files[$ionicSlideBoxDelegate.currentIndex()].name);
        $scope.editedIndex = $ionicSlideBoxDelegate.currentIndex();
        if (typeof CSDKImageEditor != 'undefined') {
            var options = {
                outputType: CSDKImageEditor.OutputType.JPEG,
                tools: [
                    CSDKImageEditor.ToolType.EFFECTS,
                    CSDKImageEditor.ToolType.CROP,
                    CSDKImageEditor.ToolType.ADJUST,
                    CSDKImageEditor.ToolType.ENHANCE
                ],
                quality: 50
            };


            CSDKImageEditor.edit($scope.success, $scope.error, $scope.postDetails.file_url + $scope.postDetails.post_files[$ionicSlideBoxDelegate.currentIndex()].name, options);
        }
    }

    $scope.resetSlider = function () {
        $ionicSlideBoxDelegate.update();
        if ($scope.editedIndex) {
            $ionicSlideBoxDelegate.slide($scope.editedIndex);
        }
        else {
            $ionicSlideBoxDelegate.slide(0);
        }

    }

    $scope.upVideo = function (mediaFile) {

        console.log(mediaFile);
    }

    $scope.chooseFiles = function (files) {
        if (files.length > 4) {
            ionicToast.show('Please select maximum 4 Photos');
        }
        else {
            $scope.imageArray = [];
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded;
                reader.readAsDataURL(file);
                $scope.imageArray.push(reader);
            }
            $timeout(function () {
                $ionicSlideBoxDelegate.update();
                $ionicSlideBoxDelegate.slide(0);
            })

            console.log($scope.imageArray)
        }
    }
    $scope.imageIsLoaded = function () {
        $ionicSlideBoxDelegate.update();
        $ionicSlideBoxDelegate.slide(0);
    }

    $scope.savePostFiles = function () {
        //console.log($scope.imageArray);
        $ionicLoading.show({
            template: 'Uploading...'
        })
        if($scope.imageArray && $scope.imageArray.length>0)
        {
            var images = [];
           
            $scope.imageArray.forEach(function(value,ind){
                $('.cropperbox-' + ind).croppie('result',{size:'original'}).then(function(html,size) {
                    console.log('----',ind,images.length)
                    
                    images.push(html);
                    if($scope.imageArray.length == ind+1)
                    {
                        
                        PostService.savePostFiles(images, $scope.userDetails.id).then(function (result) {
                            $ionicLoading.hide()
                            if (result.ack == 1) {
                                $scope.post_id = result.post_id                   
                                $state.go('post-images', { post_id: result.post_id });
                            }
                            console.log(' resss ', result)
                        })
                    }
                    
                })
                 
            })
            console.log('doneeeee')


            
        }
        else if($scope.videoFile) {
            
            PostService.savePostVideos($scope.videoFile.file_name,$scope.userDetails.id).then(function(result){
                $ionicLoading.hide()
                console.log(' resss ',result);
                if(result.ack == 1) {
                    $state.go('post-settings', { post_id: result.post_id });
                }

            })
        }
        else
        {
            $ionicLoading.hide()
            ionicToast.show('Please upload at least one image or video.')
        }
        // $state.go('post-settings');
    }
    $scope.success = function (data) {
        //$state.go('post-settings',{post_id : $scope.post_id});
        $scope.toDataUrl(data, function (base64Image) {
            //window.open(base64Image);
            PostService.replaceOldFile(base64Image, $scope.postDetails.post_files[$scope.editedIndex].name).then(function () {
                $scope.getPostDetails()
            })
            // Then you'll be able to handle the myimage.png file as base64
        });
        console.log(data)
    }
    $scope.imgres = 'helloo'
    $scope.goToNext = function () {
        // PostService.replaceOldFile(base64Image,$scope.postDetails.post_files[$scope.editedIndex].name).then(function(){
        //     $scope.getPostDetails()
        // })
        $state.go('post-settings', { post_id: $scope.postDetails.id });
    }

    $scope.error = function (err) {
        console.log(err)
    }
    $scope.postDetails = {};
    $scope.postDetails.tags = [];
    $scope.postDetails.tagmembers = [];
    $scope.gettagmemberlist = [];
    $scope.getPostDetails = function () {
        //console.log($stateParams.post_id);
        PostService.getPostDetails($stateParams.post_id).then(function (data) {
            if (data.ack == 1) {
                $scope.postDetails = data.details;
                $scope.postSetForm.address1 = data.details.address1;
                $scope.postSetForm.address2 = data.details.address2;
                $scope.postSetForm.phone = data.details.phone;
                $scope.postSetForm.gender = data.details.gender;
                $scope.postSetForm.texture = data.details.texture;
                $scope.postSetForm.dob = moment(data.details.dob).format('L');
                //console.log(data.details);
                if(data.details.post_tags.length>0){
                  var tagArr = [];
                  angular.forEach(data.details.post_tags, function (value, key){
                    //console.log(value.hashtag.tag);
                    tagArr.push({"id":value.hashtag_id,"name":value.hashtag.tag});
                  });
                  $scope.postDetails.tags =tagArr;
                  //console.log(tagArr);
                }

                if(data.details.post_tagmembers.length>0){
                  var tagmemberArr = [];
                  angular.forEach(data.details.post_tagmembers, function (value1, key){
                    tagmemberArr.push({"id":value1.user_id,"name":value1.user.name});
                  });
                  $scope.postDetails.tagmembers =tagmemberArr;
                }
            }
            //console.log(data)
        })
        PostService.getMemberList({user_id:$scope.userDetails.id}).then(function (data) {
            if (data.ack == 1) {
                $scope.gettagmemberlist = data.user_list;
            }
            //console.log('member list',data)
        })
    }

    $scope.createStringByArray = function(array) {
        var output = '';
        angular.forEach(array, function (object) {
            angular.forEach(object, function (value, key) {
                output += key + ',';
                output += value + ',';
            });
        });
        return output;
    }

    $scope.savePostDetails = function () {
        var user_details = UserService.getUserInfo();
        $scope.postDetails.user_id = user_details.id;
        $scope.postDetails.is_active = 1;
        $scope.postDetails.address1 =$scope.postSetForm.address1;
        $scope.postDetails.address2 =$scope.postSetForm.address2;
        $scope.postDetails.phone =$scope.postSetForm.phone;
        $scope.postDetails.gender =$scope.postSetForm.gender;
        $scope.postDetails.texture =$scope.postSetForm.texture;
        $scope.postDetails.dob = $scope.postSetForm.dob;
        if($scope.postDetails.tagmembers && $scope.postDetails.tagmembers.length >0){

          $scope.postDetails.tagmembers = JSON.stringify($scope.postDetails.tagmembers);
        }
        // if($scope.postDetails.tags.length >0){
        //   $scope.postDetails.tags = JSON.stringify($scope.postDetails.tags);
        // }
        //console.log($scope.postDetails);

        PostService.savePostDetails($scope.postDetails).then(function (data) {
            if (data.ack == 1) {
                ionicToast.show('Post added successfully');
                $state.go('listings')
            } else {
                ionicToast.show('Internal error please try again later.');
            }
        })
    }

    $scope.editedImage = {};

    $scope.brightness1 = 2
    //     $scope.changeBrightness = function(){
    //         console.log($scope.brightness1);
    //         Caman("#canvas-id", "path/to/image.jpg", function () {
    //   // manipulate image here
    //   this.brightness($scope.brightness1++).render();
    // });

    //     }

    $scope.startEditing = function (i) {
        console.log('editing ', i, $scope.postDetails.post_files)
        //console.log(' edit ',$scope.postDetails.post_files[i])
        $scope.editorMode = true;
        $scope.editedImageName = $scope.postDetails.post_files[i].name
    }
    $scope.cancelEditing = function () {
        $scope.editorMode = false;
    }

    $scope.updateChangedImage = function () {
       console.log('upppppp',$scope.postDetails.post_files)
            PostService.replaceOldFiles($scope.postDetails.post_files).then(function () {
                    // $scope.getPostDetails();
                    // $scope.editorMode = false;
                    console.log('usssss')
                    $scope.goToNext();
                })
        // PostService.replaceOldFile(data.img, data.name).then(function () {
        //     // $scope.getPostDetails();
        //     // $scope.editorMode = false;
        //     $scope.goToNext();
        // })
    }

    $scope.fileChanges = function(data)
    {
        $scope.postDetails.post_files[data.ind].changes = data.img;
        console.log('changes called')
    }

    $scope.changeupperphoto=function(index,imgName){
        $scope.postDetails.post_files[index].name = $scope.postDetails.post_files[0].name;
        $scope.postDetails.post_files[0].name = imgName;
    }

    $scope.getPostListings = function () {
        //PostService.getPostList($scope.userDetails.id,'','',$stateParams.show ? '' : '1').then(function (data) {
        PostService.getPostList($scope.userDetails.id,'','','',1).then(function (data) {
            if (data.ack == 1) {
                $scope.userImageUrl = data.user_img;
                $scope.postImageUrl = data.file_url;
                $scope.postListings = data.list;
                console.log($scope.postListings);
            }
        })
    }

    $scope.saveComment = function (data) {
        $ionicLoading.show({
            //template: 'Saveing...'
            template:'<ion-spinner icon="ios"></ion-spinner> Saving...'
        });
        var cmntData = { comment: data.comment, user_id: $scope.userDetails.id, post_id: data.id }
        PostService.savePostComment(cmntData).then(function (res) {
            //console.log('saved data ', data);
            if (res.ack == 1) {
                //console.log('Ack 1', res);
                data.comment = '';
                $scope.getPostListings();
                if($stateParams.post_id)
                {
                    $scope.getPostById();
                }
                $ionicLoading.hide();
            } else {
                $ionicLoading.hide();
                ionicToast.show('Something went wrong! Please try again.');
            }
            setTimeout(function() {
                $ionicLoading.hide();
            }, 5000);
        });
    }

    $scope.saveSubComment = function (comment, post) {
        $ionicLoading.show({
            //template: 'Saveing...'
            template:'<ion-spinner icon="ios"></ion-spinner> Saving...'
        });
        var cmntData = { comment: comment.newcomment, user_id: $scope.userDetails.id, post_id: post.id, comment_id: comment.id }
        PostService.savePostComment(cmntData).then(function (res) {
            if (res.ack == 1) {
                comment.newcomment = '';
                $scope.getPostListings();
                if($stateParams.post_id)
                {
                    $scope.getPostById();
                }
                $ionicLoading.hide();
            } else {
                $ionicLoading.hide();
                ionicToast.show('Something went wrong! Please try again.');
            }
            setTimeout(function() {
                $ionicLoading.hide();
            }, 5000);
        })
    }

    $scope.postLike = function (post) {
        post.my_like = !post.my_like;
        var data = { post_id: post.id, user_id: $scope.userDetails.id }
        PostService.savePostLike(data).then(function (res) {
            if (res.ack == 1) {
                post.like_count = res.like_count
            }
        })
    }

    $scope.commentLike = function (comment) {
        var data = { comment_id: comment.id, user_id: $scope.userDetails.id };
        PostService.saveCommentLike(data).then(function (res) {
            if (res.ack == 1) {
                comment.like_count = res.like_count
                comment.comment_like = !comment.comment_like
            }            
        })
    }


    $scope.loadTags = function (a) {
        return PostService.getTagSuggetion(a).then(function (data) {
            if (data.ack == 1) {
              //console.log(data);
                return data.tags;
            }
            else {
                return [];
            }
        })

    }
    $scope.imggal = false;
    $scope.toggleImageGallery = function(){
        console.log('closing gallery');
        $scope.imggal = !$scope.imggal;
        
    }

    $scope.gotGalleyImages = function(images){
        console.log('hellooo ',images)
        $scope.imageArray = images.img;
        $scope.imggal = !$scope.imggal; 
        $timeout(function(){
            $scope.imageIsLoaded()
        },600)
        //$scope.imageIsLoaded();  
    }

    $scope.loadmemberTags = function (query) {
      if(query!=''){
        return $scope.gettagmemberlist.filter(function(superhero) {
          return superhero.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
      }else{
        return $scope.gettagmemberlist;
      }
      //console.log(a);
      // return PostService.getTagSuggetion(a).then(function (data) {
      //     if (data.ack == 1) {
      //       //console.log(data);
      //         return data.tags;
      //     }
      //     else {
      //         return [];
      //     }
      // })

  }

    $scope.selectTexture=function(value)
    {
        $scope.postSetForm.texture=value;
         $scope.IsShowText=!$scope.IsShowText
    }
    $scope.toggleTexture=function(){
        $scope.IsShowText=!$scope.IsShowText;
    }

    $scope.openDatepicker = function(){
        datePicker.show({
            date: $scope.postSetForm.dob?moment($scope.postSetForm.dob):new Date(),
            mode: 'date'
        }, function(data){
            $scope.postSetForm.dob = moment(data).format('L');
            $scope.$apply();
        }, function(err){
            console.log(' errr ',err);
        });
    }
    
    $scope.getPostById = function(){
        PostService.getPostById($stateParams.post_id,$scope.userDetails.id).then(function(data){
            if(data.ack == 1)
            {
                $scope.post = data.details;
                $scope.userImageUrl = data.user_img;
                $scope.postImageUrl = data.file_url;
            }
        });
        PostService.checkFavPost({user_id:$scope.userDetails.id, post_id:$stateParams.post_id}).then(function(data){
            //console.log(data);
            if(data.ack == 1)
            {
                if(data.count > 0){
                    $scope.isSaveFavPost = true;
                }
            }
        })
        
        
    }   
    $scope.curSlider = $ionicSlideBoxDelegate;
    
    $scope.slideOptions = {noSwiping: true,noSwipingClass: 'main-div'};


    /************** Album and Images ****************/
    $scope.getAlbums = function(){
        //console.log('getting albums');
        if(typeof(cordova) != 'undefined')
        {
            $ionicLoading.show({
                template: 'Getting Albums...'
            }); 
            cordova.plugins.photoLibrary.getAlbums(
                function (albums) {
                    $scope.allAlbums = albums;
                    $ionicLoading.hide();
                }, 
                function (err) {
                    $ionicLoading.hide();
                 }
              );
        }
        
    }    

    $scope.libraryToDataUrl = function (path,id, callback) {
        window.resolveLocalFileSystemURL(path, gotFile, fail);

        function fail(e) {
            console.log('Cannot found requested file',e);
        }

        function gotFile(fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    var content = this.result;
                    callback(content,id);
                };
                reader.readAsDataURL(file);
            });
        }
    }
    $scope.albumImages = [];
    $scope.getAllImages = function(albumid){
        console.log('started')
        
        if(typeof(cordova)!= 'undefined')
        {
            $scope.library = [];
            //alert(typeof(cordova))
            if(!albumid)
            {
                cordova.plugins.photoLibrary.getLibrary(
                    function (result) {
                        // console.log('result ',result)
                    
                        
                    result.library.forEach(function(libraryItem) {    
                        //console.log(' on loop');                   
                        if(libraryItem.filePath)
                        {
                            if(!albumid || (albumid && libraryItem.albumIds.indexOf(albumid)>=0))
                            {       
                               // console.log('album id 1',albumid,libraryItem.albumIds);                        
                                $scope.library.push({imagesrc : window.Ionic.WebView.convertFileSrc("file:///" + libraryItem.filePath),file_url:"file://" + libraryItem.filePath,id : libraryItem.id});
                                $scope.albumImages.push({imagesrc : window.Ionic.WebView.convertFileSrc("file:///" + libraryItem.filePath),file_url:"file://" + libraryItem.filePath,id : libraryItem.id,albumids: libraryItem.albumIds});
                                $scope.$apply();
                                
                                // $scope.libraryToDataUrl("file://" + libraryItem.filePath,libraryItem.id, function (base64Image,id) {
                                //     $scope.library.push({image : base64Image,id : id});
                                //     $scope.albumImages.push({image : base64Image,id : id,albumids: libraryItem.albumIds});                                    
                                // });
                            }
                            
                        }
                        else
                        {
                        }
                    
                    })
                
                
                    },
                    function (err) {
                        // console.log('errr',err);
                        cordova.plugins.photoLibrary.requestAuthorization(
                            function () {
                                $scope.getAllImages($scope.selectedAlbum);
                                $scope.getAlbums();
                            },
                            function (err) {
                                console.log('err',err)
                            }, // if options not provided, defaults to {read: true}.
                            {
                            read: true,
                            write: true
                            }
                        );
            
                    },
                    { // optional options
                        
                        includeAlbumData: true, // default
                        itemsInChunk: 100, 
                        chunkTimeSec: 0.5, 
    
                    }
                );
            }
            else
            {
                $scope.albumImages.forEach(function(image){
                    console.log('album id 2',image,image.albumIds,image.albumIds.indexOf(albumid));
                    if((albumid && image.albumIds.indexOf(albumid)>=0))
                    {
                        $scope.library.push({imagesrc : image.imagesrc,id : image.id,file_url:image.file_url});
                    }
                })
            }
        }        
    }

    
        
    //$scope.getAlbums();
    //$scope.getAllImages($scope.selectedAlbum);
    $scope.showImageAlbum = false;
    $scope.changeAlbum = function(){
        $scope.getAllImages($scope.selectedAlbum);
    }
    $scope.selectedImages = []
    $scope.selectLibraryImage = function(image){
        $scope.videoFile = '';
        if(!$scope.selectedImages[image.id])
        {            
            console.log(' id before ',image.id)
            console.log('length ', $scope.imageArray.length);
            if($scope.imageArray.length<4)
            {     
                $ionicLoading.show({
                    template: 'Loading...'
                });       
                $scope.imageArray.unshift(image.imagesrc);
                $scope.selectedImages[image.id] = image.imagesrc;     
                // $scope.libraryToDataUrl(image.file_url,image.id, function (base64Image,id) {
                //     console.log('success ++ ',$scope.imageArray.indexOf(base64Image));
                //     if($scope.imageArray.indexOf(base64Image)==-1)
                //     {
                //         console.log(' id after ',id)
                //         $scope.imageArray.unshift(base64Image)
                //         $scope.selectedImages[id] = base64Image;   
                //         // $ionicSlideBoxDelegate.update();
                //         // $ionicSlideBoxDelegate.slide(0);
                        
                        
                //     }
                                              
                // });
                
            }            
        }
        else
        {
            console.log('heyyy');
            
            var index = $scope.imageArray.indexOf(image.imagesrc);
            delete $scope.selectedImages[image.id];
            if (index > -1) {
                $scope.imageArray.splice(index, 1);
            }
            $ionicSlideBoxDelegate.update();
            $ionicSlideBoxDelegate.slide(0);
        }
    }
    $scope.showImageAlbum = false;
    $scope.showAlbumDiv = function(){
        $scope.showImageAlbum = !$scope.showImageAlbum
        if($scope.showImageAlbum)
        {
            $scope.getAlbums();
            $scope.getAllImages();
        }
        $location.hash('album-contain-div');
        $ionicScrollDelegate.anchorScroll(true);
    }

    $scope.reinitailizeCroppie = function(i) {
        
        if($scope.imageArray.length>0)
        {
            console.log('loaded ')
            $timeout(function(){
                $('.cropperbox-0').croppie({
                    showZoomer: false,
                    enableOrientation: true,                                
                    viewport: {
                        width: '100%',
                        height: '100%'
                    }
                });
                console.log('crop enabled')
                $ionicLoading.hide();
            })
                      
        }
        
    }

    $scope.prevSlide = function(){
        $ionicSlideBoxDelegate.previous();
    }

    $scope.nextSlide = function(){
        $ionicSlideBoxDelegate.next();
    }
    $scope.pageNo = 1;
    $scope.lastPage = false;
    $scope.postListings = [];
    $scope.populateList = function(){
        $scope.pageNo++;
        console.log('page no ',$scope.pageNo)
        PostService.getPostList($scope.userDetails.id,'','','',$scope.pageNo).then(function (data) {
            if (data.ack == 1) {
                if(data.list.length>0)
                {
                    data.list.forEach(function(value,ind){
                        $scope.postListings.push(value);
                    })
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
                else
                {
                    $scope.lastPage = true;
                }
            }
            else
            {
                $scope.lastPage = true;
            }
        })
    }
    $scope.liblimit =32;
    $scope.increaseLimit = function () {
        console.log('called');
        if ($scope.liblimit < $scope.library.length) {
          $scope.liblimit += 40;
        }
    };
    /************** Album and Images ****************/
})
