app.controller('RootsCtrl', function($location,$scope,$rootScope,$state,UserService,PostService,$interval,$filter,$ionicPopup,$ionicSlideBoxDelegate,ionicToast,$ionicLoading,$ionicPlatform){  
    function handleOpenURL(url) {
        setTimeout(function() {
            console.log(url);
          //alert("received url: " + url);
        }, 0);
    }

    function handleOpenURL1(URL_SCHEME) {
        setTimeout(function() {
            console.log(URL_SCHEME);
          //alert("received url: " + url);
        }, 0);
    }

    // window.plugins.launchmyapp.getLastIntent(function(url) {
    //     if (intent.indexOf('rootsfolio://' > -1)) {
    //         console.log("received url: " + url);
    //     } else {
    //         return console.log("ignore intent: " + url);
    //     }
    // }, function(error) {
    //     return console.log("no intent received");
    // });

    //handleOpenURL();
    //handleOpenURL1();
    $rootScope.showhidediv = false;
    $scope.newInboxMsg= false;
    $scope.allUserList= [];
    $scope.locationpath = $location.path();
    $scope.serchhide = false;
    $rootScope.showbelowuserinfooff = true;
    $scope.newNotification = false;
    $rootScope.saveFavPostId = null;
    $scope.useridprams = $state.params.user_id;
    $scope.cityprams = $state.params.city;
    $scope.distanceprams = $state.params.distance;
    //$rootScope.followonof = false;
    $scope.goToSettings=function()
    {
        $state.go('settings');
    }
    $rootScope.userDetails = UserService.getUserInfo();
    //console.log('$rootScope.userDetails',$rootScope.userDetails);
    $scope.$on('$locationChangeStart', function($event, next, current) {
        $scope.locationpath = $location.path();
        if($scope.locationpath == '/location/' || $scope.locationpath == '/find-service/' || $scope.locationpath == '/create-post' || $scope.locationpath ==  '/post-settings/' || $scope.locationpath == '/notifications' || $scope.locationpath == '/message-chatbox' || $scope.locationpath == '/service-area' || $scope.locationpath == '/notifications-list' || $scope.locationpath == '/single-result/'+ $scope.useridprams + '/' + $scope.cityprams + '/' + $scope.distanceprams){
            $rootScope.showbelowuserinfooff =  false;
        }
    })
    // if ($scope.locationpath == '/create-post') {
    //     $scope.showbelowuserinfooff =  false;
    // }

    $rootScope.SelectToggle = function (postsocial) {
        //console.log('postsocial',postsocial);
        postsocial.showsocialpopup = !postsocial.showsocialpopup;
    }

    $rootScope.SelectToggle2 = function (post) {
        $rootScope.saveFavPostId = post.id;
        post.showpopup = !post.showpopup;
        UserService.getFollowUnfollow(post.user_id,$rootScope.userDetails.id).then(function(data){
            if(data.ack == 1)
            {
                post.followonof = data.following;
            }
        });
        UserService.getBlockUnblock(post.user_id,$rootScope.userDetails.id).then(function(data){
            if(data.ack == 1)
            {
                post.blockonof = data.block;
            }
        });
        UserService.checkpostnoti($rootScope.userDetails.id,post.id).then(function(data){
            if(data.ack == 1)
            {
                post.postnotionoff = data.noti;
            }
        });
    }

    $rootScope.goToSavesPostPage = function (){
        if($rootScope.saveFavPostId != null){
            $ionicLoading.show({
                template: 'Please wait...'
            })
            PostService.saveFavPost({user_id:$rootScope.userDetails.id, post_id:$rootScope.saveFavPostId}).then(function (result) {
                $ionicLoading.hide()
                if (result.ack == 1) {
                    if(result.type == 1){
                        ionicToast.show('Post Save Successfully.');
                    }else{
                        ionicToast.show('Post Remove Successfully.');
                    }
                    $rootScope.SelectToggle2(null);
                    //$state.go('post-images', { post_id: result.post_id });
                }
            })
        }
    }

    $rootScope.followUserroot = function(post){
        var followData = {
            user_by : $rootScope.userDetails.id,
            user_to : post.user_id
        }
        $ionicLoading.show({
            template: 'Please wait...'
        })
        UserService.followUser(followData).then(function(data){
            if(data.ack == 1)
            {
                UserService.getFollowUnfollow(post.user_id,$rootScope.userDetails.id).then(function(data){
                    if(data.ack == 1)
                    {
                        if(data.following){
                            post.followonof = data.following;
                            ionicToast.show('You have started following '+ post.user.name + ' .');
                        } else {
                            post.followonof = data.following;
                            ionicToast.show('You have unfollowed '+ post.user.name + ' .');
                        }
                    }
                    $ionicLoading.hide();
                });
            }
            else
            {
                ionicToast.show('Internal error. Please try again later');
            }
        })
    }

    $rootScope.postNotiroot = function(post){
        var ntipostData = {
            post_id : post.id,
            user_to : $rootScope.userDetails.id
        }
        $ionicLoading.show({
            template: 'Please wait...'
        })
        UserService.addPostNoti(ntipostData).then(function(data){
            if(data.ack == 1)
            {
                UserService.checkpostnoti($rootScope.userDetails.id,post.id).then(function(data){
                    if(data.ack == 1)
                    {
                        if(data.noti){
                            post.blockonof = data.noti;
                            ionicToast.show('You have turned on notification for this post.');
                        } else {
                            post.blockonof = data.noti;
                            ionicToast.show('You have turned off notification for this post.');
                        }
                    }
                    $ionicLoading.hide();
                });
            }
            else
            {
                ionicToast.show('Internal error. Please try again later');
            }
        })

    }

    $rootScope.blockUserrootsetting = function(id,id1){
        console.log('hittd');
        var followData = {
            user_by : id,
            user_to : id1
        }
        $ionicLoading.show({
            template: 'Please wait...'
        })
        UserService.blockUser(followData).then(function(data){
            if(data.ack == 1)
            {
                $rootScope.getAllBlockAcount();
                $ionicLoading.hide();
            }
            else
            {
                ionicToast.show('Internal error. Please try again later');
            }
        })
    }

    $rootScope.showhide = function(){
        $rootScope.showhidediv = !$rootScope.showhidediv;
    }

    $rootScope.blockUserroot = function(post){
        var followData = {
            user_by : $rootScope.userDetails.id,
            user_to : post.user_id
        }
        $ionicLoading.show({
            template: 'Please wait...'
        })
        UserService.blockUser(followData).then(function(data){
            if(data.ack == 1)
            {
                UserService.getBlockUnblock(post.user_id,$rootScope.userDetails.id).then(function(data){
                    if(data.ack == 1)
                    {
                        if(data.block){
                            post.blockonof = data.block;
                            ionicToast.show('You have blocked '+ post.user.name + ' .');
                        } else {
                            post.blockonof = data.block;
                            ionicToast.show('You have unblocked '+ post.user.name + ' .');
                        }
                    }
                    $ionicLoading.hide();
                    $scope.getPostListings();
                });
            }
            else
            {
                ionicToast.show('Internal error. Please try again later');
                $scope.getPostListings();
            }
        })
    }
  
    var onSuccess = function(result) {
        console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
        console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
    };
    
    var onError = function(msg) {
        console.log("Sharing failed with message: " + msg);
    };
  
    $rootScope.sendsocialShare = function(post){
        // this is the complete list of currently supported params you can pass to the plugin (all optional)
        var options = {
            message: 'Share this Post', // not supported on some apps (Facebook, Instagram)
            subject: 'RootsFolio Post Share ',
            //message: 'rootsfolio://', // not supported on some apps (Facebook, Instagram)
            files: ['', ''], // an array of filenames either locally or remotely
            url: 'rootsfolio://listings-details/'+post.id
        };
        window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
    }

    $rootScope.goToChatDetailsroot = function(loginUserId, toUserId){
        var roomId='';
        if(loginUserId>toUserId){
            roomId = toUserId+'_'+loginUserId;
        }else{
            roomId = loginUserId+'_'+toUserId;
        }
        $state.go('message-chatbox',{roomId:roomId});
        //console.log(roomId);
    }

    $rootScope.startSocialShare=function()
    {
        $state.go('social-share');
    }

    $scope.getUserListRoot = function(id){
        UserService.getUserList('').then(function (data) {
            if(data.ack == 1){
                //console.log('user',data.list);
                $scope.allUserList = data.list;
            }
        });
    }

    $scope.getAutocompleteUser = function(q){
        return $filter('filter')($scope.allUserList,q);
    }

    $scope.searchboxhideshow = function(val){
        $scope.serchhide = val;
    }

    $scope.setSelectedUser = function(user){
        if(user)
        {
          $state.go('member-profile-member-view',{userId: user.item.id});
          //console.log('user',user);
        }
    }

    $scope.goToFilter=function(){
        $state.go('filter');
    }

    $scope.logout = function(){
        UserService.logout();
        $state.go('login');
    }
    if(UserService.getUserInfo())
    {
        $rootScope.userDetails = UserService.getUserInfo();
    }
    $rootScope.reassignUser = function(){
        if(UserService.getUserInfo())
        {
            $rootScope.loginUserDetails = UserService.getUserInfo();
            //console.log($rootScope.loginUserDetails)
        }
    }

    var wsUri = "ws://111.93.169.90:25764/socket.php";
    websocket = new WebSocket(wsUri);
    websocket.onmessage = function(e){
        var msg = JSON.parse(e.data);
        var type = msg.type; //message type
        var umsg = msg.message;
        var usreceiverid = msg.receiver_id;
        // console.log(msg);
        if(umsg!=null && type != 'system')
        {
            if(type == 'usermsg')
            {
                if(usreceiverid == $rootScope.userDetails.id){
                  UserService.getNewMsgCnt({user_id:$rootScope.userDetails.id}).then(function(res){
                      //console.log(res);
                      if(res.ack==1)
                      {
                          $scope.newInboxMsg= true;
                      }
                  })
                }
            }
        }else{
         
        }
    }

    

    $rootScope.getAllBlockAcount = function()
    {
        UserService.getBlocksAccount($rootScope.userDetails.id).then(function(data){
            if(data.ack==1)
            {
                console.log('getBlocksAccount',data);
                $rootScope.blockList = data.block;
            }
            else
            {
                $rootScope.blockList = [];
            }
        })
    }
    $scope.getNewNotifications = function()
    {
        console.log('hiiii-----')
        $interval(function(){UserService.getNewNotifications($rootScope.userDetails.id).then(function(data){
            console.log('received')
            if(data.ack==1)
            {
                console.log('new noti received')
                $scope.newNotification = true;
            }
            else
            {
                $scope.newNotification = false;
            }
            if(!$scope.$$phase)
            {
                $scope.$apply();
            }
            UserService.getNewMsgCnt({user_id:$rootScope.userDetails.id}).then(function(res){
                if(res.ack==1)
                {
                    $scope.newInboxMsg= true;
                }
            })
        })},5000)
        
    }
    
    $ionicPlatform.ready(function () {
        $scope.getNewNotifications();
        });
   
    
   
   //$scope.getNewNotifications();
    // console.log($rootScope.notiInterval);
    // $scope.$on('$locationChangeStart', function($event, next, current) { 
       
    //     $interval.cancel();
    //     // $rootScope.notiInterval =$interval($scope.getNewNotifications,5000)
    //   });
    

});
