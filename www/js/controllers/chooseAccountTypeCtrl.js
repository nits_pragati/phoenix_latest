app.controller('chooseAccountTypeCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, $ionicLoading, $ionicScrollDelegate,$location, UserService,CommonService,PostService,$timeout,$sce) {
    var value;
    $scope.userDetData='';
    $scope.searchPost='';
    $scope.postType=[];
    $scope.getHashTagList=[];
    $scope.selHashTag='';
    $scope.curlpattern_id ='';
    
    $scope.postSearchDetails = {};
    $scope.userDetails=UserService.getUserInfo()
    if($scope.userDetails &&  $scope.userDetails.profile_image)
    {
        $scope.userDetails.profile_image=$scope.userDetails.image_url+$scope.userDetails.profile_image;
    }
    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
      }
    $scope.getUserId=$stateParams.userId;
    // if(!$scope.getUserId){
    //     $state.go('listings');
    // }
    $scope.goToRegister=function()
    {
        $state.go('register');
    }
    $scope.goToChatDetails = function(loginUserId, toUserId){
        var roomId='';
        if(loginUserId>toUserId){
            roomId = toUserId+'_'+loginUserId;
        }else{
            roomId = loginUserId+'_'+toUserId;
        }
        $state.go('message-chatbox',{roomId:roomId});
        //console.log(roomId);
    }

    $scope.allHashTagList = function(){
        UserService.getHashTagList().then((data)=>{
            if(data.ack==1)
            {
                $scope.getHashTagList=data.tags;
                //console.log($scope.getHashTagList);
            }
        })
    }

    $scope.allHashTagList();

    $scope.goToMessageImgupload=function()
    {
        $state.go('message-imgupload');
    }

    $scope.changebigphoto=function(index,imgName){
        $scope.userDetData.user_photos[index].name = $scope.userDetData.user_photos[0].name;
        $scope.userDetData.user_photos[0].name = imgName;
    }

    $scope.goToMemberMainProfile=function()
    {
        $state.go('member-profile-member-view');
    }
    $scope.goToProviderMainProfile=function()
    {
        $state.go('provider-profile-view');
    }
    $scope.goTomemberProfilePreferences=function()
    {

        $state.go('member-profile-preferences');
    }
    $scope.goToFeedback=function()
    {
        $state.go('feedback-page');
    }

    $scope.goToMemberForm=function()
    {
        $state.go('member-signup-form');
    }
    $scope.goToProviderForm=function()
    {
        $state.go('provider-signup-form');
    }
    
    $scope.SelectHashTag=function(shTag)
    {
        $scope.selHashTag=shTag.id;
        //console.log(shTag.id);
    }

    $scope.SelectHashTagAll=function(shTag)
    {
        $scope.selHashTag= shTag;
        //console.log(shTag.id);
    }

    $scope.filterPost=function(sdata)
    {
        //console.log($scope.postSearchDetails);
        var postTypeJson = '';
        //var postTypeJson = JSON.stringify($scope.postSearchDetails);
        angular.forEach($scope.postSearchDetails, function(value, key){
            postTypeJson+=key+',';
        });
        //postTypeJson+='&'+$scope.selHashTag;
        $state.go('filterpost',{ post_type : postTypeJson, skeyword : sdata , tags : $scope.selHashTag,curl : $scope.curlpattern_id});
    }

    $scope.SelectToggle=function() 
    {
        $scope.IsActive=!$scope.IsActive;
        $timeout(function(){
            $ionicScrollDelegate.resize();
        })        
    }

    $scope.selectCurlPattern=function(id){
        $scope.curlpattern_id = id;
    }

    $scope.getCurlCategories = function(){
        CommonService.getCurlCategories().then(function(data){
            if(data)
            {
                $scope.curlCategories = data.categories;
            }
        })
    }

    $scope.goToCurl=function()
    {
        $state.go('curl-root');
    }


    $scope.back=function($page)
    {
        $location.path('/privacy/'+$page);
    }

    $scope.pro=function()
    {
        $state.go('member-profile-preferences')
    }

    $scope.goToFeed=function()
    {
        $state.go('news-feed')
    }

    $scope.goToEplore=function()
    {
        $state.go('explore')
    }

    $scope.goToNewPost=function()
    {
        $state.go('create-post')
    }

    $scope.goToNotification=function()
    {
        $state.go('notifications-list');
    }

    $scope.goToMessage=function()
    {
        $state.go('message-chatbox');
    }
    $scope.goToSaves=function()
    {
        $state.go('saves');
    }


    $scope.goToNotDetails=function()
    {
        $state.go('notifications')
    }


    $scope.openCamera=function()
    {
        $state.go('message-camera')
    }

    $scope.openMessageImgs=function()
    {
        $state.go('message-img')
    }
    $scope.openMessageLoading=function()
    {
        $state.go('message-loading')
    }

    $scope.goToFilter=function()
    {
        $state.go('filter')
    }


    $scope.goToNewsDetails=function()
    {
        $state.go('news')
    }

    $scope.startSocialShare=function()
    {
        $state.go('social-share')
    }

    $scope.goToSinglePhotos=function()
    {
        $state.go('search-result-photos',{user_id:$stateParams.user_id})
    }

    $scope.goToSingleReviews=function()
    {
        $state.go('single-result-review')
    }


    $scope.goToSettings=function()
    {
        $state.go('settings')
    }

    $scope.goToProviderSetting=function()
    {
        $state.go('provider-setting')
    }

    

    $scope.goToSearchResult=function()
    {
        $state.go('search-result')
    }

    $scope.goToSingleResult=function()
    {
        $state.go('single-result',{user_id:$stateParams.user_id})
    }


    $scope.goToProviderProfilePreferences=function()
    {
        $state.go('provider-profile-preferences')
    }

    $scope.goToLocResult=function()
    {
        $state.go('location-result')
    }
    


    $scope.goToFindService=function()
    {
        $state.go('find-service')
    }
    $scope.menuToggle=function(ev)
    {
        $scope.IsActive=!$scope.IsActive;

    }
    $scope.IsShowRootCurl=false;

    $scope.showRootCurl=function()
    {

        $scope.IsShowRootCurl = !$scope.IsShowRootCurl;
    }


    $scope.goBackToFind=function()
    {
        $state.go('find-service')
    }


    $scope.SelectToggle2=function()
    {
        $scope.IsActive2=!$scope.IsActive2;
    }


   

    $scope.onChangeAdvance=function(event)
    {
        $scope.AdvancedOption=!$scope.AdvancedOption;
    }

    $scope.closeFilter=function()
    {
        $state.go('listings');
    }

    $scope.closeOption=function()
    {
    $scope.IsActive2=false;
    }

    $scope.goBackToFeedback=function()
    {
        $state.go('feedback-page')
    }

    $scope.goToPreference=function()
    {

        //$state.go('provider-profile-preferences')
        $state.go('member-profile-member-view', {userId:$scope.getUserId});
    }
    $scope.goToPreferenceForProvider = function () {

    $state.go('provider-profile-preferences')
    }

    $scope.goToSavesPage=function()
    {
        $state.go("saves");
    }

    $scope.goToFindServiceList=function()
    {
        $state.go("find-service")
    }

    $scope.goToRegister1=function()
    {
        //$state.go('member-signup')
        $state.go('forgot-password')
    }

    $scope.ChooseAccountBack=function()
    {
        $state.go('create-account')
    }

    $scope.goToChooseAccount=function()
    {
        $state.go('choose-account')
    }

    $scope.goToMemSignUp=function()
    {
        $state.go('member-signup')
    }

    $scope.slideHasChanged=function($index)
    {
        //console.log($index);
    }

    $scope.goToProvider=function($i)
    {
        $ionicSlideBoxDelegate.slide($i);
    }

    $scope.goToProviderFromPro=function($i)
    {
        $state.go('choose-account')
        $ionicSlideBoxDelegate.slide($i);
    }
    $scope.getUserDetails1=function(){

        if(!UserService.getUserInfo())
        {
            $state.go('login')
        }
        else
        {
            if(!$scope.getUserId)
            {
                $scope.getUserId = $scope.userDetails.id
            }
            UserService.getUserPostPhotosnew($scope.getUserId).then((userPhotos)=>{
                $scope.userPhotos=[];
                if(userPhotos.ack==1)
                {
                    var userDetails=UserService.getUserInfo();
                    for (let index = 0; index < userPhotos.photos.length; index++) {
                        userPhotos.photos[index].image=userDetails.image_url+userPhotos.photos[index].name;

                    }
                    $scope.userPhotos=userPhotos.photos;
                    //console.log('$scope.userPhotos',$scope.userPhotos);
                }
            })
            UserService.getUserDetailsData($scope.getUserId,$scope.userDetails.id).then(function(data){
              $scope.currentUser=UserService.getUserInfo();
                if(data.ack == 1)
                {
                    $scope.userDetData = data.details;
                    if($scope.userDetData.curlpattern_id)
                    {
                        CommonService.getCurlDetailsById($scope.userDetData.curlpattern_id).then((curl)=>{
                            if(curl.ack==1)
                            {
                                $scope.selectedCurlPatten=curl.details;
                            }
                        })
                    }
                    if($scope.userDetData &&  $scope.userDetData.profile_image)
                    {
                        $scope.userDetData.profile_image=$scope.userDetData.image_url+$scope.userDetails.profile_image;
                    }
                }
               //console.log(data)
            })
        }

    }
    $scope.userDetails=UserService.getUserInfo();
    if($scope.userDetails &&  $scope.userDetails.profile_image)
    {
        $scope.userDetails.profile_image=$scope.userDetails.image_url+$scope.userDetails.profile_image;
    }

    $scope.followUser = function(){
        var followData = {
            user_by : $scope.userDetails.id,
            user_to : $scope.getUserId
        }
        UserService.followUser(followData).then(function(data){
            if(data.ack == 1)
            {
                $scope.getUserDetails1();
            }
            else
            {
                ionicToast.show('Internal error. Please try again later');
            }
        })
    }

    $scope.updateUserFields = function(data){
        console.log(data)
        UserService.updateUserFields($scope.userDetails.id,data).then(function(result){

        })
    }

    $scope.getMyDetails = function(){
        UserService.getUserDetails($scope.userDetails.id).then(function(result){
            console.log(' ---- ',result)
            if(result.ack == 1)
            {
                $scope.is_hidden = result.details.is_hidden
                $scope.follower_noti = result.details.follower_noti
                $scope.likes_noti = result.details.likes_noti
                $scope.comments_noti = result.details.comments_noti
                $scope.message_noti = result.details.message_noti

                $scope.follower_noti = result.details.follower_noti
                
                
                
                
            }
        })
    }

    $scope.deleteMyAccount = function(){
        var myPopup = $ionicPopup.show({            
            title: 'Confirm!',
            subTitle: 'Are you sure you want to delete your account?',
            scope: $scope,               
            buttons: [
               { text: 'Cancel' }, {
                  text: '<b>Delete</b>',
                  type: 'button-danger',
                  onTap: function(e) {
                    UserService.updateUserFields($scope.userDetails.id,{is_hidden:1,is_delete:1}).then(function(result){
                        if(result.ack==1)
                        {
                            $scope.logout();
                        }
                        else
                        {
                            ionicToast.show('Internal Error. Please try again later'); 
                        }
                    })  
                     if (!$scope.data.model) {
                        //don't allow the user to close unless he enters model...
                        e.preventDefault();
                     } else {
                        return $scope.data.model;
                     }
                  }
               }
            ]
         });
   
         myPopup.then(function(res) {
            myPopup.hide();
            console.log('Tapped!', res);
         });    
    }
    
});
