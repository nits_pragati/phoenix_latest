app.controller('locationCtrl', function($scope, $state,$filter, $ionicSlideBoxDelegate, $ionicPopup,ionicToast,$rootScope,$ionicLoading, $stateParams,UserService, $http, $timeout, PostService ) {
    if(!UserService.getUserInfo())
    {
        $state.go('login');
    }
    $scope.postList = [];
    $scope.getPostList = function(){
        PostService.getPostList('','').then(function(data){
            if(data.ack==1)
            {
                $scope.postList = data.list;
                $scope.postImageUrl = data.file_url;
            }
            console.log(' -------- ',data)
        })
    }

    $scope.usersList = [];
    $scope.getUserList = function(){
        UserService.getUserSearch({service_type : $stateParams.service_type, city_id : $stateParams.city_id}).then(function(data){
            if(data.ack == 1)
            {
                $scope.usersList = data.list;
                $scope.userImageUrl = data.file_url;
            }
        })
    }

    $scope.goToSingleResult=function(id)
    {
        $state.go('single-result',{user_id : id});
    }
    $scope.goToSinglePhotos=function()
    {
        $state.go('search-result-photos',{user_id:$stateParams.user_id})
    }
    $scope.userDetails = []
    $scope.getUserDetails = function(){        
        UserService.getUserDetailsById($stateParams.user_id).then(function(data){
            if(data.ack==1)
            {
                $scope.userDetails = data.details;
                $scope.userImageUrl = data.details.image_url;
            }
            else
            {
                ionicToast('Invalid user');
                $state.go('location');
            }

        })
    }
    $scope.getPostListImage = function(){        
        PostService.getPostListImage($stateParams.user_id).then(function(data){
            if(data.ack==1)
            {
                $scope.photos = data.list.post_files;
                $scope.photourl = data.file_url;
            }
            else
            {
                ionicToast('Invalid user');
                $state.go('location');
            }

        })
    }
    navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        console.log(pos);
        $scope.map.setCenter(pos);
        $ionicLoading.hide();
      });
    
    $scope.city_id = $stateParams.city_id;
    $scope.getCityDetails = function(){
        if($scope.city_id)
        {
            console.log('hii');
            PostService.getCityDetails($scope.city_id).then(function(data){
                if(data.ack==1)
                {
                    $scope.cityDetails = data.details;
                    $scope.selectedServiceLoc = data.details.name;
                }
            })
        }        
    }

    $scope.getCityList = function(){
        PostService.getCityList().then(function(data){
            if(data.ack == 1)
            {
                $scope.cityList = data.details;
                
            }
        })
    }

    $scope.getAutocompleteCity = function(q){
        return $filter('filter')($scope.cityList,q);
    }

    $scope.setSelectedCity = function(city){    
        if(city)
        {
            $state.go('location',{service_type : $stateParams.service_type, city_id : city.item.id})
        }  
    }
})
