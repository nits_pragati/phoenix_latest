app.controller('chatCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, $ionicLoading, $ionicScrollDelegate, $location, UserService, $timeout,$http) {
    var value;
    var websocket;
    $scope.toUserDetData='';
    $scope.toUserId='';
    $scope.allChats = [];
    $scope.chatFileImgUrl = '';
    $scope.chatSelOptType = 'camera';
    //$scope.attachChatFile = [];
    $scope.message = {userMessage: ''};

    $scope.userDetails=UserService.getUserInfo()
    if($scope.userDetails &&  $scope.userDetails.profile_image)
    {
        $scope.userDetails.profile_image=$scope.userDetails.image_url+$scope.userDetails.profile_image;
    }

    $scope.getToUserDetails=function(){
        UserService.getUserDetailsData($scope.toUserId).then(function(data){
            if(data.ack == 1){
                $scope.toUserDetData = data.details;
            }
            //console.log('toUserDetData',$scope.toUserDetData);
        })
    }
    //console.log(new Date());
    // $timeout(function() {
    //     $ionicScrollDelegate.scrollBottom();
    // });


    $scope.getRoomId=$stateParams.roomId;
    if(!$scope.getRoomId){
        $state.go('listings');
    }else{
        var splitStr = $scope.getRoomId.split('_');
        if($scope.userDetails.id != splitStr[0]){
            $scope.toUserId = splitStr[0];
            $scope.getToUserDetails();
        }else if($scope.userDetails.id != splitStr[1]){
            $scope.toUserId = splitStr[1];
            $scope.getToUserDetails();
        }
    }
    //console.log('room id',$scope.getRoomId);
    $scope.goToRegister=function()
    {
        $state.go('register');
    }

    $scope.swipeLeftEvent = function(event,id,index){
        //console.log('Swipe Event',event);
        if(event.type == "swipeleft"){
            if(id !=''){
                $scope.allChats.splice(index,1);
                //array.splice(index, 1);
                UserService.deleteChatMsg(id).then(function(data){
                    if(data.ack == 1){
                        ionicToast.show('Massage deleted sucessfully.');
                        //$scope.toUserDetData = data.details;
                    } else {
                        ionicToast.show('Something went wrong! Please try again.');
                    }
                })
            } else {
                //$scope.allChats.splice(index,1);
            }
        }
    }

    $scope.getChats = function(){
        // $ionicLoading.show({
        //     template:'Loading ...'
        // })
        var req = {room_id : $scope.getRoomId,user_id:$scope.userDetails.id};
        UserService.getChatsdata(req).then(function(res){
            if(res.ack==1)
            {
                $scope.allChats = res.details.chat_data;
                $scope.chatFileImgUrl = res.details.image_url;
            }
            //console.log($scope.allChats);
            //$ionicLoading.hide();
            //$scope.server_url = res.server_url;
            //$scope.receiver = res.receiver
            $timeout(function () {
                //$ionicScrollDelegate.scrollBottom(true);
                $ionicScrollDelegate.$getByHandle('small').scrollBottom();
            }, 400);

        })

        //var wsUri = "ws://192.168.1.68:25763/server.php";
        //var wsUri = "ws://192.168.1.118:25763/socket.php";
        var wsUri = "ws://111.93.169.90:25764/socket.php";

	    websocket = new WebSocket(wsUri);
        //console.log(websocket);
        websocket.onmessage = $scope.gettingMessage
    }
    $scope.gettingMessage = function(ev) {

        var msg = JSON.parse(ev.data); //PHP sends Json data
        //console.log(msg);
        var type = msg.type; //message type
        var umsg = msg.message; //message text
        var uname = msg.name; //user name
        //var ureceivename = msg.receive_name;
        var usenderid = msg.sender_id;
        var usreceiverid = msg.receiver_id;
        var userGetRoomid = msg.room_id;
        var msg_type = msg.msg_type;

        //if(umsg!=null && uname!=null)
        if(umsg!=null)
        {
            if(type == 'usermsg')
            {

                if(usreceiverid == $scope.userDetails.id && userGetRoomid== $scope.getRoomId){
                    var msgDisplay = {
                        message: umsg,
                        room_id: userGetRoomid,
                        sender_id : usenderid,
                        receiver_id: usreceiverid,
                        type: msg_type,
                        is_read: 1,
                        id:'',
                        cdate:new Date()
                    };

                    $scope.allChats.push(msgDisplay)
                    $scope.$apply()
                    //console.log($scope.allChats);
                    $timeout(function () {
                        $ionicScrollDelegate.$getByHandle('small').scrollBottom();
                    }, 400);

                    //var msg = {read_msg:true,user_id:$scope.sessionuserInfo.id,crownfabrication_id:crownfabrication_id,msg_id:msg.msg_id};
                    //websocket.send(JSON.stringify(msg));
                }
            }
            else if(type=='typing')
            {

                // if(ureceivename == $scope.sessionuserInfo.id && crownfabrication_id== $stateParams.crownId)
                // {
                //     //console.log(msg);
                //     $scope.typingUser = msg;
                //     if(!$scope.$phase)
                //     {
                //         $scope.$apply();
                //     }
                // }
            }
            else if(type='stop_typing')
            {
                // if(ureceivename == $scope.sessionuserInfo.id && crownfabrication_id== $stateParams.crownId)
                // {
                //     delete $scope.typingUser;
                //     if(!$scope.$phase)
                //     {
                //         $scope.$apply();
                //     }
                // }
            }
        }
    }
    //$scope.message = {userMessage: ''};
    $scope.sendMessage = function(){

        if($scope.message.userMessage){
            var msg = {
                message: $scope.message.userMessage,
                room_id: $scope.getRoomId,
                sender_id : $scope.userDetails.id,
                receiver_id: $scope.toUserId,
                type: 1,
                is_read: 0,
                id:'',
                cdate:new Date()
            };
            var msgInsert = {
                message: $scope.message.userMessage,
                room_id: $scope.getRoomId,
                sender_id : $scope.userDetails.id,
                receiver_id: $scope.toUserId,
                type: 1,
                is_read: 0
            };
            //console.log(msgInsert);
            $scope.message = {userMessage: ''};
            websocket.send(JSON.stringify(msgInsert));
            $scope.allChats.push(msg);
            //Chats reinitilise
            var reqforchat = {room_id : $scope.getRoomId,user_id:$scope.userDetails.id};
            UserService.getChatsdata(reqforchat).then(function(res){
                if(res.ack==1)
                {
                    console.log(' hhhhhhhhh ',res.details.chat_data)
                    if(!$scope.allChats || $scope.allChats.length==0)
                    {
                        $scope.allChats = res.details.chat_data;
                        $scope.chatFileImgUrl = res.details.image_url;
                    }
                    
                }
            });
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('small').scrollBottom();
            }, 400);
            //$ionicScrollDelegate.scrollBottom(true);
        }
        //console.log('allChats',$scope.allChats);
    }

    $scope.sendChatAttachMessage = function(filename, fileUrl){
        if(filename){
            var msg = {
                message: fileUrl,
                room_id: $scope.getRoomId,
                sender_id : $scope.userDetails.id,
                receiver_id: $scope.toUserId,
                type: 2,
                is_read: 0,
                id:'',
                cdate:new Date()
            };
            var msgInsert = {
                message: filename,
                room_id: $scope.getRoomId,
                sender_id : $scope.userDetails.id,
                receiver_id: $scope.toUserId,
                type: 2,
                is_read: 0
            };
            //console.log(msg);
            $scope.message = {userMessage: ''};
            websocket.send(JSON.stringify(msgInsert));
            $scope.allChats.push(msg)
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('small').scrollBottom();
            }, 400);
        }
    }

    $scope.send_typing_stat = function(){
        var msg = {
            typing:true,
            room_id: $scope.getRoomId,
            sender_id : $scope.userDetails.id,
            receiver_id: $scope.toUserId,
            type: 1,
            message: $scope.message.userMessage,
            name:$scope.userDetails.name
        };
        //console.log("keypress  ",msg);
        websocket.send(JSON.stringify(msg));
    }

    $scope.send_stop_typing = function(){
        var msg = {
            stop_typing:true,
            room_id: $scope.getRoomId,
            sender_id : $scope.userDetails.id,
            receiver_id: $scope.toUserId,
            message: $scope.message.userMessage,
            type: 1,
            name:$scope.userDetails.name
        };
        //console.log(msg);
        websocket.send(JSON.stringify(msg));
    }

    $scope.deleteChats = function($event,noti) {
        // $event.stopPropagation();
        // ChatService.deleteChats({receiver_id : noti.Chat.receiver_id,sender_id : noti.Chat.sender_id}).then(function(){
        //     $scope.getNotifications();
        // })
    }


    $scope.toDataUrl = function(path,callback){
        window.resolveLocalFileSystemURL(path, gotFile, fail);
        function fail(e) {
              alert('Cannot found requested file');
        }
        function gotFile(fileEntry) {
              fileEntry.file(function(file) {
                var reader = new FileReader();
                reader.onloadend = function(e) {
                    var content = this.result;
                    callback(content);
                };
                // The most important point, use the readAsDatURL Method from the file plugin
                reader.readAsDataURL(file);
              });
        }
    }

    $scope.openGallery = function(){
      $scope.chatSelOptType = 'gallery';
      navigator.camera.getPicture(function(result){
          if(result)
          {
              $ionicLoading.show({
                  template: 'Uploading image...'
              });
              $scope.toDataUrl(result,function(base64Image){
                  var fd = new FormData();
                  fd.append("base_image", base64Image);
                  $http.post($rootScope.serviceurl+"users/uploadChatFile.json", fd, {
                      headers: {'Content-Type': undefined },
                      transformRequest: angular.identity
                  }).success(function(data){
                      if(data.ack == 1)
                      {
                          //$scope.attachChatFile.push({filename : data.filename, url : data.url});
                          //$scope.message.userMessage = data.filename;
                          $scope.sendChatAttachMessage(data.filename, data.url);
                      }
                      $ionicLoading.hide();
                  })
              });
          }
      },function(err){
          //console.log('error',res);
      },{sourceType:Camera.PictureSourceType.PHOTOLIBRARY,destinationType: Camera.DestinationType.FILE_URI,targetWidth:200,targetHeight:200,allowEdit:true});
    }

    $scope.recordVideo = function () {
        $scope.chatSelOptType = 'video';
        navigator.device.capture.captureVideo(function (mediaFile) {
        if(mediaFile)
        {
            console.log(' success ', mediaFile);
            // $ionicLoading.show({
            //     template: 'Uploading image...'
            // });
            var ft = new FileTransfer(),
            path = mediaFile[0].fullPath,
            name = mediaFile[0].name;
            console.log('path',path);
            if(path!=''){

                ft.upload(path,
                    $rootScope.serviceurl+"users/uploadVideoFile.json",
                    function(result) {
                        console.log('Upload success: ' + result);
                    },
                    function(error) {
                        //console.log('Error uploading file ' + path + ': ' + error.code);
                    },
                    { fileName: name }
                ); 


                // $scope.toDataUrl(path,function(base64Image){
                //     var fd = new FormData();
                //     fd.append("base_image", base64Image);
                //     console.log(fd);
                //     // $http.post($rootScope.serviceurl+"users/uploadChatFile.json", fd, {
                //     //     headers: {'Content-Type': undefined },
                //     //     transformRequest: angular.identity
                //     // }).success(function(data){
                //     //     if(data.ack == 1)
                //     //     {
                //     //         //$scope.attachChatFile.push({filename : data.filename, url : data.url});
                //     //         //$scope.message.userMessage = data.filename;
                //     //         $scope.sendChatAttachMessage(data.filename, data.url);
                //     //     }
                //     //     $ionicLoading.hide();
                //     // })
                // });
            }   
        }
            
        }, function (err) {
            console.log('err ', err)
        },{limit:1});
    }

    $scope.getImageFromCamera = function(){
      $scope.chatSelOptType = 'camera';
      navigator.camera.getPicture(function(result){
          if(result)
          {
              $ionicLoading.show({
                  template: 'Uploading image...'
              });
              console.log('Image Sel',result);
              $scope.toDataUrl(result,function(base64Image){
                  var fd = new FormData();
                  fd.append("base_image", base64Image);
                  $http.post($rootScope.serviceurl+"users/uploadChatFile.json", fd, {
                      headers: {'Content-Type': undefined },
                      transformRequest: angular.identity
                  }).success(function(data){
                      if(data.ack == 1)
                      {
                        //$scope.message.userMessage = data.filename;
                        //$scope.attachChatFile.push({filename : data.filename, url : data.url});
                        $scope.sendChatAttachMessage(data.filename, data.url);
                      }
                      $ionicLoading.hide();
                  })
              });
          }
      },function(err){
          console.log('error',res);
      },{correctOrientation:true,saveToPhotoAlbum: false,targetWidth:200,targetHeight:200,allowEdit:true});
    }
    // $scope.openMessageLoading=function()
    // {
    //     $state.go('message-loading')
    // }
    // $scope.openCamera=function()
    // {
    //     $state.go('message-camera')
    // }

    // $scope.goToMessageImgupload=function()
    // {
    //     $state.go('message-imgupload');
    // }

    $scope.goToBack=function()
    {
        $state.go('notifications-list');
    }

    $scope.goToSearch=function()
    {
        $state.go('explore');
    }

    $scope.makeDeleteChat=function(data, index){
        var delChatId = data.id;
        if(delChatId!=''){
            $scope.allChats.splice(index,1);
            //array.splice(index, 1);
            UserService.deleteChatMsg(delChatId).then(function(data){
                if(data.ack == 1){
                    //$scope.toUserDetData = data.details;
                }
            })
        }else{
            $scope.allChats.splice(index,1);
        }
        //console.log(data);
    }
});
