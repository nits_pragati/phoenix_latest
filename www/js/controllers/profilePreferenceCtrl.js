app.controller('profilePreferenceCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, $ionicLoading, $ionicScrollDelegate, UserService, CommonService, $http) {

    if (!UserService.getUserInfo()) {
        $state.go('login');
    }
    else {
        UserService.getUserDetails(UserService.getUserInfo().id).then(function (data) {
            if (data.ack == 1) {
                $scope.userDetails = data.details;
                $scope.initEditForm(data.details);
            }
        })
    }

    $scope.SelectToggle = function () {
        $scope.IsActive = !$scope.IsActive;
    }
    
   
    $scope.uploadFile = function (files) {
        $ionicLoading.show({
            template: 'Uploading image...'
        });
        $scope.saveImage(files).success(function (data) {
            if (data.ack == 1) {
                $scope.editForm.profile_image = data.filename;
                debugger;
                $scope.editForm.profile_image_url = data.url;
            }
            $ionicLoading.hide();
            console.log(' Uploaded ', data);

        });
    }

    $scope.goToSettings=function()
    {
        $state.go('settings')
    }

    $scope.uploadProfilePic = function (files, index) {
        console.log(files);
        $ionicLoading.show({
            template: 'Uploading image...'
        });
        $scope.saveImage(files).success(function (data) {
            if (data.ack == 1) {
                $scope.editForm.profile_pics.push({ filename: data.filename, url: data.url });
            }
            $ionicLoading.hide();
            console.log(' Uploaded ', data);

        });
    }
    $scope.saveImage = function (files) {
        var fd = new FormData();
        fd.append("photoimg", files[0]);
        return $http.post($rootScope.serviceurl + "users/uploads.json", fd, {
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        })
    }
    $scope.initEditForm = function (details) {
        console.log(details);
        $scope.editForm = {
            profile_image: details.profile_image,
            profile_image_url: details.profile_image ? details.image_url + details.profile_image : '',
            profile_pics: [],
            ethnicity: details.ethnicity ? details.ethnicity : 'Select Ethnicity',
            curlpattern_id: details.curlpattern_id ? details.curlpattern_id : '',
            bio: details.bio ? details.bio : '',
            natural_hair: details.natural_hair,
            hair_extensions: details.hair_extensions,
            braids: details.braids,
            cuts: details.cuts,
            dread_locks: details.dread_locks,
            hair_products: details.hair_products,
            gallery: [],
            texture: details.texture ? details.texture : 'Select texture',
            id: details.id,
            dob: details.dob ? details.dob : '',
            gender: details.gender ? details.gender : '',
            address2: details.address2 ? details.address2 : '',
            address1: details.address1 ? details.address1 : '',
            phone: details.phone ? details.phone : '',
            name: details.name ? details.name : ''
        }

        // if(details.user_photos && details.user_photos.length && details.user_photos.length>0)
        // {
        //     details.user_photos.forEach(element => {
        //         $scope.editForm.profile_pics.push()
        //     });
        // }
    }
    $scope.IsShowText=false;
    $scope.selectTexture=function(value)
    {
        $scope.editForm.texture=value;
         $scope.IsShowText=!$scope.IsShowText
    }

    $scope.toggleTexture=function()
    {
       $scope.IsShowText=!$scope.IsShowText;
    }
    $scope.openDatepicker = function(){
        console.log('hiii')
        datePicker.show({
            date: $scope.editForm.dob?moment($scope.editForm.dob):new Date(),
            mode: 'date'
        }, function(data){
            console.log(data);
            $scope.editForm.dob = moment(data).format('L');
            $scope.$apply();
        }, function(err){
            console.log(' errr ',err);
        });
    }
    $scope.selectEthnicity=function(value)
    {
        $scope.editForm.ethnicity=value;
         $scope.IsShowEth=!$scope.IsShowEth
    }
    $scope.IsShowEth=false;
    $scope.toggleEthnicity=function()
     {
         $scope.IsShowEth=!$scope.IsShowEth;
     }
    $scope.getCurlCategories = function () {
        CommonService.getCurlCategories().then(function (data) {
            if (data) {
                $scope.curlCategories = data.categories;
            }
            console.log(' categories ', data)
        })
    }


    $scope.updateDetails = function () {
        if (!$scope.editForm.name) {
            ionicToast.show("Name is required.");
            return;
        }
       
        if ($scope.editForm.braids) {
            $scope.editForm.braids = 1;
        }
        if ($scope.editForm.dread_locks) {
            $scope.editForm.dread_locks = 1;
        }
        if ($scope.editForm.hair_extensions) {
            $scope.editForm.hair_extensions = 1;
        }
        if ($scope.editForm.hair_products) {
            $scope.editForm.hair_products = 1;
        }
        if ($scope.editForm.natural_hair) {
            $scope.editForm.natural_hair = 1;
        }
        UserService.updateUserDetailsById($scope.editForm).then(function(data){
            if(data.ack == 1)
            {
                $scope.initEditForm($scope.userDetails);
                ionicToast.show(data.message);
                $state.go('listings')
            }
            else
            {
                ionicToast.show(data.message);
            }
        })

    }
});
