app.controller('notificationCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, NotificationService, $ionicLoading, $ionicScrollDelegate, $location, UserService, $timeout,$http) {
    if (!UserService.getUserInfo()) {
        $state.go('login');
    }
    $scope.userDetails = UserService.getUserInfo();
    $scope.getSavePostArr = [];
    $scope.savePostFilePath = '';
    $scope.saveNotiClass = 'notifications';

    $scope.getUseSavePostList = function(){
        NotificationService.getSavePostList($scope.userDetails.id).then(function(data){
            //console.log('notifications ',data)
            if(data.ack == 1)
            {
                $scope.getSavePostArr = data.post_list;
                $scope.savePostFilePath = data.post_files;
            }
        })
    }

    $scope.toggleTab = function(actTab){
        $scope.saveNotiClass = actTab;
        if(actTab == 'saves')
        {
            $timeout(function(){
                $ionicScrollDelegate.freezeAllScrolls(false);
            });
        }
        else
        {
            $timeout(function(){
                $ionicScrollDelegate.freezeAllScrolls(false);
            });
        }
    }
    
    $scope.getNotificationsList = function(){
        $scope.getUseSavePostList();
        NotificationService.getNotificationsList($scope.userDetails.id).then(function(data){
            if(data.ack == 1)
            {
                $scope.notifications = data.details;
                $scope.user_image = data.user_image;
                $scope.post_files = data.post_files;
            }
        })
    }

    $scope.goToDetails = function(notification){
        $scope.readNotification(notification.id);
        if(notification.type == 1)
        {
            //NotificationService.readNotification1(notification.id).then(function(){
                $state.go('listings-details',{post_id : notification.parent_id})
            //})
            
        }
        else if(notification.type == 2)
        {
            //NotificationService.readNotification1(notification.id).then(function(){
            $state.go('member-profile-member-view',{userId : notification.parent_id})
            //})
        }
    }

    $scope.readNotification = function(id){
        NotificationService.readNotification1(id).then(function(){

        })
    }

    $scope.getUserFiles = function(){
        UserService.getFavPostFiles($scope.userDetails.id).then((resultData)=>{
            console.log(resultData);
            $scope.userFiles = []
            
            if(resultData.ack == 1)
            {
                $scope.userFiles = resultData.files;
                $scope.fileUrl = resultData.path;
            }
            // if(userPhotos.ack==1)
            // {
            //     var userDetails=UserService.getUserInfo();
            //     for (let index = 0; index < userPhotos.photos.length; index++) {
            //         userPhotos.photos[index].image=userDetails.image_url+userPhotos.photos[index].name;

            //     }
            //     $scope.userPhotos=userPhotos.photos;
            //     //console.log('$scope.userPhotos',$scope.userPhotos);
            // }
        })
    }
    $scope.newFolder = {};
    $scope.addFolder = function(){
        $ionicPopup.show({
            title: 'Folder Name',
            template: '<input type="text" ng-model="newFolder.name"/>', // the preset value show 0, which is expected.
            scope: $scope,
            buttons: [{ text: 'Cancel' },{
                text: '<b>Confirm</b>',
                type: 'button-positive',
                onTap: function(e) {
                    console.log($scope.newFolder); // 0
                    $scope.newFolder.user_id = $scope.userDetails.id;
                    NotificationService.addNewFolder($scope.newFolder).then(function(){
                        $scope.getFolderList();
                        $scope.newFolder = {};
                    })
                    return $scope.newFolder;
                }
            }]
        });
    }
    $scope.folderList = [];
    $scope.getFolderList = function(){
        NotificationService.readNotification($scope.userDetails.id).then(function(data){
            if(data.ack == 1)
            {
                $scope.folderList = data.list
                $scope.fileUrl = data.path;
            }
        })
    }

    $scope.onDropComplete = function(file,e,folder){
        console.log(file)
        var data = {
            file_id:file.id,
            folder_id:folder.id,
            user_id : $scope.userDetails.id
        }

        NotificationService.saveToFolder(data).then(function(res){
            console.log(res)
            $scope.getUserFiles();
            $scope.getFolderList();
        })
    }
    
    $scope.getFolderFiles = function(id){
        NotificationService.getFolderFiles(id).then(function(files){
            console.log(files)
        })
    }
    
})