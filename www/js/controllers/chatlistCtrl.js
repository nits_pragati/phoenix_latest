app.controller('chatlistCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, $ionicLoading, $ionicScrollDelegate, $location, UserService) {
    var value;
    var websocket;
    $scope.allChatUsers = [];
    $scope.UserImage_url = '';
    $scope.userDetails=UserService.getUserInfo();
    if(!$scope.userDetails.id){
        $state.go('login');
    }
    if($scope.userDetails &&  $scope.userDetails.profile_image)
    {
        $scope.profile_image_url=$scope.userDetails.image_url+$scope.userDetails.profile_image;
    }

    $scope.getChatsList=function(){
        UserService.getmyChatList({user_id:$scope.userDetails.id}).then(function(data){
            if(data.ack == 1){

                $scope.allChatUsers = data.chat_details.chat_list;
                $scope.UserImage_url = data.chat_details.image_url;
            }
            //console.log($scope.allChatUsers);
        })
    }
    //console.log(new Date());

    $scope.goToProfile=function(userDetails)
    {
        $state.go('member-profile-member-view', {userId:userDetails.id})
    }

    $scope.goToMessage=function(room_id)
    {
        $state.go('message-chatbox',{roomId:room_id});
    }

    $scope.goToBack=function()
    {
        $state.go('listings');
    }

    $scope.goToSearch=function()
    {
        $state.go('explore');
    }
});
