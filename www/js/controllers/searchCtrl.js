app.controller('searchCtrl', function ($scope, $rootScope, $state, $ionicPopup, ionicToast, $stateParams, $ionicSlideBoxDelegate, $ionicLoading, $ionicScrollDelegate,$location, UserService,PostService) {
    $scope.userDetails = []

    $rootScope.selectedServiceLoc = 'Select a Location'
    $scope.menuToggleForService=function(type)
    {


       $rootScope.selectedServiceLoc = type;
       $state.go('find-service');

    }
    $scope.selectedService = "Select a Service";
    $scope.SelectToggle=function()
    {
        $scope.IsActive=!$scope.IsActive;
    }

    $scope.goToLoc=function()
    {
        $state.go('location',{service_type:$scope.type,city_id:$scope.city_id,lat:$stateParams.lat,lng:$stateParams.lng})
    }

    $scope.goToServiceArea=function()
    {
        $state.go('service-area')
    }

    $scope.getUserDetails = function(){        
        UserService.getUserDetailsById($stateParams.user_id,'').then(function(data){
            if(data.ack==1)
            {
                $scope.userDetails = data.details;
                $scope.userImageUrl = data.details.image_url;
            }
            else
            {
                //ionicToast('Invalid user');
                $state.go('location');
            }

        })
    }
    $scope.getPostListImage = function(){        
        PostService.getPostListImage($stateParams.user_id).then(function(data){
            if(data.ack==1)
            {
                //console.log(data.list.post_files);
                $scope.photos =data.list['0'].post_files;
                $scope.photourl = data.file_url;
            }
            else
            {
                //ionicToast('Invalid user');
                $state.go('location');
            }

        })
    }
    navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        //console.log(pos);
        $scope.map.setCenter(pos);
        $ionicLoading.hide();
      });

    $scope.SelectToggleForLoc=function(type) {
        $scope.type=type;
        switch (type) {
            case '2':
            $scope.selectedService = 'Stylist | Salons';
            $scope.IsActive = !$scope.IsActive;
            break;
            case '3':
            $scope.selectedService = 'Barber | Barbershop';
            $scope.IsActive = !$scope.IsActive;
            break;
            case '1':
            $scope.selectedService = 'Salon';
            $scope.IsActive = !$scope.IsActive;
            break;
            case '4':
            $scope.selectedService = 'Beauty Supply';
            $scope.IsActive = !$scope.IsActive;
            break;
            case '5':
            $scope.selectedService = 'Barbershop';
            $scope.IsActive = !$scope.IsActive;
            break;
            case 'all':
            $scope.selectedService = 'Show All';
            $scope.IsActive = !$scope.IsActive;
            break;
            default:
            break;
        }
    }


    $scope.city_id = $stateParams.city_id;
    $scope.getCityDetails = function(){
        if($scope.city_id)
        {
            //console.log('hii');
            PostService.getCityDetails($scope.city_id).then(function(data){
                if(data.ack==1)
                {
                    $scope.cityDetails = data.details;
                    $scope.selectedServiceLoc = data.details.name;
                }
            })
        }        
    }
    $scope.getCountryList = function(){
        PostService.getCountryList().then(function(data){
            if(data.ack == 1)
            {
                $scope.countryList = data.details;
                $scope.city_image = data.city_image;
            }
        })
    }
})