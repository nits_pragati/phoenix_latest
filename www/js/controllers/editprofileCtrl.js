app.controller('editprofileCtrl', function($scope,$state, $rootScope,CommonService,UserService,$ionicSlideBoxDelegate,$ionicLoading, $ionicPopup,ionicToast,$http){

  $scope.userDetData='';
  $scope.getUserId= '';
  $scope.signupForm = {
      id : '',
      name : '',
      profile_image : '',
      profile_image_url : '',
      profile_pics : [],
      address1:'',
      address2:'',
      phone:'',
      gender:'',
      dob:'',
      ethnicity:'Select Ethnicity',
      curlpattern_id :'',
      bio:'',
      texture:'Select Texture',
      user_provider_type:2,
  }
  $scope.IsShowEth=false;
  $scope.IsShowText=false;

  $scope.userDetails=UserService.getUserInfo();

  $scope.getUserDetails = function(){
    $scope.getUserId = $scope.userDetails.id;
    UserService.getUserDetailsData($scope.getUserId,'').then(function(data){
      //$scope.currentUser=UserService.getUserInfo();
        if(data.ack == 1)
        {
            $scope.userDetData = data.details;
            $scope.signupForm.id = $scope.getUserId;
            $scope.signupForm.name = data.details.name;
            $scope.signupForm.address1 = data.details.address1;
            $scope.signupForm.address2 = data.details.address2;
            $scope.signupForm.phone = data.details.phone;
            $scope.signupForm.gender = data.details.gender;
            $scope.signupForm.dob = moment(data.details.dob).format('L');
            $scope.signupForm.bio = data.details.bio;
            $scope.signupForm.profile_image = data.details.profile_image;
            $scope.signupForm.user_type = data.details.user_type;
            $scope.signupForm.user_provider_type = data.details.user_provider_type;
            $scope.signupForm.details_saved = data.details.details_saved;
            
            if(data.details.texture!=''){
              $scope.signupForm.texture = data.details.texture;
            }
            if(data.details.ethnicity!=''){
              $scope.signupForm.ethnicity = data.details.ethnicity;
            }

            $scope.signupForm.curlpattern_id = data.details.curlpattern_id;
            if($scope.userDetData.curlpattern_id)
            {
                CommonService.getCurlDetailsById($scope.userDetData.curlpattern_id).then((curl)=>{
                    if(curl.ack==1)
                    {
                        $scope.selectedCurlPatten=curl.details;
                    }
                })
            }
            if($scope.userDetData &&  $scope.userDetData.profile_image)
            {
                $scope.userDetData.profile_image=$scope.userDetData.image_url+$scope.userDetails.profile_image;
            }
        }
       console.log($scope.userDetData)
    })
  }
  $scope.getCurlCategories = function(){
      CommonService.getCurlCategories().then(function(data){
          if(data)
          {
              $scope.curlCategories = data.categories;
          }
          //console.log(' categories ',data)
      })
  }

  if($scope.userDetails &&  $scope.userDetails.profile_image)
  {
    $scope.getUserId = $scope.userDetails.id;
    if($scope.getUserId!=''){
      $scope.getUserDetails();
      $scope.getCurlCategories();
    }
  }

  $scope.uploadFile = function(files) {
      $ionicLoading.show({
          template: 'Uploading image...'
      });
      $scope.saveImage(files).success(function(data){
          if(data.ack == 1)
          {
              $scope.signupForm.profile_image = data.filename;
              $scope.signupForm.profile_image_url = data.url;
          }
          $ionicLoading.hide();
          //console.log(' Uploaded ',data);

      });
  }

  $scope.saveImage = function(files){
    var fd = new FormData();
    fd.append("photoimg", files[0]);
    return $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
        headers: {'Content-Type': undefined },
        transformRequest: angular.identity
    })
  }

  $scope.selectUserProfileMainPic = function(){
    $scope.upPopup = $ionicPopup.show({
        scope: $scope,
        template : '<button class="next-btn" ng-click="getImageFromCameraMain()">Camera</button>' +
                    '<button class="next-btn" style="float:right" ng-click="openGalleryMain()">Gallery</button>',
        buttons : [
            {text:"Cancel",type:"next-btn "}
        ]

    });
//document.querySelector('.sidbar_upload').click();
}

$scope.getImageFromCameraMain = function(){
    $scope.upPopup.close();
    navigator.camera.getPicture(function(result){
        if(result)
        {
            $ionicLoading.show({
                template: 'Uploading image...'
            });
            $scope.toDataUrl(result,function(base64Image){
                var fd = new FormData();
                fd.append("photoimg", base64Image);
                $http.post($rootScope.serviceurl+"users/uploadProfileImg.json", fd, {
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function(data){
                    if(data.ack == 1)
                    {
                        $scope.signupForm.profile_image = data.filename;
                        $scope.signupForm.profile_image_url = data.url;
                    }
                    $ionicLoading.hide();
                })
            });
        }
    },function(err){
        console.log('error',res);
    },{quality:100, correctOrientation:true,saveToPhotoAlbum: false,allowEdit:true});
}

$scope.openGalleryMain = function(){
    $scope.upPopup.close();
    navigator.camera.getPicture(function(result){
        if(result)
        {
            $ionicLoading.show({
                template: 'Uploading image...'
            });
            $scope.toDataUrl(result,function(base64Image){
                var fd = new FormData();
                fd.append("photoimg", base64Image);
                $http.post($rootScope.serviceurl+"users/uploadProfileImg.json", fd, {
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function(data){
                    if(data.ack == 1)
                    {
                        $scope.signupForm.profile_image = data.filename;
                        $scope.signupForm.profile_image_url = data.url;
                    }
                    $ionicLoading.hide();
                })
            });
        }
    },function(err){
        console.log('error',res);
    },{quality:100, sourceType:Camera.PictureSourceType.PHOTOLIBRARY,destinationType: Camera.DestinationType.FILE_URI,targetWidth:200,targetHeight:200,allowEdit:true});
}


  $scope.selectProfilePic = function(i){
      $scope.upPopup = $ionicPopup.show({
        scope: $scope,
        template : '<button class="next-btn" ng-click="getImageFromCamera('+i+')">Camera</button>' +
                    '<button class="next-btn" style="float:right" ng-click="openGallery('+i+')">Gallery</button>',
        buttons : [
            {text:"Cancel",type:"next-btn "}
        ]
    });
  }

  $scope.getImageFromCamera = function(index){
    $scope.upPopup.close();
    navigator.camera.getPicture(function(result){
        if(result)
        {
            $ionicLoading.show({
                template: 'Uploading image...'
            });
            $scope.toDataUrl(result,function(base64Image){
                var fd = new FormData();
                fd.append("base_image", base64Image);
                $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function(data){
                    if(data.ack == 1)
                    {
                        $scope.signupForm.profile_pics[index] = {filename : data.filename, url : data.url};
                        //$scope.signupForm.profile_pics.push({filename : data.filename, url : data.url});
                    }
                    $ionicLoading.hide();
                })
            });
        }
    },function(err){
        console.log('error',res);
    },{quality:100, correctOrientation:true,saveToPhotoAlbum: false,allowEdit:true});
  }

  $scope.toDataUrl = function(path,callback){
      window.resolveLocalFileSystemURL(path, gotFile, fail);

      function fail(e) {
            alert('Cannot found requested file');
      }

      function gotFile(fileEntry) {
              console.log('fileEntry ', fileEntry)
            fileEntry.file(function(file) {
              var reader = new FileReader();
              reader.onloadend = function(e) {
                  var content = this.result;
                  callback(content);
              };
              // The most important point, use the readAsDatURL Method from the file plugin
              reader.readAsDataURL(file);
            });
      }
  }

  $scope.openGallery = function(index){
    $scope.upPopup.close();
    navigator.camera.getPicture(function(result){
        if(result)
        {
            $ionicLoading.show({
                template: 'Uploading image...'
            });
            $scope.toDataUrl(result,function(base64Image){
                var fd = new FormData();
                fd.append("base_image", base64Image);
                $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function(data){
                    if(data.ack == 1)
                    {
                        $scope.signupForm.profile_pics[index] = {filename : data.filename, url : data.url};
                        //$scope.signupForm.profile_pics.push({filename : data.filename, url : data.url});
                    }
                    $ionicLoading.hide();
                })
            });
        }
    },function(err){
        //console.log('error',res);
    },{sourceType:Camera.PictureSourceType.PHOTOLIBRARY,destinationType: Camera.DestinationType.FILE_URI,quality:100,allowEdit:true});
  }

  $scope.onChangeAdvance=function(event){
    $scope.AdvancedOption=!$scope.AdvancedOption;
  }

  $scope.openDatepicker = function(){
      datePicker.show({
          date: $scope.signupForm.dob?moment($scope.signupForm.dob):new Date(),
          mode: 'date'
      }, function(data){
          $scope.signupForm.dob = moment(data).format('L');
          $scope.$apply();
      }, function(err){
          //console.log(' errr ',err);
      });
  }

  $scope.selectEthnicity=function(value){
      $scope.signupForm.ethnicity=value;
      $scope.IsShowEth=!$scope.IsShowEth
  }

  $scope.selectTexture=function(value){
      $scope.signupForm.texture=value;
      $scope.IsShowText=!$scope.IsShowText
  }
  $scope.toggleEthnicity=function(){
    $scope.IsShowEth=!$scope.IsShowEth;

  }

  $scope.toggleTexture=function(){
    $scope.IsShowText=!$scope.IsShowText;
  }

  $scope.SelectToggle=function(){
    $scope.IsActive=!$scope.IsActive;
  }

  $scope.saveUserDetails = function(){
    //console.log($scope.signupForm);
    
    $scope.signupForm.id = $scope.userDetails.id;
    
    if(!$scope.signupForm.curlpattern_id)
    {
        ionicToast.show('Please select your root curl.');
    }
    else
    {
        UserService.userProfileEdit($scope.signupForm).then(function(data){
            if(data.details.ack == 1){
                UserService.setUserInfo(data.details.details);
                // $state.go('listings');
                $state.go('member-profile-member-view', {userId:data.details.details.id})
                ionicToast.show(data.details.message);
            }else{
                ionicToast.show(data.details.message);
            }
        })
    }
  }
})
