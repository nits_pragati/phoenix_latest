app.controller('signupCtrl', function($scope, $state, $cordovaGeolocation, CommonService, $ionicSlideBoxDelegate, $ionicPopup,UserService,ionicToast,$rootScope,$ionicLoading, $stateParams, $http,$q, $ionicScrollDelegate,$timeout) {
    if(UserService.getUserInfo())
    {
        //$state.go('listings');
        var userDetails=UserService.getUserInfo();
        $state.go('member-profile-member-view', {userId:userDetails.id})
    }
    $scope.AdvancedOption = false;
    $scope.signupGeoLoc = 1;
    $scope.selectedProvider = 1;
    $scope.userType = $stateParams.user_type;
    $scope.provider_type = $stateParams.provider_type;
    $scope.initSignupForm = function(){
        $scope.signupForm = {
            username : '',
            password : '',
            confirm_password : '',
            userlat:'',
            userlong:'',
            name : '',
            profile_image : '',
            profile_image_url : '',
            email : '',
            user_type : $scope.userType,
            user_provider_type : $scope.provider_type,
            profile_pics : [],
            address1:'',
            address2:'',
            phone:'',
            gender:'',
            dob:'',
            ethnicity:'Select Ethnicity',
            curlpattern_id :'',
            bio:'',
            natural_hair:0,
            hair_extensions:0,
            braids:0,
            cuts:0,
            dread_locks:0,
            hair_products:0,
            gallery:[],
            device_token:$rootScope.uuid,
            device_type:'ios',
            texture:'Select Texture'
        }
    }
    $scope.getCurrentLocation = function(){
        var posOptions = {enableHighAccuracy: true};
        navigator.geolocation.getCurrentPosition(function (position) {
            //console.log('Get Current Position',position);
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            $scope.signupForm.userlat = lat;
            $scope.signupForm.userlong = long;
            //console.log(lat + '   ' + long)
        }, function(err) {
            //console.log('gps error ',err)
        },posOptions);
    }
    


    $scope.IsShowEth=false;
    $scope.IsShowText=false;
    $scope.Forcetofalse = function(value){
        console.log($scope.AdvancedOption);
        $scope.AdvancedOption = value;
        console.log($scope.AdvancedOption);
    }

    $scope.onChangeAdvance=function(event)
    {
        $scope.AdvancedOption=!$scope.AdvancedOption;
    }
    $scope.selectEthnicity=function(value)
    {
        $scope.signupForm.ethnicity=value;
         $scope.IsShowEth=!$scope.IsShowEth
    }
    $scope.selectTexture=function(value)
    {
        $scope.signupForm.texture=value;
        $scope.IsShowText=!$scope.IsShowText
    }
     $scope.toggleEthnicity=function()
     {
        $scope.IsShowEth=!$scope.IsShowEth;
     }

     $scope.toggleTexture=function()
     {
        $scope.IsShowText=!$scope.IsShowText;
     }

    $scope.initLoginForm = function(){
        $scope.loginForm = {
            username : '',
            deviceid:$rootScope.uuid,
            devicetype:'ios',
            password : ''
        }
    }

    $scope.getCurlCategories = function(){
        CommonService.getCurlCategories().then(function(data){
            if(data)
            {
                $scope.curlCategories = data.categories;
            }
            //console.log(' categories ',data)
        })
    }

    $scope.initForgotForm = function(){
        $scope.forgotForm = {
            email : ''
        }
    }

    $scope.submitForgot = function(){
        $ionicLoading.show({
            template: 'Sending...'
        });
        //console.log('Forgot Form Submitted',$scope.forgotForm)
        UserService.forgotPassword($scope.forgotForm).then(function(data){
            if(data.ack == 1)
            {
                $scope.initForgotForm();
            }
            $ionicLoading.hide();
            ionicToast.show(data.message);
        })
    }

    $scope.submitLogin = function(){
        UserService.userlogin($scope.loginForm).then(function(data){
            if(data.ack==1)
            {
                UserService.setUserInfo(data.details);
               // $state.go('listings');
               $state.go('member-profile-member-view', {userId:data.details.id})
            }
            else
            {
                ionicToast.show(data.message);
            }
        })
    }

    $scope.disableSpace = function(e){
        //console.log(e.which)
        if(e.which == 32)
        {
            e.preventDefault();
        }
    }

    $scope.submitSignup = function(){
        //console.log($scope.signupForm);
        if($scope.signupForm.password && $scope.signupForm.password != $scope.signupForm.confirm_password)
        {
            ionicToast.show('Please enter same password.');
        } else if(!$scope.signupForm.profile_image){
            ionicToast.show('Please choose your icon photo.');
        }
        else if(!$scope.signupForm.curlpattern_id)
        {
            ionicToast.show('Please select your root curl.');
        } else if (!$scope.signupForm.profile_pics.length){
            ionicToast.show('Please select at least one profile photo.');
        } else if(!$scope.signupForm.profile_image)
        {
            ionicToast.show('Please select your profile pic.')
        }
        else if(!$scope.signupForm.profile_pics || $scope.signupForm.profile_pics.length<=0)
        {
            ionicToast.show('Please select your Profile Photos')
        }
        else
        {
            if($scope.signupGeoLoc != 1){
                $scope.signupForm.userlat = '';
                $scope.signupForm.userlong = '';
            }
           if($scope.signupForm.user_type=="P")
           {
               if($scope.signupForm.braids)
               {
                $scope.signupForm.braids=1;
               }
               if($scope.signupForm.dread_locks)
               {
                $scope.signupForm.dread_locks=1;
               }
               if($scope.signupForm.hair_extensions)
               {
                $scope.signupForm.hair_extensions=1;
               }
               if($scope.signupForm.hair_products)
               {
                $scope.signupForm.hair_products=1;
               }
               if($scope.signupForm.natural_hair)
               {
                $scope.signupForm.natural_hair=1;
               }
           }
            UserService.register($scope.signupForm).then(function(data){
                //console.log(' ----- ',data)
                //console.log(' ++++++ ',data.details)
                if(data.details.ack == 1)
                {
                    $scope.initSignupForm();
                    // $state.go('login')
                    UserService.setUserInfo(data.details.details);
                    // $state.go('listings');
                    $state.go('member-profile-member-view', {userId:data.details.details.id})
                    ionicToast.show(data.details.message);
                }
                else
                {
                    ionicToast.show(data.details.message);
                }
            })
        }
    }

    $scope.toDataUrl = function(path,callback){
        window.resolveLocalFileSystemURL(path, gotFile, fail);

        function fail(e) {
              alert('Cannot found requested file');
        }

        function gotFile(fileEntry) {
            //console.log('fileEntry ', fileEntry)
            fileEntry.file(function(file) {
            var reader = new FileReader();
            reader.onloadend = function(e) {
                    var content = this.result;
                    callback(content);
            };
            // The most important point, use the readAsDatURL Method from the file plugin
            reader.readAsDataURL(file);
            });
        }
    }
    $scope.selectProfilePic = function(i){
        $scope.upPopup = $ionicPopup.show({
           scope: $scope,
           template : '<button class="next-btn" ng-click="getImageFromCamera('+i+')">Camera</button>' +
                      '<button class="next-btn" style="float:right" ng-click="openGallery('+i+')">Gallery</button>',
           buttons : [
               {text:"Cancel",type:"next-btn "}
           ]
       });
       //document.querySelector('.sidbar_upload').click();
    }
   
   $scope.selectUserProfileMainPic = function(){
        $scope.upPopup = $ionicPopup.show({
            scope: $scope,
            template : '<button class="next-btn" ng-click="getImageFromCameraMain()">Camera</button>' +
                        '<button class="next-btn" style="float:right" ng-click="openGalleryMain()">Gallery</button>',
            buttons : [
                {text:"Cancel",type:"next-btn "}
            ]

        });
    //document.querySelector('.sidbar_upload').click();
    }

    $scope.getImageFromCameraMain = function(){
        $scope.upPopup.close();
        navigator.camera.getPicture(function(result){
            if(result)
            {
                $ionicLoading.show({
                    template: 'Uploading image...'
                });
                $scope.toDataUrl(result,function(base64Image){
                    var fd = new FormData();
                    fd.append("photoimg", base64Image);
                    $http.post($rootScope.serviceurl+"users/uploadProfileImg.json", fd, {
                        headers: {'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).success(function(data){
                        if(data.ack == 1)
                        {
                            $scope.signupForm.profile_image = data.filename;
                            $scope.signupForm.profile_image_url = data.url;
                        }
                        $ionicLoading.hide();
                    })
                });
            }
        },function(err){
            console.log('error',res);
        },{quality:100, correctOrientation:true,saveToPhotoAlbum: false,allowEdit:true});
    }
    
    $scope.openGalleryMain = function(){
        $scope.upPopup.close();
        navigator.camera.getPicture(function(result){
            if(result)
            {
                $ionicLoading.show({
                    template: 'Uploading image...'
                });
                $scope.toDataUrl(result,function(base64Image){
                    var fd = new FormData();
                    fd.append("photoimg", base64Image);
                    $http.post($rootScope.serviceurl+"users/uploadProfileImg.json", fd, {
                        headers: {'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).success(function(data){
                        if(data.ack == 1)
                        {
                            $scope.signupForm.profile_image = data.filename;
                            $scope.signupForm.profile_image_url = data.url;
                        }
                        $ionicLoading.hide();
                    })
                });
            }
        },function(err){
            console.log('error',res);
        },{quality:100, sourceType:Camera.PictureSourceType.PHOTOLIBRARY,destinationType: Camera.DestinationType.FILE_URI,targetWidth:200,targetHeight:200,allowEdit:true});
    }

   $scope.getImageFromCamera = function(index){
    $scope.upPopup.close();
    navigator.camera.getPicture(function(result){
        if(result)
        {
            $ionicLoading.show({
                template: 'Uploading image...'
            });
            $scope.toDataUrl(result,function(base64Image){
                var fd = new FormData();
                fd.append("base_image", base64Image);
                $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function(data){
                    if(data.ack == 1)
                    {
                        //$scope.signupForm.profile_pics.push({filename : data.filename, url : data.url});
                        $scope.signupForm.profile_pics[index] = {filename : data.filename, url : data.url};
                    }
                    $ionicLoading.hide();
                })
            });
        }
    },function(err){
        console.log('error',res);
    },{quality:100, correctOrientation:true,saveToPhotoAlbum: false,allowEdit:true});
   }

   $scope.openGallery = function(index){
    $scope.upPopup.close();
    navigator.camera.getPicture(function(result){
        if(result)
        {
            $ionicLoading.show({
                template: 'Uploading image...'
            });
            $scope.toDataUrl(result,function(base64Image){
                var fd = new FormData();
                fd.append("base_image", base64Image);
                $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
                    headers: {'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function(data){
                    if(data.ack == 1)
                    {
                        //$scope.signupForm.profile_pics.push({filename : data.filename, url : data.url});
                        $scope.signupForm.profile_pics[index] = {filename : data.filename, url : data.url};
                    }
                    $ionicLoading.hide();
                })
            });
        }
    },function(err){
        console.log('error',res);
    },{quality:100, sourceType:Camera.PictureSourceType.PHOTOLIBRARY,destinationType: Camera.DestinationType.FILE_URI,allowEdit:true});
   }



    $scope.selectGalleryPic = function(i){
        navigator.camera.getPicture(function(result){
            if(result)
            {
                $ionicLoading.show({
                    template: 'Uploading image...'
                });
                $scope.toDataUrl(result,function(base64Image){
                    var fd = new FormData();
                    fd.append("base_image", base64Image);
                    $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
                        headers: {'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).success(function(data){
                        if(data.ack == 1)
                        {
                            //$scope.signupForm.gallery.push({filename : data.filename, url : data.url});
                            $scope.signupForm.gallery[i] = {filename : data.filename, url : data.url};
                            //$scope.signupForm.profile_pics.push({filename : data.filename, url : data.url});
                        }
                        $ionicLoading.hide();
                    })
                });
            }
        },function(err){
            console.log('error',res);
        },{quality:100, sourceType:Camera.PictureSourceType.PHOTOLIBRARY,destinationType: Camera.DestinationType.FILE_URI,allowEdit:true});

        /*window.imagePicker.getPictures(
            function(results) {
                if(results)
                {
                    $ionicLoading.show({
                        template: 'Uploading image...'
                    });
                    $scope.toDataUrl(results['0'],function(base64Image){
                        console.log(' ----- ',base64Image)
                        var fd = new FormData();
                        fd.append("base_image", base64Image);
                        $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
                            headers: {'Content-Type': undefined },
                            transformRequest: angular.identity
                        }).success(function(data){
                            if(data.ack == 1)
                            {
                                $scope.signupForm.gallery.push({filename : data.filename, url : data.url});
                            }
                            $ionicLoading.hide();
                        })
                    });
                    console.log('after encode ', $scope.imageArray)
                }
            }, function (error) {
                console.log('Error: ' + error);
            },{
                maximumImagesCount: 1
            }
        );*/
    }
    
    $scope.uploadProfilePic = function(files,index)
    {
        console.log(files);
        $ionicLoading.show({
            template: 'Uploading image...'
        });
        $scope.saveImage(files).success(function(data){
            if(data.ack == 1)
            {
                $scope.signupForm.profile_pics.push({filename : data.filename, url : data.url});
            }
            $ionicLoading.hide();
            console.log(' Uploaded ',data);

        });
    }
    $scope.uploadProGalleryPic = function(files,index)
    {
        //console.log(files);
        $ionicLoading.show({
            template: 'Uploading image...'
        });
        $scope.saveImage(files).success(function(data){
            if(data.ack == 1)
            {
                $scope.signupForm.gallery.push({filename : data.filename, url : data.url});
            }
            $ionicLoading.hide();
            //console.log(' Uploaded ',data);
        });
    }

    $scope.uploadFile = function(files) {
        $ionicLoading.show({
            template: 'Uploading image...'
        });
        $scope.saveImage(files).success(function(data){
            if(data.ack == 1)
            {
                $scope.signupForm.profile_image = data.filename;
                $scope.signupForm.profile_image_url = data.url;
            }
            $ionicLoading.hide();
            //console.log(' Uploaded ',data);
        });
    }

    $scope.saveImage = function(files){
      var fd = new FormData();
      fd.append("photoimg", files[0]);
      return $http.post($rootScope.serviceurl+"users/uploads.json", fd, {
          headers: {'Content-Type': undefined },
          transformRequest: angular.identity
      })
    }

    $scope.facebookLogin = function(){

        facebookConnectPlugin.login(["public_profile", "email"], $scope.facebookSuccess, $scope.facebookFailure)
    }

    $scope.facebookSuccess = function(data){
        $ionicLoading.show({
            template: 'Logging in...'
          });
        getFacebookProfileInfo(data).then(function(data){
            if(data.email)
            {
                UserService.socialLogin(data).then(function(socialresult){
                    if(socialresult.ack==1)
                    {
                        UserService.setUserInfo(socialresult.details);
                        if(socialresult.esists)
                        {
                            $state.go('member-profile-member-view', {userId:socialresult.details.id})
                        }
                        else
                        {
                            ionicToast.show('Please add your profile details')
                            $state.go('member-profile-preferences')
                        }
                    }
                    else
                    {
                        ionicToast.show(socialresult.message);
                    }
                    $ionicLoading.hide();
                })
            }
            else
            {
                ionicToast.show('Could not found email. Please use normal login');
                $ionicLoading.hide();
            }
        })
    }

    $scope.facebookFailure = function(data)
    {
        console.log(data)
    }

    var getFacebookProfileInfo = function (authResponse) {
        var info = $q.defer();

         facebookConnectPlugin.api('/me?fields=email,first_name,last_name,gender,picture&access_token=' + authResponse.accessToken, ["public_profile"],
             function (response) {
                 //console.log(response);
                 info.resolve(response);
             },
             function (response) {
                 //console.log(response);
                 info.reject(response);
             }
         );
         return info.promise;
     };

     $scope.googleSignIn = function() {
        $ionicLoading.show({
          template: 'Logging in...'
        });

        window.plugins.googleplus.login(
          {},
          function (user_data) {
            // For the purpose of this example I will store user data on local storage
            /*UserService.setUser({
              userID: user_data.userId,
              name: user_data.displayName,
              email: user_data.email,
              picture: user_data.imageUrl,
              accessToken: user_data.accessToken,
              idToken: user_data.idToken
            });*/
            var udata = {
                first_name : user_data.displayName,
                google_id : user_data.userId,
                email : user_data.email
            };
            UserService.socialLogin(udata).then(function(socialresult){
                if(socialresult.ack==1)
                {
                    UserService.setUserInfo(socialresult.details);
                    if(socialresult.esists)
                    {
                        $state.go('member-profile-member-view', {userId:socialresult.details.id})
                    }
                    else
                    {
                        ionicToast.show('Please add your profile details')
                        $state.go('member-profile-preferences')
                    }                    
                }
                else
                {
                    ionicToast.show(socialresult.message);
                }
                $ionicLoading.hide();
            })
            console.log('user_data ',user_data)


          },
          function (msg) {
            $ionicLoading.hide();
          }
        );
      };


    $scope.selectProvider = function(i)
    {
        $scope.selectedProvider = i;
    }
    $scope.goToMemberForm=function()
    {
        $state.go('signup-form',{ user_type : 'M' });
    }
    $scope.goToProvider=function($i)
    {
        $ionicSlideBoxDelegate.slide($i);
    }

    $scope.goToProviderForm=function()
    {

        $state.go('signup-form',{ user_type : 'P', provider_type : $scope.selectedProvider });
    }

    $scope.SelectToggle=function() 
    {
        $scope.IsActive=!$scope.IsActive;
        $timeout(function(){
            $ionicScrollDelegate.resize();
        })        
    }

    $scope.openDatepicker = function(){
        datePicker.show({
            date: $scope.signupForm.dob?moment($scope.signupForm.dob):new Date(),
            mode: 'date'
        }, function(data){
            //console.log(data);
            $scope.signupForm.dob = moment(data).format('L');
            $scope.$apply();
        }, function(err){
            console.log(' errr ',err);
        });
    }

    $scope.checkUsername = function(){
        if($scope.signupForm.username)
        {
            UserService.checkUsername({username:$scope.signupForm.username}).then(function(res){
                if(res.ack == 1)
                {
                    ionicToast.show('Username already exist. Please try another.');
                }
            })
        }
    }

    $scope.checkEmail = function(){
        if($scope.signupForm.email)
        {
            UserService.checkUsername({email:$scope.signupForm.email}).then(function(res){
                if(res.ack == 1)
                {
                    ionicToast.show('Email already exist. Please try another.');
                }
            })
        }
    }
})
