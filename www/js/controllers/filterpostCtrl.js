app.controller('filterpostCtrl', function ($scope,$sce, $state, $ionicSlideBoxDelegate, $ionicPopup, ionicToast, $rootScope, $ionicLoading, $stateParams, UserService, $http, $timeout, PostService, CommonService) {
    if (!UserService.getUserInfo()) {
        $state.go('login');
    }

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
      }

    $scope.userDetails = UserService.getUserInfo();
    if ($scope.userDetails && $scope.userDetails.profile_image) {
        $scope.userDetails.profile_image = $scope.userDetails.image_url + $scope.userDetails.profile_image;
    }
    $scope.imageArray = [];
    //$scope.userDetails = UserService.getUserInfo();
    $scope.IsShowText=false;
    $scope.SelectToggle = function () {
        $scope.IsActive = !$scope.IsActive;
    }
    
    $scope.goToMyPostPage = function (post) {
        $state.go('member-profile-member-view', { userId: post.user.id })
    }
    
    $scope.goToPreference = function () {
        $state.go('member-profile-member-view', {userId:$scope.userDetails.id})
    }
    
    $scope.goToNext = function () {
        // PostService.replaceOldFile(base64Image,$scope.postDetails.post_files[$scope.editedIndex].name).then(function(){
        //     $scope.getPostDetails()
        // })
        $state.go('post-settings', { post_id: $scope.postDetails.id });
    }

    $scope.postDetails = {};
    
    
    $scope.createStringByArray = function(array) {
        var output = '';
        angular.forEach(array, function (object) {
            angular.forEach(object, function (value, key) {
                output += key + ',';
                output += value + ',';
            });
        });
        return output;
    }


    $scope.getPostListings = function () {
        $ionicLoading.show({
            template: 'Loading...'
        });
        var skeyword =$stateParams.skeyword;
        var spost_type =$stateParams.post_type;
        var tags =$stateParams.tags;
        var curl =$stateParams.curl;
        if(!skeyword){
            skeyword = '';
        }
        if(!spost_type){
            spost_type = '';
        }
        if(!tags){
            tags = '';
        }
        if(!curl){
            curl = '';
        }
        //console.log($stateParams.post_type);
        PostService.getPostListnew($scope.userDetails.id,skeyword,spost_type,tags,curl).then(function (data) {
            //console.log('Testing Filter Post Data',data);
            if (data.ack == 1) {
                $scope.userImageUrl = data.user_img;
                $scope.postImageUrl = data.file_url;
                $scope.postListings = data.list;
                //console.log($scope.postListings);
            }
            $ionicLoading.hide();
        })
    }

    $scope.saveComment = function (data) {
        var cmntData = { comment: data.comment, user_id: $scope.userDetails.id, post_id: data.id }
        PostService.savePostComment(cmntData).then(function (res) {
            console.log('saved data ', data);
            if (res.ack == 1) {
                data.comment = '';
                $scope.getPostListings();
                if($stateParams.post_id)
                {
                    $scope.getPostById();
                }
            }

        })
    }

    $scope.saveSubComment = function (comment, post) {
        var cmntData = { comment: comment.newcomment, user_id: $scope.userDetails.id, post_id: post.id, comment_id: comment.id }
        PostService.savePostComment(cmntData).then(function (res) {

            if (res.ack == 1) {
                comment.newcomment = '';
                $scope.getPostListings();
                if($stateParams.post_id)
                {
                    $scope.getPostById();
                }
            }

        })
    }

    $scope.postLike = function (post) {
        post.my_like = !post.my_like;
        var data = { post_id: post.id, user_id: $scope.userDetails.id }
        PostService.savePostLike(data).then(function (res) {
            if (res.ack == 1) {
                post.like_count = res.like_count
            }
        })
    }

    $scope.commentLike = function (comment) {
        var data = { comment_id: comment.id, user_id: $scope.userDetails.id };
        PostService.saveCommentLike(data).then(function (res) {
            if (res.ack == 1) {
                comment.like_count = res.like_count
            }
            if(comment.is_like){
              if(comment.is_like == 1){
                comment.is_like = '0';
              }else{
                comment.is_like = '1';
              }

            }else{
              comment.is_like = '1';
            }
        })
    }


    $scope.loadTags = function (a) {
        return PostService.getTagSuggetion(a).then(function (data) {
            if (data.ack == 1) {
              //console.log(data);
                return data.tags;
            }
            else {
                return [];
            }
        })

    }
    
})
