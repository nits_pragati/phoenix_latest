app.controller('locationCtrl', function($scope, $state,$filter, $ionicSlideBoxDelegate, $ionicPopup,ionicToast,$rootScope,$ionicLoading, $stateParams,UserService, $http, $timeout, PostService,$cordovaGeolocation,$ionicTabsDelegate) {
    //console.log($stateParams);
    $scope.userDetailsnew =UserService.getUserInfo();
    $scope.getuserid = $stateParams.user_id;
    $scope.totalUsercount = 0;
    $scope.reviewcountval1 = false;
    $scope.reviewcountval2 = false;
    $scope.reviewcountval3 = false;
    $scope.reviewcountval4 = false;
    $scope.reviewcountval5 = false;
    $scope.isfollow = false;

    $scope.initreviewFrom = function(){
        $scope.review = {
            comment : ''
        }
    }

    var posOptions = {timeout: 10000,enableHighAccuracy: false};
    if($state.params.city){
        $scope.city  = $state.params.city;
    } else {
        $scope.city  = '';
    }

    if($state.params.distance){
        $scope.distance = $state.params.distance;
    } else {
        $scope.distance = '0';
    }
    if($state.params.cityname !='undefined'){
        $scope.cityname = $state.params.cityname;
        $("#input3").val($scope.cityname).trigger('change')
    }
    if(!UserService.getUserInfo())
    {
        $state.go('login');
    }
    $scope.selection = 'bio';
    $scope.changetab = function(model){
        $scope.selection = model;
    }
    $scope.$on('mapInitialized', function(event, map) {
        $scope.map=map;
        if($state.params.city_id && $state.params.lat&& $state.params.lng) {
         var pos = new google.maps.LatLng(Number($state.params.lat),Number($state.params.lng));
          //var pos = new google.maps.LatLng(29.7604,95.3698);
          //console.log(pos);
          $scope.map.setCenter(pos);
          $ionicLoading.hide();
        } else {
          navigator.geolocation.getCurrentPosition(function(position) {

            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            //console.log(pos);
            $scope.map.setCenter(pos);
            $ionicLoading.hide();
          });
        }
	});
    $scope.postList = [];
    $scope.getPostList = function(){
        PostService.getPostList('','').then(function(data){
            if(data.ack==1)
            {
                $scope.postList = data.list;
                $scope.postImageUrl = data.file_url;
            }
            //console.log(' -------- ',data)
        })
    }

    $scope.usersList = [];
    $scope.getUserList = function(){
        navigator.geolocation.getCurrentPosition(function (position) {
            if($state.params.lat && $state.params.lng){
                $scope.sclattitude = $state.params.lat;
                $scope.sclongitude = $state.params.lng;
            } else {
                $scope.sclattitude = position.coords.latitude;
                $scope.sclongitude = position.coords.longitude;
            }
            UserService.getUserSearch({service_type : $stateParams.service_type, lat : $scope.sclattitude,lang : $scope.sclongitude,city_id : $stateParams.city_id}).then(function(data){
                if(data.ack == 1)
                {
                    var result = Object.keys(data.list).map(function(key) {
                        return [Number(key), data.list[key]];
                    });
                    result.map((item)=>{
                      if(item.userlat && item.userlong)
                      {
                        item.userlat=Number(item.userlat);
                        item.userlong=Number(item.userlong);
                      }
                    })
                    $scope.usersList = data.list;
                    $scope.totalUsercount = data.totalcount
                    //debugger;
                    $scope.userImageUrl = data.file_url;
                    console.log('$scope.usersList',$scope.usersList);
                    console.log('$scope.totalUsercount',$scope.totalUsercount);
                }
            })
        }, function(err) {
            //alert(err)
            console.log('gps error ',err)
        },posOptions);
    }

    $scope.followUser = function(id){
        var followData = {
            user_by : $scope.userDetailsnew.id,
            user_to : id
        }
        UserService.followUser(followData).then(function(data){
            if(data.ack == 1)
            {
                $scope.getUserList();
                $scope.getUserDetails();
            }
            else
            {
                ionicToast.show('Internal error. Please try again later');
            }
        })
    }

    $scope.goToSingleResult=function(id,city,distance)
    {
        $state.go('single-result',{user_id : id,city : city,distance : distance});
    }
    $scope.reviewcount = function(val){
        if(val == '1'){
            if($scope.reviewcountval1){
                $scope.reviewcountval1 = false;
                $scope.reviewcountval2 = false;
                $scope.reviewcountval3 = false;
                $scope.reviewcountval4 = false;
                $scope.reviewcountval5 = false;
            } else {
                $scope.reviewcountval1 = true;
            }
        } else if(val == '2'){
            if($scope.reviewcountval2){
                $scope.reviewcountval2 = false;
                $scope.reviewcountval3 = false;
                $scope.reviewcountval4 = false;
                $scope.reviewcountval5 = false;
            } else {
                $scope.reviewcountval2 = true;
            }
            $scope.reviewcountval1 = true;
        } else if(val == '3') {
            if($scope.reviewcountval3){
                $scope.reviewcountval3 = false;
                $scope.reviewcountval4 = false;
                $scope.reviewcountval5 = false;
            } else {
                $scope.reviewcountval3 = true;
            }
            $scope.reviewcountval1 = true;
            $scope.reviewcountval2 = true;
        } else if(val == '4') {
            if($scope.reviewcountval4){
                $scope.reviewcountval4 = false;
                $scope.reviewcountval5 = false;
            } else {
                $scope.reviewcountval4 = true;
            }
            $scope.reviewcountval1 = true;
            $scope.reviewcountval2 = true;
            $scope.reviewcountval3 = true;
        } else if(val == '5') {
            if($scope.reviewcountval5){
                $scope.reviewcountval5 = false;
            } else {
                $scope.reviewcountval5 = true;
            }
            $scope.reviewcountval1 = true;
            $scope.reviewcountval2 = true;
            $scope.reviewcountval3 = true;
            $scope.reviewcountval4 = true;
            
        }
    }

    $scope.saveReview = function (data) {
        console.log(data);
        if(data.comment)
        {        
            $ionicLoading.show({
                //template: 'Saveing...'
                template:'<ion-spinner icon="ios"></ion-spinner> Saving...'
            });
            if($scope.reviewcountval1 && !$scope.reviewcountval2 && !$scope.reviewcountval3 && !$scope.reviewcountval4 && !$scope.reviewcountval5){
                $scope.reviewcountvalue = 1;
            } else if($scope.reviewcountval1 && $scope.reviewcountval2 && !$scope.reviewcountval3 && !$scope.reviewcountval4 && !$scope.reviewcountval5){
                $scope.reviewcountvalue = 2;
            } else if($scope.reviewcountval1 && $scope.reviewcountval2 && $scope.reviewcountval3 && !$scope.reviewcountval4 && !$scope.reviewcountval5){
                $scope.reviewcountvalue = 3;
            } else if($scope.reviewcountval1 && $scope.reviewcountval2 && $scope.reviewcountval3 && $scope.reviewcountval4 && !$scope.reviewcountval5){
                $scope.reviewcountvalue = 4;
            } else if($scope.reviewcountval1 && $scope.reviewcountval2 && $scope.reviewcountval3 && $scope.reviewcountval4 && $scope.reviewcountval5){
                $scope.reviewcountvalue = 5;
            } else {
                $scope.reviewcountvalue = 0;
            }
            var cmntData = { comment: data.comment, user_id: $scope.userDetailsnew.id, user_to: Number($stateParams.user_id),rating: $scope.reviewcountvalue };
            UserService.savereview(cmntData).then(function (res) {
                if (res.details.ack == 1) {
                    //console.log('Ack 1', res);
                    data.comment = '';
                    $scope.getUserList();
                    $ionicLoading.hide();
                    ionicToast.show(res.details.message);
                    $scope.getUserDetails()
                } else {
                    $ionicLoading.hide();
                    ionicToast.show(res.details.message);
                }
                setTimeout(function() {
                    $ionicLoading.hide();
                }, 5000);
            });
        }
    }

    $scope.goToSinglePhotos=function()
    {
        $state.go('search-result-photos',{user_id:$stateParams.user_id})
    }
    $scope.userDetails = []
    $scope.getUserDetails = function(){
        UserService.getUserDetailsById($stateParams.user_id,$scope.userDetailsnew.id).then(function(data){
            if(data.ack==1)
            {
                $scope.userDetails = data.details;
                $scope.userImageUrl = data.details.image_url;
                console.log('gallery ', $scope.userDetails.gallery)
            }
            else
            {
                ionicToast('Invalid user');
                $state.go('location');
            }

        })
    }

    // Triggered on a button click, or some other target
    $scope.showPopup = function(img) {
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
        template: '<div class="bottom-modal"><a class="closemodal" ng-click="closePopup()"><img src="../img/x.png" ></a><img src="' + img + '" alt=""></div>',
        scope: $scope,
        });
        $scope.closePopup = function() {
            myPopup.close();
        }
    };
    


    $scope.getPostListImage = function(){
        PostService.getPostListImage($stateParams.user_id).then(function(data){
            if(data.ack==1)
            {
                $scope.photos = data.list.post_files;
                $scope.photourl = data.file_url;
            }
            else
            {
                ionicToast('Invalid user');
                $state.go('location');
            }

        })
    }


    $scope.city_id = $stateParams.city_id;
    $scope.getCityDetails = function(){
        if($scope.city_id)
        {
            //console.log('hii');
            PostService.getCityDetails($scope.city_id).then(function(data){
                if(data.ack==1)
                {
                    $scope.cityDetails = data.details;
                    $scope.selectedServiceLoc = data.details.name;
                }
            })
        }
    }

    $scope.getCityList = function(){
        PostService.getCityList().then(function(data){
            if(data.ack == 1)
            {
                $scope.cityList = data.details;
                //console.log($scope.cityList);
            }
        })
    }

    $scope.getAutocompleteCity = function(q){
        return $filter('filter')($scope.cityList,q);
    }

    $scope.setSelectedCity = function(city){
        if(city)
        {
            $state.go('location',{service_type : $stateParams.service_type, city_id : city.item.id, lat:city.item.lat, lng:city.item.lng,cityname : city.item.name})
        }
    }
})
